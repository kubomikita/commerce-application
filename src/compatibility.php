<?php


class_alias(\Kubomikita\Commerce\ConfiguratorInterface::class, \Kubomikita\Commerce\IDiConfigurator::class);
class_alias(\Kubomikita\Commerce\DatabaseFactory::class, \Kubomikita\Commerce\DatabaseService::class);

if(!function_exists("Timestamp")){
	function Timestamp(){
		$T=new Timestamp();
		$arg=func_get_args();
		$T->set($arg);
		return $T;
	}
}

$langstr = new LangStr();