<?php
class LogAction {
	public static function checkPohoda(){
		$db = Registry::get("database");
		$R = $db->query("SELECT datum FROM ec_bridge_pohoda_results ORDER BY id DESC LIMIT 1")->fetch();

		$last = strtotime($R["datum"]);
		$secs = time() - $last;

		$interval = 60*25;
		$interval_sms = 60*60*5;
		//LogAction:add("Odoslaná SMS o zlyhani synchronizácie Pohody.",null,null,"SMS_warning");
//		dump("Sek. od poslednej synchronizacie: ".$secs,"Sek. po kt. poslat SMS: ".$interval);

		$R1 = $db->query("SELECT datetime FROM ec_users_action WHERE objectId='SMS_warning' ORDER BY id DESC LIMIT 1")->fetch();
		$last_sms = strtotime($R1["datetime"]);
		$secs_sms = time()-$last_sms;
//		dump("Sek. od poslendej SMS: ".$secs_sms,"Sek. po kt. sa odosle dasia SMS: ".$interval_sms);
		//LogAction::add("Odoslaná SMS o zlyhani synchronizácie Pohody.",null,null,"SMS_warning");
		if($secs > $interval and $secs_sms > $interval_sms){
			LogAction::add("Odoslaná SMS o zlyhani synchronizácie Pohody.",null,null,"SMS_warning");
			//dump("poslat spravu");
			Sms::send("+421911531872","Vypadok synchronizacie Pohody. Posledna o ".$R["datum"]);
			Sms::send("+421914777222","Vypadok synchronizacie Pohody. Posledna o ".$R["datum"]);
		}
	}
	public static function add($text,$dumpBefore=null,$dumpAfter=null,$objectId=null){
		$d = debug_backtrace();
		$n = new Users_action;
		$n->text = $text;
		$n->ip = $_SERVER["HTTP_X_REAL_IP"];
		if(Auth::$userId > 0){
			$n->user_login = Auth::$User->username;
			$n->user = Auth::$User->fullname;
		}
		$n->file = end(explode("/",$d[0]["file"]));
		if(php_sapi_name() != "cli" and is_array($d[1]["args"][0])) {
			$n->action = http_build_query( $d[1]["args"][0] );
		}
		if(strpos($text,"otvoril")!==false){
			$n->objectName = "open";
		}
		if(strpos($text,"vymazal")!==false){
			$n->objectName = "delete";
		}
		if(strpos($text,"pridal")!==false){
			$n->objectName = "add";
		}
		if(strpos($text,"upravil")!==false){
			$n->objectName = "edit";
		}/* else {
            $n->objectName = "unknown";
        }*/
		$n->objectId = $objectId;
		$n->dumpBefore = $dumpBefore;
		$n->dumpAfter = $dumpAfter;
		$n->datetime = new \Nette\Utils\DateTime();
		$n->save();

		//dump($n,$d,http_build_query($d[1]["args"][0]));
	}
}