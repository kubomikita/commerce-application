<?php

class Cena {
	private $bezdph = Config::cenyBezDph;
	private $dph = Config::cenyDefaultDph;
	private $zaklad = 0;
	private $suma = 0;
	private $currency = Config::nativeCurrency;
	private $zlava;

	function __construct($cena = null,int $dph = null, float $zlava = null){
		if($this->bezdph){
			$this->zaklad= (float) $cena;
		} else {
			$this->suma = (float) $cena;
		};
		if($dph !== null) {
			$this->dph = $dph;
		}
		if($zlava !== null){
			$this->zlava = $zlava;
		}
	}

	public function raw(){
		if($this->bezdph){ return $this->zaklad; } else { return $this->suma; };
	}

	public function dph(){
		if(func_num_args()>=1){ $this->dph=func_get_arg(0); };
		return $this->dph;
	}

	public function print_dph(){
		return $this->dph.'%';
	}

	private function format_currency(){
		$format='%.2f '.$this->currency;
		if(Config::$currencyFormat[$this->currency]!=''){ $format=Config::$currencyFormat[$this->currency]; };
		return $format;
	}

	private function format_currency_plain(){ return '%.2f '.$this->currency; }

	public function bezdph($v){
		if($this->bezdph && !$v){
			$this->suma($this->suma());
		} else if(!$this->bezdph && $v){
			$this->zaklad($this->zaklad());
		};

	}

	public function zaklad(){
		if(func_num_args()>=1){ $this->zaklad=func_get_arg(0); $this->bezdph=1; };
		if($this->bezdph){
			return $this->zaklad;
		} else {
			return $this->suma/(1+($this->dph/100));
		}
	}
	public function print_zaklad($decimals=2){
		return sprintf("%.".$decimals."f",$this->zaklad());
	}
	public function format_zaklad(){
		return sprintf($this->format_currency(),$this->zaklad());
	}

	public function suma(){
		if(func_num_args()>=1){ $this->suma=func_get_arg(0); $this->bezdph=0; };
		if($this->bezdph){
			return (float) round($this->zaklad*(1+($this->dph/100)),2);
		} else {
			return (float) round($this->suma,2);
		}
	}
	/*public function old_suma(): float {
		if(!$this->zlava){
			return $this->suma();
		}
		if(!$this->bezdph){
			
		}
	}*/
	public function print_suma($decimals=2){
		return sprintf("%.".$decimals."f",$this->suma());
	}
	public function format_suma(){
		return sprintf($this->format_currency(),$this->suma());
	}

	public function format_suma_plain(){
		return sprintf($this->format_currency_plain(),$this->suma());
	}

	public function multiply($k){
		$C = clone $this;
		if($this->bezdph){
			$C->zaklad($this->zaklad()*$k);
		} else {
			$C->suma($this->suma()*$k);
		};
		return $C;
	}

	public function minus(float $value) : Cena {
		$cena = clone $this;
		$this->bezdph ? ($cena->zaklad($this->zaklad() - $value)) : ($cena->suma($this->suma() - $value));
		return $cena;
	}

}