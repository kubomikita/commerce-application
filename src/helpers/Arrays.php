<?php

namespace Kubomikita\Utils;

class Arrays {
	/**
	 * Setting nested value.
	 *
	 * Example:
	 * <code>
	 * $array = array();
	 * Arrays::setNestedValue($array, array('1', '2', '3'), 'value');
	 * </code>
	 * Result:
	 * <code>
	 * Array
	 * (
	 *      [1] => Array
	 *          (
	 *              [2] => Array
	 *                  (
	 *                      [3] => value
	 *                  )
	 *          )
	 * )
	 * </code>
	 *
	 * @param array $array
	 * @param array $keys
	 * @param $value
	 */
	public static function setNestedValue(array &$array, array $keys, $value)
	{
		$current = &$array;
		foreach ($keys as $key) {
			if (!isset($current[$key])) {
				$current[$key] = [];
			}
			$current = &$current[$key];
		}
		$current = $value;
	}

	/**
	 * Return nested value when found, otherwise return <i>null</i> value.
	 *
	 * Example:
	 * <code>
	 * $array = array('1' => array('2' => array('3' => 'value')));
	 * $value = Arrays::getNestedValue($array, array('1', '2', '3'));
	 * </code>
	 * Result:
	 * <code>
	 * value
	 * </code>
	 *
	 * @param array $array
	 * @param array $keys
	 * @return array|mixed|null
	 */
	public static function getNestedValue(array $array, array $keys)
	{
		foreach ($keys as $key) {
			$array = self::getValue(self::toArray($array), $key);
			if (!$array) {
				return $array;
			}
		}
		return $array;
	}
	/**
	 * Returns the element for the given key or a default value otherwise.
	 *
	 * Example:
	 * <code>
	 * $array = array('id' => 1, 'name' => 'john');
	 * $value = Arrays::getValue($array, 'name');
	 * </code>
	 * Result:
	 * <code>john</code>
	 *
	 * Example:
	 * <code>
	 * $array = array('id' => 1, 'name' => 'john');
	 * $value = Arrays::getValue($array, 'surname', '--not found--');
	 * </code>
	 * Result:
	 * <code>--not found--</code>
	 *
	 * @param array $elements
	 * @param string|int $key
	 * @param mixed|null $default
	 * @return mixed|null
	 */
	public static function getValue(array $elements, $key, $default = null)
	{
		return isset($elements[$key]) ? $elements[$key] : $default;
	}

	/**
	 * Make array from element. Returns the given argument if it's already an array.
	 *
	 * Example:
	 * <code>
	 * $result = Arrays::toArray('test');
	 * </code>
	 * Result:
	 * <code>
	 * Array
	 * (
	 *      [0] => test
	 * )
	 * </code>
	 *
	 * @param mixed $element
	 * @return array
	 */
	public static function toArray($element)
	{
		if (is_null($element)) {
			return [];
		}
		return is_array($element) ? $element : [$element];
	}
	/**
	 * @deprecated
	 * @param array $array
	 * @param array $keys
	 */
	public static function removeNestedValue(array &$array, array $keys)
	{
		trigger_error('Use Arrays::removeNestedKey instead', E_USER_DEPRECATED);
		self::removeNestedKey($array, $keys);
	}
}