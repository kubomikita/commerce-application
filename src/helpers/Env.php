<?php

/**
 * @method payment
 * @method shop
 */
class Env {

	/**
	 * @param $name
	 * @param $arguments
	 *
	 * @return mixed
	 * @throws \Nette\Application\AbortException
	 */
	public static function __callStatic( $name, $arguments ) {
		/** @var \Kubomikita\Commerce\ProteinConfigurator $c */
		$c = Registry::get("container");
		return $c->getParameter($name,$arguments[0]);
	}
}