<?php

use Nette\Utils\Strings;

class Format {

	static public function bytes($bytes, $precision = 2) {
		$units = array('B', 'KB', 'MB', 'GB', 'TB');

		$bytes = max($bytes, 0);
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1);

		// Uncomment one of the following alternatives
		$bytes /= pow(1024, $pow);
		// $bytes /= (1 << (10 * $pow));

		return round($bytes, $precision) . ' ' . $units[$pow];
	}

	static public function parsefloat($n){
		return (float)strtr(trim($n),array(','=>'.'));
	}

	static public function clean_filename($fileName){
		$allowedchars=str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-.');
		$newFileName='';
		for($i=0;$i<strlen($fileName);$i++){
			$char=$fileName[$i];
			if(in_array($char,$allowedchars)){ $newFileName.=$char; } else { $newFileName.='_'; };
		};
		$newFileName;

	}

	static public function money($amount){
		return sprintf("%.2F",$amount).' '.Config::nativeCurrency;
	}
	/*static public function money_user($amount){
		$currency=Config::nativeCurrency;
		$format='%.2f '.$currency;
		if(Config::$currencyFormat[$currency]!=''){ $format=Config::$currencyFormat[$currency]; };
		return sprintf($format,$amount);
	}*/
	static public function money_user($amount,$currency=''){
		if($currency==''){
			if(isset($_SESSION['currency']) && Config::formatCurrencySession && isset($_SESSION["currency"]) && $_SESSION['currency']!=''){
				$currency=$_SESSION['currency'];
			} else { $currency=Config::nativeCurrency; };
		};
		$format='%.2f '.$currency;
		if($currency!=Config::nativeCurrency){ $amount*=Currency::rate($currency)/Currency::rate(Config::nativeCurrency); };
		if(Config::$currencyFormat[$currency]!=''){ $format=Config::$currencyFormat[$currency]; };
		return sprintf($format,$amount);
	}
	static public function money_user_plain($amount,$currency=""){
		if($currency==''){
			if(Config::formatCurrencySession && isset($_SESSION["currency"]) && $_SESSION['currency']!=''){
				$currency=$_SESSION['currency'];
			} else { $currency=Config::nativeCurrency; };
		};
		$format='%.2F';
		if($currency!=Config::nativeCurrency){ $amount*=Currency::rate($currency)/Currency::rate(Config::nativeCurrency); };
		//if(Config::$currencyFormat[$currency]!=''){ $format=Config::$currencyFormat[$currency]; };
		return sprintf($format,$amount);

	}
	static public function money_papi($amount,$currency = null,$f = true){
		if($currency === null){
			$currency = Config::nativeCurrency;
		}
		$format='%.2f';
		if($f){
			$format.=" ".Config::nativeCurrency;
		}
		if($currency!=Config::nativeCurrency){ $amount/=Currency::rate_papi($currency)/Currency::rate_papi(Config::nativeCurrency); };
		//if(Config::$currencyFormat[$currency]!=''){ $format=Config::$currencyFormat[$currency]; };
		return sprintf($format,$amount);
	}
	static public function qty($amount,$int=0){
		$ret=sprintf("%.2F",$amount);
		if($int){ $ret=round($amount); };
		return $ret;
	}

	static public function id_encode($id){
		$ret='';
		$itrans=array('0'=>'k','1'=>'l','2'=>'m','3'=>'n','4'=>'o','5'=>'p','6'=>'q','7'=>'r','8'=>'s','9'=>'t',);
		$rtrans=array('0'=>'a','1'=>'b','2'=>'c','3'=>'d','4'=>'e','5'=>'f','6'=>'g','7'=>'h','8'=>'i','9'=>'j',);
		for($i=0;$i<strlen($id);$i++){
			$c=substr($id,$i,1);
			//var_dump($c);
			$p1=rand(0,10);
			$p2=rand(0,10);
			for($j=0;$j<$p1;$j++){ $ret.=strtr(rand(0,9),$rtrans); };
			$ret.=strtr($c,$itrans);
			for($j=0;$j<$p2;$j++){ $ret.=strtr(rand(0,9),$rtrans); };
		};
		return $ret;
	}

	static public function id_decode($code){
		$xtrans=array('a'=>'','b'=>'','c'=>'','d'=>'','e'=>'','f'=>'','g'=>'','h'=>'','i'=>'','j'=>'','k'=>'0','l'=>'1','m'=>'2','n'=>'3','o'=>'4','p'=>'5','q'=>'6','r'=>'7','s'=>'8','t'=>'9',);
		return strtr($code,$xtrans);
	}
	static function shortFilename($string,$max_length,$end_substitute="..."){
		if(strlen($string) >= $max_length){
			return "<span title=\"$string\">".mb_substr($string, 0, $max_length, 'UTF-8').$end_substitute."</span>";
		} else {
			return $string;
		}
	}

	static function shortText($string, $max_length, $end_substitute = "...", $html_linebreaks = true) {

		if($html_linebreaks) $string = preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
		$string = strip_tags($string); //gets rid of the HTML

		if(empty($string) || mb_strlen($string) <= $max_length) {
			if($html_linebreaks) $string = nl2br($string);
			return $string;
		}

		if($end_substitute) $max_length -= mb_strlen($end_substitute, 'UTF-8');

		$stack_count = 0;
		while($max_length > 0){
			$char = mb_substr($string, --$max_length, 1, 'UTF-8');
			if(preg_match('#[^\p{L}\p{N}]#iu', $char)) $stack_count++; //only alnum characters
			elseif($stack_count > 0) {
				$max_length++;
				break;
			}
		}
		$string = mb_substr($string, 0, $max_length, 'UTF-8').$end_substitute;
		if($html_linebreaks) $string = nl2br($string);

		return $string;
	}
	static function date($date,$pattern="d.m.Y H:i:s"){
		if(!is_int($date)){
			$date=strtotime($date);
		}
		return date($pattern,$date);
	}
	static function chmod($perms){
		if (($perms & 0xC000) == 0xC000) {
			// Socket
			$info = 's';
		} elseif (($perms & 0xA000) == 0xA000) {
			// Symbolic Link
			$info = 'l';
		} elseif (($perms & 0x8000) == 0x8000) {
			// Regular
			$info = '-';
		} elseif (($perms & 0x6000) == 0x6000) {
			// Block special
			$info = 'b';
		} elseif (($perms & 0x4000) == 0x4000) {
			// Directory
			$info = 'd';
		} elseif (($perms & 0x2000) == 0x2000) {
			// Character special
			$info = 'c';
		} elseif (($perms & 0x1000) == 0x1000) {
			// FIFO pipe
			$info = 'p';
		} else {
			// Unknown
			$info = 'u';
		}

		// Owner
		$info .= (($perms & 0x0100) ? 'r' : '-');
		$info .= (($perms & 0x0080) ? 'w' : '-');
		$info .= (($perms & 0x0040) ?
			(($perms & 0x0800) ? 's' : 'x' ) :
			(($perms & 0x0800) ? 'S' : '-'));

		// Group
		$info .= (($perms & 0x0020) ? 'r' : '-');
		$info .= (($perms & 0x0010) ? 'w' : '-');
		$info .= (($perms & 0x0008) ?
			(($perms & 0x0400) ? 's' : 'x' ) :
			(($perms & 0x0400) ? 'S' : '-'));

		// World
		$info .= (($perms & 0x0004) ? 'r' : '-');
		$info .= (($perms & 0x0002) ? 'w' : '-');
		$info .= (($perms & 0x0001) ?
			(($perms & 0x0200) ? 't' : 'x' ) :
			(($perms & 0x0200) ? 'T' : '-'));

		return $info;
	}
	static function seoName($nadpis) {
		return Strings::webalize($nadpis);
		/*$a = array(
			"ö" => "o",
			"ű" => "u",
			"ő" => "o",
			"ü" => "u",
			"ł" => "l",
			"ż" => "z",
			"ń" => "n",
			"ć" => "c",
			"ę" => "e",
			"ś" => "s",
			"ŕ" => "r",
			"á" => "a",
			"ä" => "a",
			"ĺ" => "l",
			"č" => "c",
			"é" => "e",
			"ě" => "e",
			"í" => "i",
			"ď" => "d",
			"ň" => "n",
			"ó" => "o",
			"ô" => "o",
			"ř" => "r",
			"ů" => "u",
			"ú" => "u",
			"š" => "s",
			"ť" => "t",
			"ž" => "z",
			"ľ" => "l",
			"ý" => "y",
			"Ŕ" => "R",
			"Á" => "A",
			"Ä" => "A",
			"Ĺ" => "L",
			"Č" => "C",
			"É" => "E",
			"Ě" => "E",
			"Í" => "I",
			"Ď" => "D",
			"Ň" => "N",
			"Ó" => "O",
			"Ô" => "O",
			"Ř" => "R",
			"Ů" => "U",
			"Ú" => "U",
			"Š" => "S",
			"Ť" => "T",
			"Ž" => "Z",
			"Ľ" => "L",
			"Ý" => "Y",
			"Ä" => "A");

			  echo strtr($str, $a);

		?>*/
		$url = $nadpis;
		$url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
		$url = trim($url, "-");
		$url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
		$url = strtolower($url);
		$url = preg_replace('~[^-a-z0-9_]+~', '', $url);
		return $url;
	}
	static function convertSecToStr($secs){
		$output = '';
		if($secs >= 86400) {
			$days = floor($secs/86400);
			$secs = $secs%86400;
			$output = $days.' dní';
			if($days != 1) $output .= '';
			if($secs > 0) $output .= ', ';
		}
		if($secs>=3600){
			$hours = floor($secs/3600);
			$secs = $secs%3600;
			$output .= $hours.' hodín';
			if($hours != 1) $output .= '';
			if($secs > 0) $output .= ', ';
		}
		if($secs>=60){
			$minutes = floor($secs/60);
			$secs = $secs%60;
			$output .= $minutes.' minút';
			/*if($minutes != 1) $output .= '';
			if($secs > 0) $output .= ', ';*/
		}
		/*$output .= $secs.' second';
		if($secs != 1) $output .= '';*/
		return $output;
	}
}