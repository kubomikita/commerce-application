<?php

namespace Kubomikita\Commerce;
use Nette\InvalidArgumentException;
use Nette\Utils\Json;
use Nette\Utils\Strings;

/**
 * @property $breadcrumb
 * @property $product
 * @property $website
 * @property $organization
 * @property $blog
 * @property $blogitem
 */
class Microdata {

	const SECTION = "kubomikita/microdata";
	private $session;
	private $section = [];
	/** @var ProteinConfigurator */
	private $context;
	public function __construct(ConfiguratorInterface $context) {
		$this->context = $context;


	}
	public function __set( $name, $value ) {
		$fname = "action".Strings::firstUpper($name);
		$this->$fname($value);
	}
	public function __get( $name ) {
		return $this->section[$name];
	}

	private function actionBreadcrumb(array $value){
		$a = [
			"@context" => "http://schema.org",
			"@type" => "BreadcrumbList",
			"itemListElement" => []
		];
		$i = 1;
		$item = [];
		$item["@type"] = "ListItem";
		$item["position"] = $i;
		$item["name"] = $this->context->getParameter("shop","name");
		$item["item"] = $this->context->getParameter("shop","url");
		$i++;

		$a["itemListElement"][] = $item;

		foreach($value as $link => $name){
			$item = [];
			$item["@type"] = "ListItem";
			$item["position"] = $i;
			$item["name"] = $name;
			$item["item"] = $this->context->getParameter("shop","url").$link;
			$i++;

			$a["itemListElement"][] = $item;
		}
		$this->section["breadcrumb"] = $a;
		//return Json::encode($a);
	}
	private function actionOrganization(array $values){
		$a = [
			"@context" => "http://schema.org",
			"@type" => "Organization"
		];
		if(isset($values["url"])){
			$a["url"] = $values["url"];
		}
		if(isset($values["logo"])){
			$a["logo"] = $values["logo"];
		}
		if(isset($values["contact"])){
			if(is_array($values["contact"])){
				foreach($values["contact"] as $contact){
					$a["contactPoint"][] = ["@type"=>"ContactPoint","telephone" => $contact,"contactType"=>"customer service"];
				}
			} else {
				$a["contactPoint"][] = ["@type"=>"ContactPoint","telephone" => $values["contact"],"contactType"=>"customer service"];
			}
		}
		if(isset($values["profiles"])){
			foreach($values["profiles"] as $profile){
				$a["sameAs"][] = $profile;
			}
		}
		$this->section["organization"] = $a;
	}
	private function actionWebsite(array $values){
		if(!isset($values["url"])){
			throw new InvalidArgumentException("Microdata 'WebSite' must have setted URL parameter.");
		}
		$a = [
			"@context"=> "http://schema.org",
            "@type"=> "WebSite",
			"url" => $values["url"]
		];
		if(isset($values["author"])){
			$a["author"] = $values["author"];
		}
		if(isset($values["search"])){
			$a["potentialAction"]["@type"] = "SearchAction";
			$a["potentialAction"]["target"] = $values["search"]["action"];
			$a["potentialAction"]["query-input"] =  $values["search"]["input"];

		}
		$this->section["website"] = $a;
	}
	private function actionProduct(array $values){
		/** @var \Tovar $T */
		$T = $values["product"];
		$images = $T->images();
		$cena = $T->predajna_cena();
		$a = [
			"@context"=> "http://schema.org/",
			"@type"=> "Product",
		    "name"=> (string) $T->nazov,
			"productID" => $T->id
		];

		if(!empty($images)){
			foreach ($images as $img){
				$a["image"][] = $this->context->getParameter("shop","url").$img->cachedUrlName();
			}
		}
		if(strlen(trim($T->intro)) > 0){
			$a["description"] = trim($T->intro);
		}
		if($T->vyrobca->id > 0){
			$a["brand"] = (string) $T->vyrobca->nazov;
		}
		if($T->hodnotenie() > 0){
			$a["aggregateRating"]["@type"] = "AggregateRating";
			$a["aggregateRating"]["ratingValue"] = $T->hodnotenie();
			$a["aggregateRating"]["ratingCount"] = $T->hodnotenie_count();
		}
		//bdump($cena);
		//bdump($T->predajna_cena_value(""));
		if($cena->suma() > 0) {
			$single = true;
			if(!empty($values["variants"])){
				$prices = [];
				foreach($values["variants"] as $var){
					foreach($var as $v){
						$price = $v->predajna_cena()->suma();
						if($price > 0 and !in_array($price,$prices)){
							$prices[] = $price;
						}
					}
				}
				$min = (float)min($prices);
				$max = (float)max($prices);
				if($min != $max){
					$single = false;
				}
			}


			if ( $single ) {

				$a["offers"]["@type"]         = "Offer";
				$a["offers"]["priceCurrency"] = "EUR";
				$a["offers"]["price"]         = round( $cena->suma(), 2 );


			} else {
				$a["offers"]["@type"] = "AggregateOffer";
				$a["offers"]["priceCurrency"] = "EUR";
				$a["offers"]["lowPrice"] = round($min,2);
				$a["offers"]["highPrice"] = round($max,2);
			}
			$a["offers"]["itemCondition"] = "http://schema.org/NewCondition";
			if ( \Config::kosikCheckSklad ) {
				if ( $T->sklad_available() > 0 and $T->aktivny ) {
					$a["offers"]["availability"] = "http://schema.org/InStock";
				} else {
					$a["offers"]["availability"] = "http://schema.org/OutOfStock";
				}
			} else {
				if($T->aktivny) {
					$a["offers"]["availability"] = "http://schema.org/InStock";
				} else {
					$a["offers"]["availability"] = "http://schema.org/OutOfStock";
				}
			}

			$a["offers"]["seller"] = [
				"@type" => "Organization",
				"name"  => $this->context->getParameter( "shop", "name" )
			];
		}
		$this->section["product"] = $a;
	}
	public function render(){
		ob_start();
		foreach($this->section as $section){
			echo '<script type="application/ld+json">'.Json::encode($section).'</script>';
		}
		$html = ob_get_clean();
		return $html;
	}
	public function count(){
		return count($this->section);
	}
	public function getSection(){
		return $this->section;
	}
	public static function internationalPhoneNumber($number){
		$val = str_replace(" ","",$number);
		if(!Strings::contains($val,"+421")){
			$val = "+421".Strings::substring($val,1);
		}
		return $val;
	}
}
/*
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [{
    "@type": "ListItem",
    "position": 1,
    "name": "Books",
    "item": "https://example.com/books"
  },{
    "@type": "ListItem",
    "position": 2,
    "name": "Authors",
    "item": "https://example.com/books/authors"
  },{
    "@type": "ListItem",
    "position": 3,
    "name": "Ann Leckie",
    "item": "https://example.com/books/authors/annleckie"
  },{
    "@type": "ListItem",
    "position": 4,
    "name": "Ancillary Justice",
    "item": "https://example.com/books/authors/ancillaryjustice"
  }]
}
</script>
*/