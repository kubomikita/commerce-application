<?php

//declare(strict_types=1);

namespace Kubomikita\Commerce;
use Kubomikita\Service;
use MatthiasMullie\Minify;
use Nette\Caching\Cache;
use Nette\Utils\Strings;
class Minifier {
	/** @var array */
	private $exts = ["js","css"];

	/** @var null|string Type of minifier */
	private $type = null;

	/** @var string Default path for CSS minified file */
	private $path_css = "assets/css/stylesheet-{count}.css";
	/** @var string Default path for JS minified file */
	private $path_js = "assets/js/packed-{count}.js";

	/** @var null Real path used to generate file */
	private $path = null;

	private $files = [];
	private $raw = [];

	private $render = [];

	const TYPE_RAW = "raw";
	const TYPE_FILE = "file";

	/**
	 * Minifier constructor.
	 *
	 * @param array $files
	 * @param Minify\Minify $minifier
	 * @param $path
	 *
	 * @throws \Exception
	 */
	public function __construct(array $files, Minify\Minify $minifier, $path = null) {
		$this->files = $this->checkFiles($files);
		$this->type = $this->getType($minifier);
		$time = $this->checkTimes();
		if($time) {
			/*foreach($this->files as $f){
				$minifier->add($f);
			}
			foreach ( $this->raw as $f){
				$minifier->add($f);
			}*/
			foreach ($this->render as $item){
				$minifier->add($item["content"]);
			}
			$minifier->minify(WWW_DIR.$this->path);
			bdump($files,"GENERATING NEW FILE: ".$this->path);
		}
		//dump($this);
	}

	/**
	 * @param array $files
	 *
	 * @return array
	 * @throws \Exception
	 */
	private function checkFiles(array $files) {
		$ok = [];
		foreach($files as $f){
			$e = explode(".",$f);
			if(in_array(end($e),$this->exts)) {

				if(Strings::contains($f,"http://") || Strings::contains($f,"https://") || Strings::contains($f,"//")){
					//dump($f);
					//$this->raw[] = $this->getRawFromRemote($f);
					$this->render[] = ["type" => self::TYPE_RAW, "content" => $this->getRawFromRemote($f)];
				} else {
					if ( file_exists( $f ) ) {
						$ok[] = $f;
						$this->render[] = ["type" => self::TYPE_FILE, "content" => $f];
					}
				}
			} else {
				$this->render[] = ["type" => self::TYPE_RAW, "content" => $f];
				//$this->raw[] = $f;
			}
		}
		if(empty($ok)){
			throw new \Exception("No files added to minify.",500);
		}

		return $ok;
	}

	private function getRawFromRemote($remote){
		$c = new Cache(\Registry::get("cache.storage"),"MinifierCDNs");

		$raw = $c->load(Strings::webalize($remote), function (&$dependencies) use ($remote){
			return \HTTP::get($remote);
		});

		return $raw;
	}

	/**
	 * Check all modified times of files to last minified file
	 *
	 * @return bool
	 */
	private function checkTimes() {

		$times = [];
		foreach($this->files as $f){
			$times[] = (int) @filemtime($f);
		}

		return !((int) @filemtime($this->path) > max($times));
	}

	/**
	 * Get type by minifier class
	 * @param Minify\Minify $minifier
	 *
	 * @return string
	 */
	private function getType(Minify\Minify $minifier) {
		$class = get_class($minifier);
		$explode = explode("\\",$class);
		$type = Strings::lower(end($explode));

		$this->path = str_replace("{count}", count($this->render),$this->{"path_".$type});

		return $type;
	}

	/**
	 * @deprecated
	 * @param $minified
	 */
	private function generateMinify($minified){
		file_put_contents(WWW_DIR.$this->path,$minified);
	}

	/**
	 * @return string
	 * @throws \Exception
	 */
	public function output() {
		/** @var ProteinConfigurator $context */
		$context = Service::get("container");
		//dump($this->files,$this);
		if(!$context->isProduction()){
			ob_start();
			foreach($this->render as $item){
				$type = $item["type"];
				$content = $item["content"];
				if($type == self::TYPE_FILE) {
					$path = $content;
					if(function_exists("autoVersion")){
						$path = autoVersion($content,false,true);
					}
					echo $this->getHtml($path);
				} elseif ($type == self::TYPE_RAW){
					echo $this->getHtml($content,true);
				}
			}
			$html = ob_get_clean();
			return $html;
		} else {
			$path = $this->path;
			if(function_exists("autoVersion")){
				$path = autoVersion($this->path,false,true);
			}
			return $this->getHtml($path);
		}


	}

	/**
	 * @param string $path
	 * @param bool $raw
	 *
	 * @return string
	 */
	private function getHtml($path,$raw = false) {
		if(!$raw) {
			switch ( $this->type ) {
				case "css":
					return '<link rel="stylesheet" type="text/css" href="' . $path . '">';
					break;
				case "js":
					return '<script type="text/javascript" src="' . $path . '"></script>';
					break;
			}
		} else {
			switch ( $this->type ) {
				case "css":
					return '<style type="text/css">' . $path . '</style>';
					break;
				case "js":
					return '<script type="text/javascript">' . $path . '</script>';
					break;
			}
		}
	}

	/**
	 * @return string
	 * @throws \Exception
	 */
	public function __toString() {
		return $this->output();
	}
}