<?php

namespace Kubomikita\Commerce;


use Detection\MobileDetect;
use Gettext\Translator;
use Kubomikita\Commerce\Routing\Router;
use Latte\Engine;
use Nette\Application\BadRequestException;
use Nette\DI\Container;
use Nette\Http\IRequest;
use Nette\Http\IResponse;
use Nette\Http\Request;
use Nette\Http\UrlScript;
use Nette\InvalidArgumentException;
use Nette\InvalidStateException;
use Nette\SmartObject;
use Throwable;
use Tracy\Debugger;
use function GuzzleHttp\Psr7\str;

/**
 * @property-read Router $router
 */
class Application implements ApplicationInterface {
	use SmartObject;
	/** @var callable[] */
	public $onStart;
	/** @var callable[] */
	public $onRequest;
	/** @var callable[] */
	public $onShutdown;
	
	protected $httpRequest;
	protected $httpResponse;
	protected $configurator;
	/** @var Container */
	protected $container;

	protected $url;
	/** @var \Page */
	protected $page;
	/** @var Router|null */
	protected $router;

	protected $responseCode = 200;

	protected $controllers;
	protected $boxes = [];
	protected $metadata = [];
	protected $templateParams = [];


	public function __construct(IRequest $request, IResponse $response, ConfiguratorInterface $configurator) {
		$this->httpRequest = $request;
		$this->httpResponse = $response;
		$this->configurator = $configurator;
		$this->container = $this->configurator->container;
		$this->url = $this->httpRequest->getQuery("seolink");
		$this->controllers = $this->configurator->controllers;
		//dumpe($this->controllers);

		$this->onStart[] = function (){
			/** @var Translator $translator */
			$translator = $this->container->getByType(Translator::class);
		};

		$this->onRequest[] = function () {
			if($this->page instanceof \Page && $this->page->isMain()){
				$this->setDefaultMicrodata();
			}
		};

		$this->onShutdown[] = function () use($configurator){
			if($configurator->isDebuggerEnabled() && class_exists($configurator->getDebuggerPanelService())){
				$class = ($configurator->getDebuggerPanelService());
				/** @var TracyPanelService $tracyPanel */
				$tracyPanel = new $class();
				$tracyPanel->shutdownPanels($configurator);

				$registry = $this->container->getByType(RegistryProvider::class);
			}
		};

		$this->setDefaultTemplateParameters();
	}

	protected function processInitialRequest() {
		$routeList = $this->container->getByType(\Commerce\Routing\RouterFactory::class)->createRouter();
		//$this->container->addService("router.router", $routeList);
		$params = $routeList->match((new \Nette\Http\RequestFactory())->fromGlobals());
		bdump($params);
		//dumpe($params, "Nette Router");
		try {
			if ( $params === null ) {
				throw new BadRequestException();
			}
			$this->page = $params[\Commerce\Routing\RouterFactory::PAGE_KEY];
			if(($template = $this->page->template()) === null){
				throw new InvalidStateException("Page object is null.");
			}
			$this->boxes = $template->fullBoxes($this->page, $this->controllers);

		} catch (Throwable $e){
			$this->responseCode = $e->getCode();
			if(in_array($this->responseCode, [404,403])){
				$this->page = new \Page(\Env::page("404"));
				if(($template = $this->page->template()) === null){
					throw new InvalidArgumentException("Page object is null.");
				}
				$this->boxes = $template->fullBoxes($this->page, $this->controllers);
			} else {
				throw $e;
			}
		}
	}

	/**
	 * @throws Throwable
	 */
	protected function processRequest() {
		try {
			$seolink = $this->httpRequest->getQuery( 'seolink' );
			if ( !empty($this->url)){ // !== '' ) {
				$PageSeo = \Page::getBySeolink( $seolink );
				if ( $PageSeo->id > 0 ) {
					$this->page = $PageSeo;
				} else {
					$this->router = \Kubomikita\Commerce\Routing\Router::findByLink( $seolink );
					$this->container->addService("application.router", $this->router);
					$res      = $this->router->match();
					if ( $res === false ) {
						throw new \Nette\Application\BadRequestException();
					}
					$this->page = new \Page($res);
				}
			} else {
				$this->page = \Page::getMain();
			}
			if(($template = $this->page->template()) === null){
				throw new InvalidArgumentException("Page object is null.");
			}
			$this->boxes = $template->fullBoxes($this->page, $this->controllers);
		} catch ( Throwable $e) {
			$this->responseCode = $e->getCode();
			if(in_array($this->responseCode, [404,403])){
				$this->page = new \Page(\Env::page("404"));
				if(($template = $this->page->template()) === null){
					throw new InvalidArgumentException("Page object is null.");
				}
				$this->boxes = $template->fullBoxes($this->page, $this->controllers);
			} else {
				throw $e;
			}

		}

	}

	protected function defaultMeta() {
		global $PARM;
		$PARM['title'] = __( $this->page->title );
		$PARM['desc']  = __( $this->page->desc );
		//$PARM["alternate"]["sk"] = $Page->url;
		//$PARM["alternate"]["cz"] = $Page->url_cz;

		$PARM["robots"]    = ( $this->page->robots == "" ? "index, follow" : $this->page->robots );
		$PARM["seolink"]   = $this->page->url;
		$PARM["microdata"] = $this->page->microdata;


		$TITLE_ADD = " • " . $this->container->parameters[ "shop"]["name"];
		if ( $this->page->isMain() ) {
			$PARM["btitle"] = $PARM["title"];
			$TITLE_ADD      = " • Eshop " . $this->container->parameters[ "shop"]["name"];
		}

		$desc = __( "desc" );
		if ( $PARM["desc"] != "" ) {
			$desc = $PARM["desc"];
		}
		if ( $PARM["desc"] == null ) {
			$desc = "";
		}

		if ( isset( $PARM['ptitle'] ) && $PARM['ptitle'] != '' ) {
			$title = $PARM['ptitle'] . $TITLE_ADD;
		} elseif ( isset( $PARM['btitle'] ) && $PARM["btitle"] != '' ) {
			$title = $PARM["btitle"] . $TITLE_ADD;
		} else {
			$title = $PARM["title"] . $TITLE_ADD;
		}

		$PARM["title"] = $title;

		$this->addTemplateParameter("title", $title);
		$this->addTemplateParameter("desc", $desc);

		$uri       = $this->httpRequest->getUrl()->withQuery(["seolink" => null]); //$URL->getAbsoluteUrl();
		$CANONICAL = preg_replace( "~/page-[0-9]+~", "", (string) $uri);
		$MAINDOMAIN = str_replace( "/", "", substr( $this->container->parameters[ "shop"][ "basehref"], - 3 ) );

		if ( isset( $PARM["canonical"] ) && $PARM["canonical"] != "" ) {
			$CANONICAL = $PARM["canonical"];
		}
		if (
			($this->router !== null && isset($this->router->query["page"])) ||
			$this->httpRequest->getQuery("keywords") ||
			$this->httpRequest->getQuery("ids") ||
			$this->httpRequest->getQuery("cena_od") ||
			$this->httpRequest->getQuery("cena_do") ||
			$this->httpRequest->getQuery("filter") ||
			$this->httpRequest->getQuery("brand")
		) {
			$PARM["robots"] = "noindex,follow";
		}

		if (
			(isset($_SERVER["HTTP_HTTPS"] ) && $_SERVER["HTTP_HTTPS"] == "on") ||
			(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") ||
			(isset($_SERVER["REQUEST_SCHEME"]) && $_SERVER["REQUEST_SCHEME"] == "https")
		) {

			$uri = $uri->withScheme("https");
			$CANONICAL = (string) (new UrlScript($CANONICAL))->withScheme("https");
		}

		$HEAD = [
			"title" => $title,
			"desc" => $desc,
			"uri" => (string) $uri,
			"canonical"  => $CANONICAL,
			"maindomain" => $MAINDOMAIN,
		];

		$this->addTemplateParameter("HEAD", $HEAD);
		$this->addTemplateParameter("PARM", $PARM);
		$this->addTemplateParameter("HTML", ['class' => '' , 'add' => '']);
	}

	public function setDefaultTemplateParameters() {
		$this->addTemplateParameter("shopName", $this->container->parameters["shop"]["name"]);
		$this->addTemplateParameter("basePath", $this->container->parameters["shop"]["relurl"]);
		$this->addTemplateParameter("baseUrl", $this->container->parameters["shop"]["basehref"]);

		$this->addTemplateParameter("HOST", $this->httpRequest->getUrl()->getHost());
		$this->addTemplateParameter("detect", new MobileDetect());
		$this->addTemplateParameter("cookiesBar", $this->getCookiesBar());
	}

	public function getDefaultTemplateParameters() : array {
		return $this->templateParams;
	}

	protected function sendResponse(){
		$template = $this->page->template();

		$this->httpResponse->setCode($this->responseCode);

		$this->defaultMeta();


		$this->addTemplateParameter("BOX", $this->boxes);
		$this->addTemplateParameter("microdata", $this->container->getByType(Microdata::class));

		$templateParameters = $this->getDefaultTemplateParameters();

		$this->container->getByType(Engine::class)->render(
			WWW_DIR . $template->src ,
			$this->templateParams
		);
	}
	
	public function run() {
		$this->onStart($this, $this->httpRequest, $this->httpResponse);
		if(!empty($this->container->findByType(\Commerce\Routing\RouterFactory::class))){
			$this->processInitialRequest();
		} else {
			$this->processRequest();
		}
		$this->onRequest($this, $this->httpRequest, $this->httpResponse);
		$this->sendResponse();
		$this->onShutdown($this);
	}

	public function addTemplateParameter(string $name, $value) : self {
		$this->templateParams[$name] = $value;
		return $this;
	}
	/**
	 * @return Router
	 */
	public function getRouter(): ?Routing\Router {
		return $this->router;
	}

	public function getCookiesBar() : array {
		$cookie = [];
		$cookie["setted"] = (bool) $this->httpRequest->getCookie("cookies_setted");
		$cookie["technical"] = (bool)$this->httpRequest->getCookie("allow_technical_cookies");
		$cookie["analytics"] = (bool)$this->httpRequest->getCookie("allow_analytics_cookies");
		$cookie["marketing"] = (bool)$this->httpRequest->getCookie("allow_marketing_cookies");
		$cookie["scripts"]["css"] = file_get_contents(__DIR__."/../assets/kubomikita.cookiebar.css");
		$cookie["scripts"]["js"] = file_get_contents(__DIR__."/../assets/kubomikita.cookiebar.js");

		return $cookie;
	}

	protected function setDefaultMicrodata(){
		$this->container->getByType(Microdata::class)->organization = [
			"url" => $this->container->parameters["shop"]["url"],
			"logo" => $this->container->parameters["shop"]["url"]."assets/img/logo.png",
			"contact" => Microdata::internationalPhoneNumber($this->container->parameters["shop"]["telefon"]),
			"profiles" => [
				"https://www.facebook.com/1.firstday/"
			]
		];
		$this->container->getByType(Microdata::class)->website = [
			"url" => $this->container->parameters["shop"]["url"],
			"author" => $this->container->parameters["shop"]["name"],
			"search" => [
				"action" => $this->container->parameters["shop"]["url"]."vyhladavanie?keywords={keywords}",
				"input" => "required name=keywords"
			]
		];
	}

}