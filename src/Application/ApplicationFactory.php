<?php

namespace Kubomikita\Commerce;

use Nette\Http\Request;

final class ApplicationFactory {

	public static function create(Request $request) : ApplicationInterface{
		return new Application($request);
	}
	//public function run();
}