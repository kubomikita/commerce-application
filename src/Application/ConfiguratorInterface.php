<?php

namespace Kubomikita\Commerce;

interface ConfiguratorInterface {
	/**
	 * @param Page Page
	 * @param AppRouter Router
	 * @return string - renders to output
	 */
	//public function run($Page, $Router);
	public function setTempDirectory($path);
	public function setDebugMode($value);
	public function isDebugMode();
	public function isCli();
	public function isHost($host);
	public function enableDebugger($logDirectory = null, $email = null);
	public function getService($key);

	/**
	 * @param $section
	 * @param null $key
	 *
	 * @return array|mixed
	 */
	public function getParameter($section,$key = null);

	/**
	 * @param $section
	 * @param $value
	 *
	 * @return ConfiguratorInterface
	 */
	public function setParameter($section,$value);
	/**
	 * Check if app runing on localhost or production server
	 * @return bool
	 */
	public function isProduction();

	/**
	 * Checks if app is protein.sk or protein.cz
	 * @return bool
	 */
	public function isProtein();

	public function getByType(string $class);
}