<?php
class CommerceDB {
	static $DB;

	/**
	 *
	 * @return bool
	 * @throws Exception
	 */
	static public function connect(array $db){

		if(CommerceDB::$DB === null) {
			if ( $db["socket"] != "" ) {
				CommerceDB::$DB = mysqli_connect( null, $db["user"], $db["password"], $db["name"], null, $db["socket"] );
			} else {
				CommerceDB::$DB = mysqli_connect( $db["host"], $db["user"], $db["password"], $db["name"] );
			}

			mysqli_query( CommerceDB::$DB, "set character set utf8" );
			mysqli_query( CommerceDB::$DB, "set names 'utf8'" );
			mysqli_query( CommerceDB::$DB, "set collation_connection = utf8_unicode_ci" );
		}
		//mysqli_select_db(Config::dbDbase);
		return true;
	}


}


function echelon_db_connect(){
	return CommerceDB::$DB;
}
function folder_access(){
	return true;
}



