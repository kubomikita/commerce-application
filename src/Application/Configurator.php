<?php
namespace Kubomikita\Commerce;

use Composer\Autoload\ClassLoader;
use Kubomikita\Commerce\Model\Loader;
use Kubomikita\Commerce\Routing\Router;
use Kubomikita\FormFactory;
use Nette;
use Nette\DI;
use Nette\SmartObject;
use Tracy;
use Nette\DI\Container;
use Nette\InvalidStateException;
use Nette\Loaders\RobotLoader;
use Kubomikita\Service;
use Nette\Application\AbortException;
use Nette\Neon\Neon;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;
use Tracy\Debugger;

/**
 * @property-read Router $router
 */
class Configurator implements ConfiguratorInterface {

	use SmartObject;

	/** @var \CommerceController[] */
	public $controllers = [];
	const COOKIE_SECRET = "commerce_debug";
	/** @var callable[]  */
	public $onCompile;
	/** @var callable[] */
	public $onRoute;
	/** @var callable[] */
	public $onEnableDebugger;
	/** @var array */
	protected $parameters;
	/** @var array  */
	protected $files = [];
	/** @var array */
	protected $routers = [
		"cli" => Routing\Cli\BaseRouter::class,
		"redirects" => null,
		"ajax" =>  Routing\Ajax\Router::class,
		"api" => Routing\Api\Router::class,
	];
	/** @var Container */
	public $container;
	/** @var bool */
	protected $debuggerEnabled = false;
	/** @var string */
	protected $debuggerPanelService = TracyPanelService::class;
	/** @var string[] of classes which shouldn't be autowired */
	public $autowireExcludedClasses = [\ArrayAccess::class, \Countable::class, \IteratorAggregate::class, \stdClass::class, \Traversable::class];


	public function __construct()
	{
		$this->parameters = $this->getDefaultParameters();
		if(!defined("APP_DIR") || !defined("WWW_DIR") || !defined("ASSETS_DIR") || !defined("TEMP_DIR")){
			throw new AbortException("Main dir paths is not defined!");
		}
		$this->onCompile[] = function(){
			if($this->container->hasService("session")) {
				$this->container->getService( "session" )->start();
			}
			$this->container->getByType(Loader::class)->load();
			$this->container->getByType(\Kubomikita\Commerce\ImgCacheFactory::class)->initialize();
		};
		$this->onRoute[] = function() {
			if($this->container->hasService("form.factory")){
				$this->container->getByType(FormFactory::class)->register();
			}
			$this->controllers = $this->container->getByType(ControllerService::class)->create();
		};
		$this->onEnableDebugger[] = function () {
			Tracy\Debugger::$errorTemplate = APP_DIR."view/error500.phtml";

			$this->debuggerEnabled = true;
			error_reporting(E_ALL & ~E_NOTICE);
			Tracy\Bridges\Nette\Bridge::initialize();
			if (class_exists(Latte\Bridges\Tracy\BlueScreenPanel::class)) {
				Latte\Bridges\Tracy\BlueScreenPanel::initialize();
			}

			if(!empty(($logger = $this->container->findByType(\Tracy\ILogger::class)))){
				\Tracy\Debugger::setLogger($this->container->getService($logger[0]));
			}

			if(class_exists($this->getDebuggerPanelService())) {
				$class = $this->getDebuggerPanelService();
				/** @var TracyPanelService $tracyPanel */
				$tracyPanel = new $class();
				$tracyPanel->addPanels( $this );
			}
		};
	}

	public function isBackend():bool {
		return Strings::endsWith($this->container->getByType(Nette\Http\Request::class)->getUrl()->getBasePath(), 'commerce/manager/');
	}



	/**
	 * Creates App container
	 * @return Container
	 */
	public function createContainer(): DI\Container
	{
		$class = $this->loadContainer();
		$container = new $class([]);
		$this->container = $container;
		$this->container->addService("configurator", $this);
		$this->parameters = $this->container->parameters;

		$this->initialize();
		return $container;
	}

	/**
	 * Init app resources
	 * @throws \Exception
	 */
	protected function initialize() : void {
		Service::add("context",$this->container);
		Tracy\Debugger::getBar()->addPanel(new Nette\Bridges\DITracy\ContainerPanel($this->container));
		// Set cache storage
		\Cache::setStorage($this->container->getByType(\Nette\Caching\IStorage::class));
		// onCompile event trigger
		$this->onCompile($this->container, $this);
		// Reload cart of registered user
		\Kosik::restore();

	}


	/**
	 * Loads system DI container class and returns its name.
	 */
	public function loadContainer(): string
	{
		$loader = new DI\ContainerLoader(
			$this->getCacheDirectory() . '/nette.configurator',
			$this->parameters['debugMode']
		);
		$class = $loader->load(
			[$this, 'generateContainer'],
			[
				$this->parameters,
				[],
				$this->files,
				PHP_VERSION_ID - PHP_RELEASE_VERSION, // minor PHP version
				class_exists(ClassLoader::class) ? filemtime((new \ReflectionClass(ClassLoader::class))->getFilename()) : null, // composer update
			]
		);
		return $class;
	}


	/**
	 * @internal
	 */
	public function generateContainer(DI\Compiler $compiler): void
	{
		$loader = $this->createLoader();
		$loader->setParameters($this->parameters);

		foreach ($this->files as $config) {
			if (is_string($config)) {
				$compiler->loadConfig($config, $loader);
			} else {
				$compiler->addConfig($config);
			}
		}

		$compiler->addConfig(['parameters' => $this->parameters]);
		$builder = $compiler->getContainerBuilder();
		$builder->addExcludedClasses($this->autowireExcludedClasses);

		//$this->onCompile($this, $compiler);
	}


	protected function createLoader(): DI\Config\Loader
	{
		return new DI\Config\Loader;
	}

	public function getRouters(){
		return $this->routers;
	}
	/**
	 * Creates routers
	 * @param array $list
	 *
	 * @throws Nette\Application\BadRequestException
	 */
	public function createRouter(array $list = ["api","cli","ajax"]){

		$this->onRoute($this);

		if($this->isCli()){
			if(!in_array("cli",$list)){
				throw new AbortException("Cli Router not defined.");
			}
		}
		/** @var RouterFactory $routerFactory */
		$routerFactory = $this->container->getByType(RouterFactory::class);
		foreach($list as $key){
			if(!isset($this->routers[$key])){
				throw new AbortException("Router with '$key' not exists.",500);
			}
			if($this->routers[$key] !== null) {
				$selectedRouter = $routerFactory->getByType( $this->routers[ $key ] );
			} else {
				throw new AbortException("Router with key '$key' is not defined.");
			}
		}
	}



	public function createFunctionsLoader(array $list){
		foreach ( $list as $file )
		{
			if(!file_exists($file)){
				throw new \Exception("File '$file' not exists.");
			}
			include_once $file;
		}
	}

	/**
	 * checks if shop is protein
	 * @return bool
	 */
	public function isProtein(){
		$url = $this->parameters["shop"]["url"];
		return Strings::contains($url,"protein");
	}

	/**
	 * Is host
	 * @param $host
	 *
	 * @return bool
	 */
	public function isHost($host){
		/**
		 * only for debug
		 */
		if(!$this->isProduction()) {
			if ( $host == "protein.cz" ) {
				//return true;
			}
			if ( $host == "protein.sk" ) {
				return true;
			}
		} else {
			if(strpos($this->parameters["host"],$host) !== false){
				return true;
			}
		}
		return false;
	}

	/**
	 * Is console mode
	 * @return bool
	 */
	public function isCli(){
		return $this->parameters["consoleMode"];
	}

	/**
	 * Set parameter %debugMode%.
	 * @param  bool|string|array
	 * @return self
	 */
	public function setDebugMode($value)
	{
		if (is_string($value) || is_array($value)) {
			$value = static::detectDebugMode($value);
		} elseif (!is_bool($value)) {
			throw new Nette\InvalidArgumentException(sprintf('Value must be either a string, array, or boolean, %s given.', gettype($value)));
		}
		$this->parameters['debugMode'] = $value;
		$this->parameters['productionMode'] = !$this->parameters['debugMode']; // compatibility
		$this->parameters['environment'] = $this->parameters['debugMode'] ? 'development' : 'production';
		return $this;
	}


	/**
	 * @return bool
	 */
	public function isDebugMode()
	{
		return $this->parameters['debugMode'];
	}
	//public function is


	/**
	 * Sets path to temporary directory.
	 * @return self
	 */
	public function setTempDirectory($path)
	{
		$this->parameters['tempDir'] = $path;
		if(!file_exists($this->parameters["tempDir"])){
			FileSystem::createDir($this->parameters["tempDir"]);
		}
		$this->parameters["logDir"] = $path."/log";
		if(!file_exists($this->parameters["logDir"])){
			FileSystem::createDir($this->parameters["logDir"]);
		}
		return $this;
	}



	protected function getDefaultParameters()
	{
		$trace = debug_backtrace(PHP_VERSION_ID >= 50306 ? DEBUG_BACKTRACE_IGNORE_ARGS : FALSE);
		$last = end($trace);
		$debugMode = static::detectDebugMode();
		return array(
			'appDir' => isset($trace[1]['file']) ? dirname($trace[1]['file']) : NULL,
			'wwwDir' => isset($last['file']) ? dirname($last['file']) : NULL,
			'debugMode' => $debugMode,
			'productionMode' => !$debugMode,
			'environment' => $debugMode ? 'development' : 'production',
			'consoleMode' => PHP_SAPI === 'cli',
			"host" => (PHP_SAPI === "cli") ? "cli" : $_SERVER["SERVER_NAME"],
			'container' => array(
				'class' => NULL,
				'parent' => NULL,
			),
		);
	}

	/**
	 * @param $key [database,latte]
	 *
	 * @return mixed
	 */
	public function getService($key){
		return $this->container->getService($key);
	}

	/**
	 * @param string $type
	 *
	 * @return object|string|null
	 */
	public function getByType($type){
		return $this->container->getByType($type);
	}

	/**
	 * @param  string        error log directory
	 * @param  string        administrator email
	 * @return void
	 */
	public function enableDebugger($logDirectory = NULL, $email = NULL)
	{
		if($email !== null){
			$this->setParameter("adminEmail",$email);
		}
		//Tracy\Debugger::$strictMode = TRUE;
		Tracy\Debugger::enable(!$this->parameters['debugMode'], $logDirectory, $email);

		$this->onEnableDebugger($this);
	}



	/**
	 * @return RobotLoader
	 * @throws Nette\NotSupportedException if RobotLoader is not available
	 */
	public function createRobotLoader()
	{
		if (!class_exists('Nette\Loaders\RobotLoader')) {
			throw new Nette\NotSupportedException('RobotLoader not found, do you have `nette/robot-loader` package installed?');
		}

		$loader = new RobotLoader;
		$loader->setTempDirectory($this->getCacheDirectory());
		//$loader->autoRebuild = $this->parameters['debugMode'];
		return $loader;
	}
	public function getCacheDirectory()
	{
		if (empty($this->parameters['tempDir'])) {
			throw new InvalidStateException('Set path to temporary directory using setTempDirectory().');
		}
		$dir = $this->parameters['tempDir'] . '/cache';
		if (!is_dir($dir)) {
			@FileSystem::createDir($dir); // @ - directory may already exist
		}
		return $dir;
	}
	/**
	 * Detects debug mode by IP address.
	 * @param  string|array  IP addresses or computer names whitelist detection
	 * @return bool
	 */
	public static function detectDebugMode($list = NULL)
	{
		if(isset($_SERVER["HTTP_X_REAL_IP"])){
			$addr = $_SERVER["HTTP_X_REAL_IP"];
		} else {
			$addr = isset( $_SERVER['REMOTE_ADDR'] )
				? $_SERVER['REMOTE_ADDR']
				: php_uname( 'n' );
		}
		$secret = isset($_COOKIE[self::COOKIE_SECRET]) && is_string($_COOKIE[self::COOKIE_SECRET])
			? $_COOKIE[self::COOKIE_SECRET]
			: NULL;
		$list = is_string($list)
			? preg_split('#[,\s]+#', $list)
			: (array) $list;
		if (!isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$list[] = '127.0.0.1';
			$list[] = '::1';
		}
		return in_array($addr, $list, TRUE) || in_array("$secret@$addr", $list, TRUE);
	}

	/**
	 * @return array
	 * @throws Nette\Application\BadRequestException
	 */
	protected function cookiesBar(){
		$REQUEST = $this->getService("request");
		$cookie = [];
		$cookie["setted"] = (bool) $REQUEST->getCookie("cookies_setted");
		$cookie["technical"] = (bool)$REQUEST->getCookie("allow_technical_cookies");
		$cookie["analytics"] = (bool)$REQUEST->getCookie("allow_analytics_cookies");
		$cookie["marketing"] = (bool)$REQUEST->getCookie("allow_marketing_cookies");
		$cookie["scripts"]["css"] = file_get_contents(__DIR__."/../assets/kubomikita.cookiebar.css");
		$cookie["scripts"]["js"] = file_get_contents(__DIR__."/../assets/kubomikita.cookiebar.js");

		return $cookie;
	}

	public function addConfig($config)
	{
		$this->files[] = $config;
		return $this;
	}

	/**
	 * @param $section
	 * @param null $key
	 *
	 * @return mixed
	 * @throws AbortException
	 */
	public function getParameter($section,$key = null){
		if(!isset($this->container->parameters[$section])){
			trigger_error("Section '$section' in parameters not exists", E_USER_NOTICE);
			return null;
		}
		if(!isset($this->container->parameters[$section][$key]) and $key !== null){
			trigger_error("Key '$key' in section '$section' not exists", E_USER_NOTICE);
			return null;
		}
		if($key !== null){
			return $this->container->parameters[$section][$key];
		}
		return $this->container->parameters[$section];
	}


	/**
	 * @param $section
	 * @param $value
	 *
	 * @return $this|ConfiguratorInterface
	 */
	public function setParameter( $section, $value) {
		$this->container->parameters[$section]= $value;
		return $this;
	}

	/**
	 * Check if app runing on localhost or production server
	 * @return bool
	 */
	public function isProduction(){
		if(!$this->isCli()) {
			return ( $_SERVER["HTTP_HOST"] != "localhost" );
		} else {
			if(strpos(__DIR__,"E:\\www") !== false){
				return false;
			}
		}
		return true;
	}

	public function getRouter() : Router {
		return ($router = $this->container->getByType(ApplicationInterface::class)->router) !== null ? $router : new Router();
	}

	public function isDebuggerEnabled() :bool {
		return $this->debuggerEnabled;
	}

	public function getDebuggerPanelService() : string {
		return $this->debuggerPanelService;
	}

	/**
	 * Set class name of CLI router
	 * @param $class string
	 */
	public function setCliRouter($class){
		$this->routers["cli"] = $class;
	}

	/**
	 * Set class name of API router
	 * @param $class string
	 */
	public function setApiRouter($class){
		$this->routers["api"] = $class;
	}

	/**
	 * Set class name of Application router
	 * @param $class string
	 */
	public function setAppRouter($class){
		$this->routers["application"] = $class;
	}
	public function setAjaxRouter($class){
		$this->routers["ajax"] = $class;
	}
	public function setRedirectsRouter($class){
		$this->routers["redirects"] = $class;
	}

}