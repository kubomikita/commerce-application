<?php

namespace Kubomikita\Commerce\DataLayer;

use Nette\Application\AbortException;
use Nette\Utils\Html;

abstract class DataLayer {
	protected $action = "window.dataLayer.push(%s)";
	protected $output = [];
	protected $outputType = Container::OUTPUT_JSON;
	/** @var string */
	protected $insertBefore = '';
	/** @var string */
	protected $insertAfter = '';
	/** @var string */
	protected $insertBeforeScript = '';

	/**
	 * @return string
	 */
	public function getAction(): string {
		return $this->action;
	}

	/**
	 * @param $action
	 *
	 * @return DataLayer
	 */
	public function setAction($action) : DataLayer{
		$this->action = $action;
		return $this;
	}

	/**
	 * @param int $outputType
	 */
	public function setOutputType( int $outputType ) {
		$this->outputType = $outputType;
	}

	/**
	 * @return int
	 */
	public function getOutputType(): int {
		return $this->outputType;
	}

	/**
	 * @param string $insertBefore
	 */
	public function setInsertBefore( string $insertBefore ) {
		$this->insertBefore = $insertBefore;
	}

	/**
	 * @return string
	 */
	public function getInsertBefore(): string {
		return $this->insertBefore;
	}

	/**
	 * @param string $insertAfter
	 */
	public function setInsertAfter( string $insertAfter ) {
		$this->insertAfter = $insertAfter;
	}

	/**
	 * @return string
	 */
	public function getInsertAfter(): string {
		return $this->insertAfter;
	}

	/**
	 * @return array
	 */
	public function getOutput() : array {
		return $this->output;
	}

	abstract public function track() : array;

	/**
	 * @param string $insertBeforeScript
	 *
	 * @return DataLayer
	 */
	public function setInsertBeforeScript( string $insertBeforeScript ): DataLayer {
		$this->insertBeforeScript = $insertBeforeScript;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getInsertBeforeScript(): string {
		return $this->insertBeforeScript;
	}


}