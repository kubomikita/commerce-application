<?php

namespace Kubomikita\Commerce\DataLayer;

use Nette\InvalidArgumentException;
use Nette\Utils\Html;

class Container {
	const CONTAINER_TOP = "top";
	const CONTAINER_BOTTOM = "bottom";
	const CONTAINER_AJAX = "ajax";

	const OUTPUT_JSON = 1;
	const OUTPUT_JSON_OBJECT = 6;
	const OUTPUT_PLAIN = 2;
	const OUTPUT_ARRAY_ROW = 3;
	const OUTPUT_ARRAY_ARGS = 4;
	const OUTPUT_GLAMI = 5;

	private $type = Container::CONTAINER_TOP;
	private $dataLayer = [];
	private $initDataLayer = true;
	private $dataLayerVar = "dataLayer";

	private $counter = 0;

	/**
	 * @var \Nette\DI\Container
	 */
	public $container;
	
	
	public function __construct(\Nette\DI\Container $container) {
		$this->container = $container;
	}

	/**
	 * @return array
	 */
	public function getDataLayer(): array {
		return $this->dataLayer;
	}

	/**
	 * @param string $type
	 *
	 * @return Container $this
	 */
	public function setType( string $type ) :Container {
		$this->type = $type;
		return $this;
	}
	/**
	 * @param bool $initDataLayer
	 *
	 * @return Container $this
	 */
	public function setInitDataLayer( bool $initDataLayer ) : Container {
		$this->initDataLayer = $initDataLayer;
		return $this;
	}

	/**
	 * @return string
	 */
	private function dataLayerInit():string {
		if($this->initDataLayer) {
			return 'window.' . $this->dataLayerVar . ' = window.' . $this->dataLayerVar . ' || [];' . "\n";
		}
		return "";
	}
	public function getContainer() : string {
		$dl = [];

		/** @var DataLayer $DataLayer */
		if(isset($this->dataLayer[$this->type]) && !empty($this->dataLayer[$this->type])) {
			foreach ( $this->dataLayer[ $this->type ] as $DataLayer ) {
				if ( ! is_array( $dl[ $DataLayer->getAction() ] ) ) {
					$dl[ $DataLayer->getAction() ] = ["output" => [], "outputType" => null,"before" => null,"after" => null];
				}
				if($DataLayer->getOutputType() == self::OUTPUT_ARRAY_ARGS ){
					$dl[ $DataLayer->getAction() ]["output"][] = $DataLayer->getOutput();
				} else {
					$dl[ $DataLayer->getAction() ]["output"] += $DataLayer->getOutput();
				}
				//$dl[ $DataLayer->getAction() ]["output"] += $DataLayer->getOutput();
				//
				$dl[ $DataLayer->getAction() ]["outputType"] = $DataLayer->getOutputType();
				$dl[ $DataLayer->getAction() ]["before"] = $DataLayer->getInsertBefore();
				$dl[ $DataLayer->getAction() ]["beforeScript"] = $DataLayer->getInsertBeforeScript();
				$dl[ $DataLayer->getAction() ]["after"] = $DataLayer->getInsertAfter();

				//bdump( $DataLayer->getOutput(), get_class( $DataLayer ) );
			}
			return $this->getScript( $dl );
		}
		return "";
	}

	public function add(DataLayer $dataLayer) : Container{
		$d = $dataLayer->track();
		$this->dataLayer[$this->type][] = $dataLayer;
		if(!empty($d)){
			$this->counter++;
		}
		return $this;
	}

	/**
	 * @param array $dataLayer
	 *
	 * @return string
	 */
	private function getScript(array $dataLayer) : string {
		$html = "\n".$this->dataLayerInit();
		$beforeScript = "";
		foreach($dataLayer as $action => $output){
			$beforeScript = $output["beforeScript"]."\n";
			$html .= $output["before"]."\n";
			if($output["outputType"] == self::OUTPUT_JSON) {
				$html .= sprintf( $action, self::arrayToJsObject( $output["output"] ) ) . ";\n";
			}elseif($output["outputType"] == self::OUTPUT_JSON_OBJECT){
				$html .= sprintf( $action, substr(self::arrayToJsObject( $output["output"] ), 1, -1) ) . ";\n";
			} elseif($output["outputType"] == self::OUTPUT_ARRAY_ROW){
				foreach($output["output"] as $row){
					$html .= sprintf($action, self::arrayToJsArray($row)).";\n";
				}
			} elseif ($output["outputType"] == self::OUTPUT_ARRAY_ARGS){
				foreach($output["output"] as $row){
					//bdump($row);
					$data = [] ;//$row;
					/*foreach($row as $k=>$v) {
						if ( is_array( $v ) ) {
							$data[$k] = self::arrayToJsObject( $v );

						}
					}*/
					foreach($row as $v1) {
						foreach($v1 as $k=>$v) {
							if ( is_array( $v ) ) {
								$data[ $k ] = self::arrayToJsObject( $v );
							} else {
								$data[$k] = $v;
							}
						}
					}
					//bdump($data);
					$html.= sprintf($action, ...$data);
				}
			} elseif ($output["outputType"] == self::OUTPUT_GLAMI){
				//dump($output);
			}
			$html .= $output["after"]."\n";
		}
		$script ="";
		$script .= $beforeScript;
		$script .= Html::el("script")->setAttribute("type","text/javascript")->setHtml($html)->render();

		return $script;
	}

	public function count(){
		return $this->counter;
	}
	public static function arrayToJsArray(array $arr, $json_encode = false) : string {
		if($json_encode) {
			return json_encode($arr);
		} else {
			return '[\''.implode('\', \'',$arr).'\']';
		}

	}
	public static function arrayToJsObject(array $arr, $sequential_keys = false, $quotes = true, $beautiful_json = false) {
		$output = static::isAssoc($arr) ? "{" : "[";
		$count = 0;
		foreach ($arr as $key => $value) {
			if ( static::isAssoc($arr) || (!static::isAssoc($arr) && $sequential_keys == true ) ) {
				$output .= ($quotes ? '"' : '') . $key . ($quotes ? '"' : '') . ' : ';
			}

			if (is_array($value)) {
				$output .= static::arrayToJsObject($value, $sequential_keys, $quotes, $beautiful_json);
			} else if (is_bool($value)) {
				$output .= ($value ? 'true' : 'false');
			} else if (is_numeric($value)) {
				$output .= $value;
			} else {
				$output .= ($quotes || $beautiful_json ? '"' : '') . $value . ($quotes || $beautiful_json ? '"' : '');
			}

			if (++$count < count($arr)) {
				$output .= ', ';
			}
		}

		$output .= static::isAssoc($arr) ? "}" : "]";

		return $output;
	}
	protected static function isAssoc(array $arr) {
		if (array() === $arr) return false;
		return array_keys($arr) !== range(0, count($arr) - 1);
	}

	public function getByType(string $type,bool $silent = false){
		foreach($this->getDataLayer() as $container => $layers){
			foreach($layers as $item){
				if($item instanceof $type){
					return $item;
				}
			}
		}
		if(!$silent) {
			throw new InvalidArgumentException( "Type '$type' of DataLayer not exists" );
		} else {
			return new $type;
		}
	}

	/**
	 * @param string $dataLayerVar
	 *
	 * @return Container
	 */
	public function setDataLayerVar( string $dataLayerVar ): Container {
		$this->dataLayerVar = $dataLayerVar;

		return $this;
}
}