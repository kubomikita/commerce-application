<?php

namespace Kubomikita\Commerce\DataLayer;


class EventTracking  extends DataLayer {

	protected $products = [];
	/** @var \Objednavka|null */
	protected $order;
	protected $pageView;

	public function track(): array {
		// TODO: Implement track() method.
		$output = [];

		$output["ecommerce"]["currencyCode"] = "EUR";
		$output["ecommerce"]["pageView"] = $this->pageView;

		$i = 0;
		/** @var \Tovar $t */
		foreach($this->getProducts() as $t){
			$output["ecommerce"]["impressions"][] = [
				"id" => $t->id,
				"name" => addslashes((string) $t->nazov),
				"price" => \Format::money_user_plain($t->predajna_cena('')->suma()),
				"available" => $t->sklad_available() > 0 ? 'Na sklade' : 'Vypredané',
				"category" => $t->primarna_kategoria->nazov,
				"brand" => $t->vyrobca->nazov,
				"position" => $i,
			];
			$i++;
		}

		return $this->output = $output;
	}

	private function getProducts(){
		//if(count($this->products) > 1) {
			$ids = [];
			foreach ( $this->products as $p ) {
				if($p instanceof \KosikItem || $p instanceof \ObjednavkaItem){
					$ids[] = $p->tovar;
				} else {
					$ids[] = $p;
				}
			}

			return $ids;
		/*}
		if($this->products[0] instanceof \KosikItem || $this->products[0] instanceof \ObjednavkaItem){
			return $this->products[0]->tovar;
		} else {
			return $this->products[0];
		}*/

	}

	/**
	 * @param mixed $pageView
	 */
	public function setPageView( $pageView ): void {
		$this->pageView = $pageView;
	}

	/**
	 * @param array $products
	 */
	public function setProducts( array $products ): void {
		$this->products = $products;
	}

	/**
	 * @param \Objednavka|null $order
	 */
	public function setOrder( ?\Objednavka $order ): void {
		$this->order = $order;
	}
}