<?php

namespace Kubomikita\Commerce\DataLayer;

use Nette\Application\AbortException;
use Nette\Utils\Html;

class OwlcureDataLayerTracking  extends DataLayer {

	/** @var array */
	protected $data;

	/**
	 * @param array $data
	 */
	public function setData( array $data ) :void {
		$this->data = $data;
	}

	public function track() : array {
		$this->output = $this->data;
		return $this->output;
	}



}