<?php

namespace Kubomikita\Commerce\DataLayer;

use Kubomikita\Commerce\ShopConfigurator;
use Nette\Application\AbortException;
use Nette\Utils\Html;

class EcommerceTracking  extends DataLayer {
	/** @var \Objednavka */
	private $order;
	private $prefix = "transaction";



	public function __construct(\Objednavka $O) {
		if(!($O->id > 0)){
			throw new AbortException("Order not exists.");
		}
		$this->order = $O;
	}

	public function setPrefix($prefix = null){
		$this->prefix = $prefix;
		return $this;
	}



	/**
	 * Outputs Tracking code
	 * @return string
	 */
	public function track() : array{
		$suma_spolu = \Format::money_user_plain($this->order->suma_spolu_bez_dopravy()/1.2);
		$suma_dph = \Format::money_user_plain($this->order->suma_spolu_bez_dopravy())-$suma_spolu;
		$doprava = \Format::money_user_plain($this->order->logistika->cena->suma());

		$this->output[$this->prefix."Id"] = $this->order->cislo;
		$this->output[$this->prefix."Affiliation"] = \Registry::get("container")->getParameter("shop","name");
		$this->output[$this->prefix."Total"] = $suma_spolu;
		$this->output[$this->prefix."Tax"] = \Format::money_user_plain($suma_dph);
		$this->output[$this->prefix."Shipping"] = $doprava;
		/** @var \ObjednavkaItem $oi */
		foreach($this->order->items() as $oi) {
			$cena = $oi->cena->multiply($this->order->zlava_koef())->zaklad();
			$this->output[ $this->prefix . "Products" ][] = [
				"sku" => $oi->tovar->id,
				"name" => addslashes((string) $oi->tovar->nazov),
				"category" => (string) $oi->tovar->primarna_kategoria->nazov,
				"price" => \Format::money_user_plain($cena),
				"quantity" => $oi->mnozstvo_objednane
			];
		}
		$this->output[$this->prefix."PromoCode"] = $this->order->metadata["promo_kod"];

		/** @var Partner $UL */
		$db = \Registry::get("database");
		$r = $db->query("SELECT id FROM ec_objednavky WHERE klient=? ORDER BY datum DESC LIMIT 1 OFFSET 1", $this->order->klient->id)->fetch();
		$oid = (int) $r["id"];
		$uobj = new \Objednavka($oid);
		$lastOrder = null;
		if((int) $uobj->id > 0){
			$uobj_date = $uobj->datum->show("Y-m-d H:i:s");
			$lastOrder = $uobj_date;
		}

		$partner = $this->order->klient;

		$this->output[$this->prefix."LastOrder"] = $lastOrder;
		try {
			/** @var ShopConfigurator $configurator */
			$configurator = \Registry::get("configurator");
			$dognet = $configurator->getParameter( "dognet" );
			if($dognet !== null) {
				if ( $partner->hasVO() || $partner->hasVIP() ) {
					$this->output[ $this->prefix . "CampaignID" ] = $dognet["campaignIdVo"];
				} else {
					$this->output[ $this->prefix . "CampaignID" ] = $dognet["campaignId"];
				}
			}
		} catch (\Exception $e){

		}
		return $this->output;
/*
		$html = $this->dataLayerInit();
		$html .= sprintf($this->action,static::arrayToJsObject($this->output)).";";
		$script = Html::el("script")->setAttribute("type","text/javascript")->setHtml($html);
		bdump($this->getOutput(),"EcommerceTracking");
		return $script->render();*/
	}

}