<?php

namespace Kubomikita\Commerce\DataLayer;

use Nette\Application\AbortException;
use Nette\Utils\Html;

class OwlcureTracking  extends DataLayer {
	protected $event;
	/** @var array */
	protected $data;

	/**
	 * @param string $event
	 */
	public function setEvent( string $event ) :void {
		$this->event = $event;
	}

	/**
	 * @param array $data
	 */
	public function setData( array $data ) :void {
		$this->data = $data;
	}

	public function track() : array {
		$this->setAction("owlFly('%s',%s);");
		$this->setOutputType(Container::OUTPUT_ARRAY_ARGS);

		if($this->event !== null && !empty($this->data)) {
			$this->output[] = [ $this->event, $this->data ];
		}

		return $this->output;
	}



}