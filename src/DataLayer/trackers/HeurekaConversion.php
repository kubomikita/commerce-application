<?php

namespace Kubomikita\Commerce\DataLayer;

use Nette\Application\AbortException;
use Nette\Utils\Html;

class HeurekaConversion  extends DataLayer {
	/** @var \Objednavka */
	private $order;

	/**
	 * HeurekaConversion constructor.
	 *
	 * @param \Objednavka $O
	 *
	 * @throws AbortException
	 */
	public function __construct(\Objednavka $O) {
		if(!($O->id > 0)){
			throw new AbortException("Order not exists.");
		}
		$this->order = $O;
	}
	public function track(): array {
		$this->setAction("_hrq.push(%s)");
		$this->setOutputType(Container::OUTPUT_ARRAY_ROW);

		if(\Env::heureka("conversion") != "") {
			$this->insertBefore = 'var _hrq = _hrq || [];';
			$this->insertAfter  = '(function() {
    var ho = document.createElement(\'script\'); ho.type = \'text/javascript\'; ho.async = true;
    ho.src = \''.\Env::heureka("conversionScript").'\';
    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ho, s);
})();';

			$this->output[] = [ "setKey", \Env::heureka("conversion") ];
			$this->output[] = [ "setOrderId", $this->order->cislo ];
			foreach ( $this->order->items() as $item ) {
				$this->output[] = [
					"addProduct",
					addslashes((string) $item->tovar->nazov),
					\Format::money_user_plain( $item->cena->multiply( $this->order->zlava_koef() )->suma() ),
					$item->mnozstvo_objednane
				];
			}
			$this->output[] = [ "trackOrder" ];
		}
		// TODO: Implement track() method.
		return $this->output;
	}

}

/*<script type="text/javascript">
var _hrq = _hrq || [];
    _hrq.push(['setKey', 'CA10678092CD9E9215FFB9513021DC5A']);
    _hrq.push(['setOrderId', 'CISLO_OBJEDNAVKY']);
    _hrq.push(['addProduct', 'NAZEV_PRODUKTU', 'CENA_ZA_KUS', 'POCET_KUSU']);
    _hrq.push(['addProduct', 'NAZEV_PRODUKTU', 'CENA_ZA_KUS', 'POCET_KUSU']);
    _hrq.push(['trackOrder']);

(function() {
    var ho = document.createElement('script'); ho.type = 'text/javascript'; ho.async = true;
    ho.src = 'https://im9.cz/sk/js/ext/2-roi-async.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ho, s);
})();
</script>*/