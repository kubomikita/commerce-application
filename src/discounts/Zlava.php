<?php
class Zlava {
	use StandardDiscount;
	public $id;
	public $suma_od = 0;
	public $zlava = 0;
	public $code = '';
	public $suma = 0;
	public $card;

	public $kod;
	public $title;

	
	public function hasMinus() :bool {
		return $this->suma > 0;
	}

	public function format(){
		$ret=sprintf("%.2F",$this->zlava).'%';
		return $ret;
	}

	public function koef(){
		$ret=(100-$this->zlava)/100;
		return $ret;
	}

	public function title(){
		if($this->kod !== null) {
			return '<div class="tx-12"><a class="text-danger" href="kosik?do=deleteCupon&presenter=Cart_controller&code='.$this->kod.'"><i class="fas fa-times"></i></a> ' . LangStr( $this->title ) . '</div>';
		} elseif($this->isZlava()){
			return $this->title;
		}
		return '';
	}
	
	public function titlefa(){ return LangStr('ZĽAVA'); }
	public static function make($code,$zlava,$suma=0){
		$Z = new Zlava();
		if($code=='M'){ $Z = new MnozstevnaZlava(); };
		if($code=='V'){ $Z = new VernostnaZlava(); };
		if($code=='W'){ $Z = new VipZlava(); };
		if($code=="DP"){$Z = new DarcekovaPoukazka();}
		if($code=="MD"){$Z = new MultiDiscount([]);}
		$Z->zlava=$zlava;
		$Z->suma = $suma;
		return $Z;
	}

	public function minus() {
		return 0;
	}

	public function checkCupons(): array {
		$messages = [];
		if($this->card !== null && (!Auth::User() || (Auth::$User->vernostnaKarta()->cislo !== $this->card))){
			$messages["danger"][$this->kod] = '1day Club kupón [<strong>'.$this->kod.'</strong>] je možné použiť len s klub kartou pre ktorú bol vydaný.';
			$messages["success"][$this->kod] = null;
			$this->unsetZlava($this->kod);
		}
		return $messages;
	}
	public function unsetZlava(string $kod = null) : void {
		Kosik::$ZLAVA = new NullZlava();
	}
}