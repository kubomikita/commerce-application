<?php
class LoyalityDiscount extends Zlava {}
class LoaylityDiscountActiveRecord extends ActiveRecord {
	protected static $table = "ec_vernostnezlavy";
}

class VernostnaZlava extends LoaylityDiscountActiveRecord {
	use StandardDiscount;

	public $id;
	/**
	 * @var suma
	 * @source suma_od
	 */
	public $suma = 0;
	public $suma_od = 0;
	public $zlava = 0;
	/** @var serialized */
	private $metadata;
	/**
	 * @global $code
	 * @write true
	 */
	public $code = 'V';

	public function __construct( $data = null ) {
		parent::__construct( $data );
		//$this->suma = $this->suma_od;
	}
	public function beforeSave() {
		//$this->suma = $this->suma_od;
	}

	public function checkCupons() : array {
		return [];
	}


	public static function fetch(){
		$ret=array();
		$db = Registry::get("database");
		if(Cache::check(static::getCacheGroup(),'_list')){ $ret=Cache::get(static::getCacheGroup(),'_list'); } else {
			$q = $db->query("select * from ".static::$table." order by suma_od asc");
			foreach($q as $R){
				$ret[]=new self($R);
			}
			Cache::put(static::getCacheGroup(),'_list',$ret);
		}
		return $ret;
	}

	public static function get($suma){
		$ret=new VernostnaZlava();
		$instance = static::getCacheGroup()."_get";
		$db = Registry::get("database");
		if(!isset(static::$instances[$instance][$suma])) {
			$R = $db->query( "select * from " . static::$table . " where suma_od <= ? order by suma_od desc limit 1",
				(float) $suma )->fetch();
			if ( $R ) {
				$ret = new VernostnaZlava( $R );
			};
			static::$instances[$instance][$suma] = $ret;
		} else {
			$ret = static::$instances[$instance][$suma];
		}
		return $ret;
	}

	public function format(){
		$ret=sprintf("%.2F",$this->zlava).'%';
		return $ret;
	}

	public function koef(){
		$ret=(100-$this->zlava)/100;
		return $ret;
	}

	public function title(){ return LangStr('VERNOSTNÁ ZĽAVA'); }
	public function titlefa() { return "VERNOSTNÁ ZĽAVA";}
}