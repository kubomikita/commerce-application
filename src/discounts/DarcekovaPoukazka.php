<?php

class DarcekovaPoukazka extends Zlava {
	use StandardDiscount;

	public $id = 0;
	public $suma_od = 0;
	public $suma = 0;
	public $zlava = 0;
	public $code = "DP";

	public $cupon = null;

	public $title = 'Darčeková poukážka';

	public function __construct() {

	}


	public function titlefa() { return "Vernostná zľava";}
	public function format(){
		return Format::money_user($this->suma);
	}
	public function minus() : float{
		if(Kosik::total_inclVAT() >= $this->suma_od) {
			return -$this->suma;
		}
		return 0;
	}


}