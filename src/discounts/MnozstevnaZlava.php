<?php

class MnozstevnaZlava extends QuantityDiscountActiveRecord {
	use StandardDiscount;

	protected static $table = "ec_mnozstevnezlavy";
	public $id;
	//public $suma_od;
	public $zlava = 0;
	/** @var serialized */
	public $metadata;
	/**
	 * @global $code
	 * @write true
	 */
	public $code = 'M';
	/**
	 * @global $promo
	 * @write true
	 */
	public $promo;
	/**
	 * @var suma
	 * @source suma_od
	 */
	public $suma = 0;
	public $suma_od = 0;

	private $title = 'MNOŽSTEVNÁ ZĽAVA';

	public function __construct( $data = null ) {
		parent::__construct( $data );
		$this->suma = $this->suma_od;
	}
	public function beforeSave() {
		$this->suma = $this->suma_od;
	}

	public static function fetch(){
		$ret=array();
		$db = Registry::get("database");
		if(Cache::check(static::getCacheGroup(),'_list')){ $ret=Cache::get(static::getCacheGroup(),'_list'); } else {
			$q = $db->query("select * from ".static::$table." order by suma_od asc");
			foreach($q as $R){
				$ret[]=new self($R);
			}
			Cache::put(static::getCacheGroup(),'_list',$ret);
		};
		return $ret;
	}

	public static function get($suma){
		$ret=new MnozstevnaZlava();
		if(isset(self::$instances["get"][$suma])){
			$ret = self::$instances["get"][$suma];
		} else {
			$db = Registry::get( "database" );
			$R  = $db->query( "select * from " . static::$table . " where suma_od <= ? order by suma_od desc limit 1",
				(float) $suma )->fetch();
			if ( $R ) {

				$ret = self::$instances["get"][$suma] = new MnozstevnaZlava( $R );

			};
			self::$instances["get"][$suma] = $ret;
		}
		return $ret;
	}

	public function format(){
		$ret=sprintf("%.2F",$this->zlava).'%';
		return $ret;
	}

	public function koef(){
		$ret=(100-$this->zlava)/100;
		return $ret;
	}

	public function setTitle($text){
		$this->title = $text;
	}
	public function title(){
		if($this->promo instanceof PromoKod) {
			return "Zľavový kod: ".$this->promo->kod;
		}
		return $this->title;
	}
	public function titlefa() { return "Množstevná zľava";}
}