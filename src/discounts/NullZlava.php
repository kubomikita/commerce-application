<?php

class NullZlava extends Zlava {
	use StandardDiscount;
	public $id;
	public $suma_od;
	public $suma = 0;
	public $zlava = 0;
	public $code;

	public function format() {
		return '-';
	}
}
