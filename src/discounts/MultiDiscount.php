<?php

class MultiDiscount extends Zlava {
	protected $zlavy = [];
	protected $titles = [];
	protected $sumy = [];

	public $code = "MD";


	/**
	 * MultiDiscount constructor.
	 *
	 * @param Zlava[] $zlavy
	 */
	public function __construct(array $zlavy) {
		//bdump(debug_backtrace(), "multidiscount");

		foreach ($zlavy as $zlava){
			$this->zlavy[$zlava->kod] = $zlava;
		}

		$total_inclVAT = Kosik::total_inclVAT();
		foreach ($this->zlavy as $zlava){
			//if($total_inclVAT >= $zlava->suma_od) {
				$this->sumy[$zlava->kod] = (float) $zlava->suma;
				$this->titles[$zlava->kod] = $zlava->title();
			//}
		}
		$this->suma = array_sum($this->sumy);
	}



	public function title() {
		if(empty($this->titles)){
			return 'Kupóny';
		}
		return '<div>'.implode('</div><div>', $this->titles).'</div>';
	}


	public function minus() {
		return -array_sum($this->sumy);
	}

	public function format() {
		return '';
	}

	public function checkCupons(): array {
		Kosik::$ZLAVA = $this;
		$combine = false;
		$total = Kosik::total_discount_inclVAT(true);
		$hasItemZlava = Kosik::hasItemZlava();

		$messages = [];

		if(!$combine && count($this->zlavy) > 1){

			foreach ($this->zlavy as $k => $zlava){
				$messages["danger"][$zlava->kod] = 'Kombinácia zľavových kupónov [<strong>'.$zlava->kod.'</strong>] nieje možná. Prosím vyberte iba jeden kupón.';
				$this->unsetZlava($zlava->kod);
			}
			//Kosik::$ZLAVA = new MultiDiscount([]);
			return $messages;
		}

		foreach($this->zlavy as $k => $zlava){
			if($zlava->suma_od > 0 && ($total) <= $zlava->suma_od){
				$messages["danger"][$zlava->kod] = '1day Club kupón [<strong>'.$zlava->kod.'</strong>] je možné použiť až od objednávky <strong>nad '.Format::money_user($zlava->suma_od).'</strong>';
				$messages["success"][$zlava->kod] = null;
				$this->unsetZlava($zlava->kod);
			}
			if($hasItemZlava && !$combine){
				$messages["danger"][$zlava->kod] = '1day Club kupón [<strong>'.$zlava->kod.'</strong>] je možné použiť len bez kombinácie kupónov na produkty.';
				$messages["success"][$zlava->kod] = null;
				$this->unsetZlava($zlava->kod);
			}
			//bdump($zlava->card)
			if(!Auth::User() || ($zlava->card !== null && Auth::$User->vernostnaKarta()->cislo !== $zlava->card)){
				$messages["danger"][$zlava->kod] = '1day Club kupón [<strong>'.$zlava->kod.'</strong>] je možné použiť len s klub kartou pre ktorú bol vydaný.';
				$messages["success"][$zlava->kod] = null;
				$this->unsetZlava($zlava->kod);
			}
		}

		return $messages;
	}

	public function unsetZlava(string $kod = null) : void {
		unset($this->zlavy[$kod], $this->sumy[$kod], $this->titles[$kod]);
		$this->suma = array_sum($this->sumy);
		Kosik::$ZLAVA = $this;
	}
}