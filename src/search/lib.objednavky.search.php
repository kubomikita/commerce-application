<?php

class ObjednavkaSearchResult {
    public $klient;
    public $klient_nazov;
    public $klient_email;
    public $klient_telefon;
    public $klient_tag;
    public $klient_no_tag;
    public $datum_od;
    public $datum_do;
    public $cislo;
    public $suma_od;
    public $suma_do;
    public $nevybavene;
    public $vybavene;
    public $stav;
    public $hidden = 0;
    public $domain;
    public $cc;
    public $cc_sended;

    public $stav_code;
    public $logistika_code;
    public $no_logistika_code;
    
    public $pohoda_export = 0;
    public $export;
    
    public $order_by = 'datum desc';
	public $tovar;
	private $counting;

	public function netteQuery($limit = null,$offset = null){
		$db = Registry::get("database");
		$join = $where = [];
		if(isset($this->klient)){
			$where["klient"] = $this->klient;
		};
		if($this->datum_od != null && !($this->datum_od instanceof Timestamp)){
			throw new \Nette\InvalidArgumentException("Field 'datum_od' must by instance of 'Timestamp'");
		}
		if($this->datum_do != null && !($this->datum_do instanceof Timestamp)){
			throw new \Nette\InvalidArgumentException("Field 'datum_do' must by instance of 'Timestamp'");
		}
		if($this->datum_od instanceof Timestamp){
			$where["datum >="] = $this->datum_od->val();
		};
		if($this->datum_do instanceof Timestamp){
			$where["datum <="] = $this->datum_do->val();
		};
		if(isset($this->cislo)){
			$where["cislo"] = trim($this->cislo);
		};
		if($this->nevybavene){
			$where[] = $db::literal("o.vybavene = 0 or o.vybavene IS NULL");
		};
		if($this->cc !== null){
			$where["cc"] = $this->cc;
		};
		if($this->cc_sended !== null){
			$where["cc_sended"] = $this->cc_sended;
		};
		if($this->vybavene){
			$where["vybavene"] = 1;
		};
		if(isset($this->stav)){
			$where[] = $db::literal("(select stav from ".Config::tableObjednavkyStavy." where objednavka=o.id and stav>0 order by datum desc,id asc limit 1) = ?",$this->stav);
		};
		// suma sa riesi az na urovni php
		if(isset($this->klient_tag)){
			$where["o.klient IN "] = $db::literal("(SELECT partner_id AS id FROM ec_partneri_data WHERE",["vlastnosti_list LIKE" => "%[$this->klient_tag]%"],")");
		}
		if(isset($this->klient_no_tag)){
			$where["o.klient IN "] = $db::literal('(SELECT partner_id AS id FROM ec_partneri_data WHERE vlastnosti_list NOT LIKE ?)', "%[$this->klient_no_tag]%"); //$db::literal("(SELECT partner_id AS id FROM ec_partneri_data WHERE",["vlastnosti_list NOT LIKE" => "%[$this->klient_no_tag]%"],")");
		}
		
		if(isset($this->klient_nazov)){
			/*$query.=" and klient in (select id from ".Config::tablePartneri." where temporary=0 ";
			foreach(explode(' ',$this->klient_nazov) as $str){ $query.=" and nazov like '%".addslashes($str)."%' COLLATE 'utf8_general_ci' "; };
			$query.=" ) ";*/
			//$where["o.klient IN "] = $db::literal("(SELECT id FROM ec_partneri WHERE",["nazov LIKE" => "%$this->klient_nazov%"],")");
			$where["o.klient IN "] = $db::literal("(SELECT id FROM ec_partneri WHERE nazov LIKE ? OR kontakt LIKE ?)","%$this->klient_nazov%","%$this->klient_nazov%");
		};

		if(isset($this->klient_telefon)){
			//$query.=" and klient in (select id from ".Config::tablePartneri." where temporary=0 and kontakt like '%\"".addslashes($this->klient_telefon)."\"%' ) ";
			$where["o.klient IN "] = $db::literal("(SELECT id FROM ec_partneri WHERE",["kontakt LIKE" => "%$this->klient_telefon%"],")");
		};
		if(isset($this->klient_email)){
			//$query.=" and klient in (select id from ".Config::tablePartneri." where temporary=0 and kontakt like '%\"".addslashes($this->klient_email)."\"%' ) ";
			$where["o.klient IN "] = $db::literal("(SELECT id FROM ec_partneri WHERE",["kontakt LIKE" => "%$this->klient_email%"],")");
		};


		if(isset($this->suma_od) && $this->suma_od>0){
			//$query.=" and _cache_suma>=".(float)$this->suma_od."";
			$where["_cache_suma >="] = $this->suma_od;
		};
		if(isset($this->suma_do) && $this->suma_do>0){
			//$query.=" and _cache_suma<=".(float)$this->suma_do."";
			$where["_cache_suma <="] = $this->suma_do;
		};
		if(isset($this->stav_code) && $this->stav_code!=''){
			//$query.=" and _cache_stav_event like '%[".addslashes($this->stav_code)."]%'";
			$where["_cache_stav_event LIKE"] = "%[$this->stav_code]%";
		};
		if(isset($this->logistika_code) && $this->logistika_code!=''){
			//$query.=" and _cache_logistika_code='".addslashes($this->logistika_code)."'";
			$where["_cache_logistika_code"] = $this->logistika_code;
		};
		if(isset($this->no_logistika_code) && $this->no_logistika_code != ""){
			$where["_cache_logistika_code <>"] = $this->no_logistika_code;
		}
		if($this->pohoda_export){
			//$query.=" and _cache_meta_flags like '%[ready]%' and _cache_meta_flags not like '%[pohoda_exported]%'";
			$where[] = $db::literal("_cache_meta_flags LIKE '%[ready]%' and _cache_meta_flags NOT LIKE '%[pohoda_exported]%'");
		};
		if($this->export) {
			$where["export"] = $this->export;
		}

		if($this->hidden){
			$where["o.hidden"] = 1;
		} else {
			$where["o.hidden"] = 0;
		}
		if($this->domain !=""){
			//$query.=" and metadata LIKE '%s:6:\"domain\";s:".strlen($this->domain).":\"".$this->domain."\"%'";
			$where["o.metadata LIKE"] = "%s:6:\"domain\";s:".strlen($this->domain).":\"".$this->domain."\"%";
		}
		if(isset($this->order_by)){
			$e = explode(" ",$this->order_by);
			$d=["DESC"=>false,"ASC"=>true];
			$order = ["o.".$e[0]=>$d[strtoupper($e["1"])]];
		}
		/*if(empty($order)){
			$order = ["t.priorita"=>true];
		}*/
		$select = "o.*";
		if($this->counting){
			$select = " count(o.id) as pocet";
		}
		if($this->tovar!=""){
			$r = $db->query("SELECT DISTINCT (objednavka) FROM ec_objednavky_data WHERE tovar=?",$this->tovar)->fetchPairs();
			$where["id"] = $r;
		}

		if(isset($this->limit) && $this->limit >0){
			$q = $db->query("SELECT $select FROM ec_objednavky as o ".implode(" ",$join)." WHERE",$where,"ORDER BY",$order,"LIMIT",[$this->limit]); // ,"ORDER BY",$order
		} else {
			if($limit !== null && $offset !== null){
				$q = $db->query( "SELECT $select FROM ec_objednavky as o " . implode( " ", $join ) . " WHERE", $where,
					"ORDER BY", $order,"LIMIT ? ",$limit,"OFFSET ?",$offset );
			} else {
				$q = $db->query( "SELECT $select FROM ec_objednavky as o " . implode( " ", $join ) . " WHERE", $where,
					"ORDER BY", $order );
			}
		}



		//echo $q->getQueryString();
		return $q;//->getQueryString();
	}

    /*public function query(){
        if($this->order_by!='datum desc'){
          //$query="select o.id as id from ".Config::tableObjednavky." o left join ".Config::tablePartneri." p on p.id=o.klient where true ";
          $query="select o.* from ".Config::tableObjednavky." o left join ".Config::tablePartneri." p on p.id=o.klient where true ";
        } else {
          //$query="select id from ".Config::tableObjednavky." where true ";
          $query="select * from ".Config::tableObjednavky." o where true ";
        };

        if(isset($this->klient)){ $query.=" and klient=".(int)$this->klient." "; };
        if($this->datum_od instanceof Timestamp){ $query.=" and datum>=".$this->datum_od->val()." "; };
        if($this->datum_do instanceof Timestamp){ $query.=" and datum<=".$this->datum_do->val()." "; };
        if(isset($this->cislo)){ $query.=" and cislo='".addslashes(trim($this->cislo))."' "; };
        if($this->nevybavene){ $query.=" and (vybavene=0 or vybavene is null)"; };
        if($this->vybavene){ $query.=" and vybavene=1"; };
        if(isset($this->stav)){ $query.=" and (select stav from ".Config::tableObjednavkyStavy." where objednavka=o.id and stav>0 order by datum desc,id asc limit 1)=".(int)$this->stav."  "; };
        // suma sa riesi az na urovni php

        if(isset($this->klient_nazov)){
            $query.=" and klient in (select id from ".Config::tablePartneri." where temporary=0 ";
            foreach(explode(' ',$this->klient_nazov) as $str){ $query.=" and nazov like '%".addslashes($str)."%' COLLATE 'utf8_general_ci' "; };
            $query.=" ) ";
        };

        if(isset($this->klient_telefon)){ $query.=" and klient in (select id from ".Config::tablePartneri." where temporary=0 and kontakt like '%\"".addslashes($this->klient_telefon)."\"%' ) "; };
        if(isset($this->klient_email)){ $query.=" and klient in (select id from ".Config::tablePartneri." where temporary=0 and kontakt like '%\"".addslashes($this->klient_email)."\"%' ) "; };

        if(isset($this->suma_od) && $this->suma_od>0){ $query.=" and _cache_suma>=".(float)$this->suma_od.""; };
        if(isset($this->suma_do) && $this->suma_do>0){ $query.=" and _cache_suma<=".(float)$this->suma_do.""; };
        if(isset($this->stav_code) && $this->stav_code!=''){ $query.=" and _cache_stav_event like '%[".addslashes($this->stav_code)."]%'"; };
        if(isset($this->logistika_code) && $this->logistika_code!=''){ $query.=" and _cache_logistika_code='".addslashes($this->logistika_code)."'"; };
        if($this->pohoda_export){ $query.=" and _cache_meta_flags like '%[ready]%' and _cache_meta_flags not like '%[pohoda_exported]%'"; };
        if($this->hidden){
            $query.=" and hidden='1'";
        } else {
            $query.=" and hidden='0'";
        }
        if($this->domain !=""){
            $query.=" and metadata LIKE '%s:6:\"domain\";s:".strlen($this->domain).":\"".$this->domain."\"%'";
        }
        //if(isset($this->order_by)){ $query.=" order by ".$this->order_by." "; };
        $query.=" order by ".$this->order_by." ";

        return $query;
    }*/

    public function hash(){
        return hash('sha256',gzcompress(serialize($this),1));
    }

    public function result($fast=0,$ids_only=0){
        $hash=$this->hash().'_'.(int)$fast.'_'.(int)$ids_only;
        if(Cache::check('objednavky_search',$hash)){ $ret=Cache::get('objednavky_search',$hash); } else {
            $ret=array();
            //$Q=mysqli_query(CommerceDB::$DB,$this->query());
            //while($R=mysqli_fetch_assoc($Q)){
	        foreach($this->netteQuery() as $R){
                $O=new Objednavka($R,false,true);
                //if(!$fast){ $O->load_klient(); };
                if($ids_only){ $ret[]=$O->id; } else { $ret[]=$O; }; 
            };
            Cache::put('objednavky_search',$hash,$ret);
        };
        return $ret;
    }
    
    public function result_ids(){
      return $this->result(true,true);
    }

    public function result_part($start,$end){
        
        $i=0; $ret=array();
        $hash=$this->hash().'_part_'.$start.'_'.$end;
        if(Cache::check('objednavky_search',$hash)){ $ret=Cache::get('objednavky_search',$hash); } else {
          $q_offset = (int)$start;
          $q_limit = (int)($end - $start);
          //$Q=mysqli_query(CommerceDB::$DB,$this->query()." limit $q_limit offset $q_offset ");
          //while($R=mysqli_fetch_assoc($Q)){
	        foreach($this->netteQuery($q_limit,$q_offset) as $R){
            $ret[]=new Objednavka($R);
          };    
          
          Cache::put('objednavky_search',$hash,$ret);
        };
        
        return $ret;
    }

    public function count(){
        $ret=0;
        if(Cache::check('objednavky_search',$this->hash().'_count')){ $ret=Cache::get('objednavky_search',$this->hash().'_count'); } else {
            //$ret = mysqli_num_rows(mysqli_query(CommerceDB::$DB,$this->query()  ));
	        $this->counting = true;
	        $R = $this->netteQuery()->fetch();
	        $ret = $R["pocet"];
	        $this->counting = false;
            Cache::put('objednavky_search',$this->hash().'_count',$ret);
        };
        return $ret;
    }


}


?>
