<?php

use Kubomikita\Core\TraitContainer;
use Nette\Database\ResultSet;
use Nette\Utils\Strings;

class TovarSearchResult {

	use TraitContainer;

	protected static $defaultWhere = [];
	protected static $tovarCountWhere = 'SELECT tovar_id FROM ec_tovar_search_cache WHERE indexable=1 and aktivny=1 and (nedostupny_akcia <> 3 OR sklad > 0)';
    public $kategoria;
    public $nokategoria;
    public $indexable = 1;
    public $aktivny = 1;
    public $nazov;
    public $keywords;
    public $kod_systemu;
	public $kod_systemu_like;
    public $kod_vyrobcu;
    public $id;
    public $noid;
    public $ean;
    public $vyrobca;
	public $novyrobca;
    public $cenovaskupina;
    public $sablona;
    public $tagy;
    public $gift;
    public $fulltext; // univerzalny vstup na nazov,keywords,kody
    public $order_by;
    public $cena_od;
    public $cena_do;
    public $sklad;
    public $nosklad;
    public $noSkladToEnd = false;
    public $dodavatel;
    public $limit = 0;
    private $katalog = 0;
    public $neaktivny = 0 ;
    public $show_hidden = 0;
    public $withStack =false;
	public $host;
	public $filter;
	public $filtered = false;
	public $filterValues = [];
	public $date_from;
	public $ulovok;

	public $admin = 0;

	public $predajna;

	public $visible;
	public $invisible;
	/** @var \Kubomikita\Commerce\ConfiguratorInterface */
	//private $context;
	public $hidden = false;
	public $tovarova_skupina;

	public $group_by;

	private $tags = ["N" => "Novinky","A" => "Akcie"];


	public function __construct() {
		$this->host = $this->httpRequest->getUrl()->getHost();
		if($this->host == "www.protein.cz" || $this->host=="m.protein.cz"){
			$this->novyrobca = $this->getProteinCzNoTovar();
		}

		if( Strings::endsWith($this->httpRequest->getUrl()->getBasePath(), 'commerce/manager/')){
			//TovarSearchResult::setTovarCountWhere('SELECT count(id) FROM ec_tovar_kategorie WHERE kategorie_id=k.id and tovar_id IN (SELECT tovar_id FROM ec_tovar_search_cache WHERE indexable=1 and aktivny=1 and (nedostupny_akcia <> 3 OR sklad > 0))');
			$this->admin = 1;
		}
	}

	/**
	 * @param array $tags
	 */
	public function setTags( array $tags ): void {
		$this->tags = $tags;
	}

	public function getProteinCzNoTovar(){
		$vyr = [];
		$q=mysqli_query(CommerceDB::$DB,"SELECT id FROM ec_tovar_vyrobcovia WHERE proteincz = 0");
		while($r = mysqli_fetch_assoc($q)){
			$vyr[] = $r["id"];
		}
		if(empty($vyr)){
			return null;
		}
		return $vyr;
	}



	public function recommend(){
		if($this->kategoria_only() || $this->vyrobca_only()){
			$s = clone $this;
			$s->tagy = ["D"];
			return $s->count_fast() > 1;
		}
		return false;
	}

	public function tags($request = null){
		//dump($request,$this);
		//echo $this->netteQuery()->getQueryString();
		$ret = [];
		foreach($this->tags as $k => $t){
			$ret[$k]["name"] = $t;
			$ret[$k]["checked"] = false;
			if(isset($request["tag"]) && is_array($request["tag"]) && in_array($k,$request["tag"])){
				$ret[$k]["checked"] = true;
				$this->filtered = true;
			}
		}
		return $ret;
	}

	public function gift($request = null){
		//echo $this->netteQuery()->getQueryString();
		if(isset($request["gift"]) && $request["gift"] == 1){
			$this->filtered = true;
			return true;
		}
		return false;
	}

	public function stock($request = null){
		if(isset($request["skladom"]) && $request["skladom"] > 0){
			$this->filtered = true;
			return true;
		}
		return false;
	}
	public function setTagy($tag){
		if(!is_array($tag)){
			//$allowedTags = ["A","N","V","D"];
			$allowedTags = array_keys(\Config::$tovarTagy);
			if(in_array($tag,$allowedTags)){
				$this->tagy = [$tag];
			}
		}else{
			$this->tagy = $tag;
		}
	}
	public function brands($request=null){
		$value = [];
		if(!$this->vyrobca_only()) {
			$this->vyrobca = null;
			$this->cena_od = null;
			$this->cena_do = null;
			$this->filter  = null;
			$this->tagy = null;
			$this->gift = null;
			//echo $this->netteQuery( "t.vyrobca_id,t.vyrobca_nazov" )->getQueryString();
			$value         = $this->netteQuery( "t.vyrobca_id,t.vyrobca_nazov" )->fetchPairs();
			natsort( $value );
			foreach ( $value as $k => $v ) {
				if ( $k > 0 ) {
					$value[ $k ] = [ "name" => $v, "checked" => false ];

					if ( isset( $request["brand"] ) && is_array( $request["brand"] ) && in_array( $k,
							$request["brand"] ) || (isset($request["vyrobcovia"]) && is_array($request["vyrobcovia"]) && in_array($k,$request["vyrobcovia"]))
					) {
						$value[ $k ]["checked"] = $this->filtered = true;
					}
					if(!Registry::get("container")->getParameter("shop","filterMultipleBrands")) {
						$value[ $k ]["object"] = new TovarVyrobca( $k );
						$value[$k]["link"] = $this->generateCategoryBrandLink($value[$k]["object"]);
					}
				} else {
					unset( $value[ $k ] );
				}
			}
			/*if($this->isProtein() && $this->isGiftCategory()){

			}*/
		}
		//dump($this->filtered,$value);
		return $value;
	}
	private function generateCategoryBrandLink(TovarVyrobca $v){
		$router = Registry::get("container")->router;
		//dump($v,$router);
		//if(isset($router->query["vyrobcovia"])){

		//}
		return $router->requested_link."/".$v->seo_name;
	}
	public function prices($request=null){
		$this->cena_od = null;
		$this->cena_do = null;
		if(!$this->vyrobca_only()) {
			$this->vyrobca = null;
		}

		$value = $this->netteQuery("MIN(t.cena) as min,MAX(t.cena) as max")->fetch();
		$value["original_min"] = floor($value["min"]);
		$value["original_max"] = ceil($value["max"]);
		$value["min"] = (isset($request["cena_od"]) && (float)$request["cena_od"] > 0)?(float)$request["cena_od"]:$value["original_min"];
		$value["max"] = (isset($request["cena_do"]) && (float)$request["cena_do"] > 0)?(float)$request["cena_do"]:$value["original_max"];

		if($value["min"] != $value["original_min"] or $value["max"] != $value["original_max"]){
			$this->filtered = true;
		}

		return $value;
	}

	public function filterSlider($request=null){
		$db = Registry::get("database");
		$q = $db->query("SELECT * FROM ec_tovar_search_cache_filter WHERE aktivny = 1 AND slider = 1 ORDER BY poradie");
		$slider = [];
		//bdump($request,"REQUEST");
//		bdump($this->filterValues);
		foreach ($q as $r){
			list( $col, $key ) = explode( "_", $r["col"] );
			$name = $key;

			$min_original = isset($this->filterValues[$key]) ? (int) floor(min($this->filterValues[$key])["raw"]) : 0;
			$max_original = isset($this->filterValues[$key]) ? (int) ceil(max($this->filterValues[$key])["raw"]) : 0;

			$slider[$name] = ["min_original" => $min_original, "max_original" => $max_original];
			$slider[$name]["min"] = $request !== null ? (float) $request[$key]["min"] : $min_original;
			$slider[$name]["max"] = $request !== null ? (float) $request[$key]["max"] : $max_original;


		}
		return $slider;
	}

	public function filter($request=null){
		$filter = [];
		$db = Registry::get("database");

		$q = $db->query("SELECT * FROM ec_tovar_search_cache_filter WHERE aktivny = 1 ORDER BY poradie");
		//echo json_encode(["S","M"]);exit;
		foreach($q as $f){
			$this->filter = null;
			$value = $this->netteQuery("DISTINCT(".$f["col"].")")->fetchPairs();
			//$tovSkup =$this->netteQuery("DISTINCT(".$f["col"]."), tovarova_skupina")->fetchPairs();
			//bdump($value);
			//bdump($this->netteQuery("DISTINCT(".$f["col"]."), tovarova_skupina")->fetchPairs());
			$vals = [];
			foreach($value as $k=> $v){
				if(strlen(trim($v)) == 0){
					unset($value[$k]);
				} else {
					//if(\Nette\Utils\Strings::contains($f["col"],"filter_")) {
						list( $col, $key ) = explode( "_", $f["col"] );
					/*} else {
						$col = "filter";
						$key = $f["col"];
					}*/
					//bdump($f);
					//bdump($f["col"]);
					if($f["col"] == "filter_velkost" || $f["col"] == "filter_velkostdieta"){
						$val = \Nette\Utils\Json::decode($v,\Nette\Utils\Json::FORCE_ARRAY);
						foreach($val as $itm){
							$value[$itm] = ["raw" => $itm, "value" => $itm, "checked" => false ];
						}
						if ( isset( $request[ $key ] ) && ! empty( $request[ $key ] )  ) {
							foreach($request[ $key ] as $selected){
								$this->filtered = $value[$selected]["checked"] = true;
							}
						}
						unset( $value[ $k ] );
					} else {
						if ( $f["col"] == "filter_objem" ) {
							$value[ $k ] = [ "raw"     => $v,
							                 "value"   => (substr($v, -1) == "0") ? number_format( $v, 2 ) : number_format($v, 3) ,//. " l",
							                 "checked" => false
							];
						} elseif ( $f["col"] == "filter_alkohol" ) {
							$value[ $k ] = [ "raw"     => $v,
							                 "value"   => number_format( $v, 1 ) . " %",
							                 "checked" => false
							];
						} else {
							$value[ $k ] = [ "raw"     => $v,
							                 "value"   => $v,
							                 "checked" => false
							];
						}


						if ( isset( $request[ $key ] ) && ! empty( $request[ $key ] ) && in_array( $v,
								$request[ $key ] ) ) {
							$this->filtered = $value[ $k ]["checked"] = true;
						}
						$value[ $v ] = $value[ $k ];
						unset( $value[ $k ] );
					}

				}
			}
			//dump($value,$vals);
			//dump($request[$key],$vals,in_array( $vals,
			//	$request[ $key ] ));
			if(!empty($value)) {
				list($col,$key) = explode("_",$f["col"]);
				ksort($value, SORT_NATURAL);
				$filter[ $f["nazov"]."|".$key ] = $value;
				$this->filterValues[$key] = $value;
			}
		}

		//echo $this->netteQuery("DISTINCT(filter_objem)")->getQueryString();
		return $filter;
	}

	/**
	 * @param string $select
	 *
	 * @return ResultSet
	 * @throws Exception
	 */
	public function netteQuery($select = "t.tovar_id"): ResultSet{
		$db = $this->db;

		$where = $join = [];

		$where             = self::$defaultWhere;

		if(!$this->admin) {
			$where["t.cena >"] = 0;
		} else {
			unset($where["t.show_eshop"]);
			unset($where["t.show_vo"]);
			unset($where["t.show_cc"]);
		}

		if(!$this->hidden) {
			$where["t.hidden <>"] = 1;
		}


		if($this->indexable){
			$where["t.indexable"] = 1;
		}
		if($this->aktivny){
			$where["t.aktivny"] = 1;
		} elseif($this->neaktivny){
			$where["t.aktivny"] = 0;
		}
		if($this->invisible){
			$this->visible = null;
			if(isset($where["t.aktivny"])) {
				unset($where["t.aktivny"]);
			}
			$where[] = $db::literal("t.nedostupny_akcia = ?", 3);
		}
		if(isset($this->kategoria)){
			$where["k.kategorie_id"] = $this->kategoria;
			$join[] = "LEFT JOIN ec_tovar_kategorie as k ON `k`.`tovar_id`=`t`.`tovar_id`";
		}
		if(isset($this->nokategoria)){
			//$where["k.kategorie_id NOT IN"] = $this->nokategoria;
			$where[] = $db::literal("(SELECT COUNT(id) FROM ec_tovar_kategorie WHERE tovar_id=`t`.`tovar_id` and kategorie_id IN(?)) = 0",$this->nokategoria);
			//$join[] = "LEFT JOIN ec_tovar_kategorie as k ON `k`.`tovar_id`=`t`.`tovar_id`";
		}
		if(isset($this->tagy) && is_array($this->tagy)){
			foreach($this->tagy as $tag){
				//$where["t.tagy_list LIKE"] = "%[".$tag."]%";
				$where[] = $db::literal("t.tagy_list LIKE ?","%[".$tag."]%");
			}
		}
		if(isset($this->nazov)){
			$where["t.nazov LIKE"] = "%".$this->nazov."%";
		}
		if(isset($this->fulltext)){
			if(Strings::contains($this->fulltext, ",")){
				$e = explode(",",$this->fulltext);
				$or = [];
				foreach($e as $word){
					$or[] = $db::literal( "t.combined LIKE ?", "%" . trim($word) . "%" );
				}
				$where[] = $db::literal("?or", $or);
			} else {
				$e = explode( " ", $this->fulltext );
				foreach ( $e as $word ) {
					$where[] = $db::literal( "t.combined LIKE ?", "%" . trim($word) . "%" );
				}
			}
		}
		if(isset($this->visible) and (bool)$this->visible){
			$where[] = $db::literal("t.nedostupny_akcia <> ? or t.sklad > ?", 3, 0);
		}
		if(isset($this->kod_systemu)){
			$where["t.kod_systemu"] = $this->kod_systemu;
			//$query.=" and kod_systemu='".addslashes($this->kod_systemu)."' ";
			//$query.=" and ( kod_systemu='".safeString(addslashes($this->kod_systemu))."' or t.tovar_id in (select tovar as tovar_id from ".Config::tableTovarVariantyData." where kod_systemu='".safeString(addslashes($this->kod_systemu))."' ) ) ";
		};
		if(isset($this->gift)){
			$where["t.gift"] = $this->gift;
		}

		if(isset($this->kod_vyrobcu)) {
			$where["t.kod_vyrobcu"] = $this->kod_vyrobcu;
		}
		if(isset($this->id)){

			$where["t.tovar_id"] = $this->id;

		};
		if(isset($this->ulovok)){

			$where["t.ulovok"] = 1;

		};
		if(isset($this->noid)){
			$where["t.tovar_id <>"] = $this->noid;
		};
		if(isset($this->ean)){
			$where["t.ean"] = $this->ean;
		};
		if(isset($this->vyrobca)){
			$where["t.vyrobca_id"] = $this->vyrobca;
		};

		if(isset($this->date_from) && (int) $this->date_from > 0){
			$where["t.datum_created >"] = $this->date_from;
		}

		if(isset($this->novyrobca)){
			if(is_array($this->novyrobca)){
				$where["t.vyrobca_id NOT"] = $this->novyrobca;
			} else {
				$where["t.vyrobca_id <>"] = $this->novyrobca;
			}
		};
		if(isset($this->filter) && is_array($this->filter) && !empty($this->filter)){
			//echo "filter";
			//bdump($this->filter);
			$filter = $this->filter($_REQUEST["filter"]);
			$this->filter = $_REQUEST["filter"];
			$slides = $this->filterSlider();
			//bdump($slides);
			foreach($this->filter as $key => $value){
				if(!isset($slides[$key])) {
					if ( $key == "velkost" || $key == "velkostdieta" ) {
						if ( is_array( $value ) ) {
							foreach ( $value as $v ) {
								$where[] = $db::literal( "t.filter_" . $key . " LIKE ?", '%"' . $v . '"%' );
							}
						} else {
							$where[] = $db::literal( "t.filter_" . $key . " LIKE ?", '%"' . $value . '"%' );
						}

					} else {
						$where[ "t.filter_" . $key ] = $value;
					}
				} else {
					//bdump($value);
					//bdump($slides[$key]);
					$min = (int) $value["min"];
					$max = (int) $value["max"];
					if(/*($slides[$key]["min"] != 0  && $slides[$key]["max"] != 0 )*/ $slides[$key]["min"] > 0 && $slides[$key]["min"] != $min) {
						$where[ "t.filter_" . $key . " >=" ] = (float) $value["min"];
					}
					if(/*($slides[$key]["min"] != 0  && $slides[$key]["max"] != 0 ) */$slides[$key]["max"] > 0 && $slides[$key]["max"] != $max) {
						$where[ "t.filter_" . $key . " <=" ] = (float) $value["max"];
					}
					//bdump($where);
				}
			}
		}

		if(isset($this->cena_od)){
			$where["t.cena >"] = (float) $this->cena_od;
		}
		if(isset($this->cena_do)){
			$where["t.cena <"] = (float) $this->cena_do;
		}
		if(isset($this->sklad)){
			$where["t.sklad >"] = 0;
			//bdump($where);
			//bdump($this);
		};
		if(isset($this->nosklad)){
			$where["t.sklad <"] = 1;
		}
		if(isset($this->predajna) and $this->predajna > 0){
			$join[] = "LEFT JOIN ec_predajne_zasoby as z ON t.tovar_id = z.tovar_id";
			$where["z.predajne_id"] = $this->predajna;
			$where["z.stav >"] = 0;
		}


		if($this->configurator->isProtein()){
			if(!$this->admin) {
				if($this->configurator->isHost("protein.sk")){
					$where["protein_sk"] = 1;
				}
				if($this->configurator->isHost("protein.cz")){
					$where["protein_cz"] = 1;
				}
			}
		}

		if($this->configurator->isHost("1day.sk")){
			if(isset($this->tovarova_skupina)){
				$where["t.tovarova_skupina"] = $this->tovarova_skupina;
			}
		}

		/*if($select != "t.tovar_id"){
			$where["t"]
		}*/

		if(isset($this->order_by) && is_string($this->order_by) && strlen(trim($this->order_by)) > 0){
			$e = explode(" ",$this->order_by);
			$d=["DESC"=>false,"ASC"=>true];
			$order = ["t.".$e[0]=>$d[strtoupper($e["1"])]];
		} elseif (isset($this->order_by) && is_array($this->order_by)){
			$order = $this->order_by;
			$this->order_by = "";
			foreach($order as $column => $asctype){
				$this->order_by .= $column ." ".($asctype ? "ASC":"DESC").",";
			}
			$this->order_by = substr($this->order_by,0,-1);
		}
		if(empty($order)){
			$order = ["t.priorita"=>true];
		}
		//bdump($order);
		//bdump($where);
		//bdump($join);
		//bdump($select);

		//bdump($where);

		if($this->order_by === null || ! Strings::contains($this->order_by,"sledovanost")) {
			if($this->group_by !== null){
				$group_by = "GROUP BY ".$this->group_by;
			}
			if ( isset( $this->limit ) && $this->limit > 0 ) {
				$q = $db->query( "SELECT $select FROM " . Config::tableTovarSearchCache . " as t " . implode( " ",
						$join ) . " WHERE", $where, $group_by." ORDER BY", $order, "LIMIT",
					[ $this->limit ] ); // ,"ORDER BY",$order
			} else {

				$q = $db->query( "SELECT $select FROM " . Config::tableTovarSearchCache . " as t " . implode( " ",
						$join ) . " WHERE", $where, $group_by." ORDER BY", $order );
			}
		} else {
			$select = "count(w.tovar) as sum, w.tovar as tovar_id";
			$where["t.aktivny"] = 1;
			$where["t.delete"] = 0;
			if ( isset( $this->limit ) && $this->limit > 0 ) {
				$q = $db->query( "select $select from ec_watchdog as w LEFT JOIN ec_tovar_search_cache as t ON w.tovar=t.tovar_id WHERE",
					$where, "GROUP BY w.tovar ORDER BY sum DESC LIMIT",[ $this->limit ]);
			} else {
				$q = $db->query( "select $select from ec_watchdog as w LEFT JOIN ec_tovar_search_cache as t ON w.tovar=t.tovar_id WHERE",
					$where, "GROUP BY w.tovar ORDER BY sum DESC" );
			}
			//echo  $q->getQueryString();
		}

		//bdump($where,"TovarSearchCache WHERE");
		//echo  $q->getQueryString();
		//dump($q->getQueryString());
		//bdump($q->getQueryString(),"SEARCH");
		return $q;//->getQueryString();


	}

	/**
	 * @deprecated
	 * @return string|string[]
	 */

	public function query(){
        if($this->order_by == "sledovanost"){
            $query = "select count(ec_watchdog.tovar) as sum,ec_watchdog.tovar as tovar_id
from ec_watchdog
LEFT JOIN ec_tovar ON ec_watchdog.tovar=ec_tovar.id
WHERE ec_tovar.aktivny='1' and ec_tovar.deleted='0' and ec_tovar.hlavna_off='0'
group by ec_watchdog.tovar order by sum desc";
            //if(isset($this->limit) && $this->limit>0){ $query.=" limit ".$this->limit." ";  };
        } else {
            $JOIN = "";
            //select t.tovar_id from ec_tovar_search_cache as t LEFT JOIN ec_tovar_kategorie as k ON t.tovar_id = k.tovar_id  where true and (k.kategorie_id='13') and t.indexable=1 and t.aktivny=1 order by t.datum_created DESC
            $query="select t.tovar_id from ".Config::tableTovarSearchCache." as t [JOIN] where true ";

            if($this->indexable){ $query.=" and t.indexable=1 "; };

            if($this->aktivny){ $query.=" and t.aktivny=1 "; };
            if($this->neaktivny){ $query.=" and t.aktivny<>1 "; };

	        if($this->hidden) {
		        $query .= "and t.hidden = 0 ";
	        }
	        if($this->visible) {
		        $query .= "and t.nedostupny_akcia <> 3";
	        }

            if(isset($this->kategoria)){
                //if($_SERVER["HTTP_X_REAL_IP"] == "217.31.42.54"){
                       // dump(strip_tags(whitespaces($e[0])),substr(strip_tags(whitespaces($e[0])),1,-1),mb_strtolower($e[1]));
                    $query.=" and ( true ";
                    $i=0;
                    foreach(explode(',',$this->kategoria) as $kat){
                      if($i==0){
                        $query.=" and k.kategorie_id='".safeString(addslashes(trim($kat)))."' ";
                      } else {
                        $query.=" or k.kategorie_id='".safeString(addslashes(trim($kat)))."' ";
                      }
                      $i++;
                    };
                    $query.=" ) ";
                    $JOIN = "LEFT JOIN ec_tovar_kategorie as k ON t.tovar_id = k.tovar_id";
                /*} else {
                $query.=" and ( true ";
                  $i=0;
                foreach(explode(',',$this->kategoria) as $kat){
                  if($i==0){
                    $query.=" and t.kategorie_list like '%[".addslashes(trim($kat))."]%' ";
                  } else {
                    $query.=" or t.kategorie_list like '%[".addslashes(trim($kat))."]%' ";
                  }
                  $i++;
                };
                $query.=" ) ";
                }*/
            };

            if(isset($this->tagy)){
                $query.=" and ( true ";
                foreach(explode(',',$this->tagy) as $tag){ $query.=" and t.tagy_list like '%[".safeString(addslashes(trim($tag)))."]%' "; };
                $query.=" ) ";
            };

            if(isset($this->nazov)){
                $query.=" and ( true ";
                foreach(explode(' ',$this->nazov) as $str){ $query.=" and t.nazov like '%".safeString(addslashes($str))."%' COLLATE 'utf8_general_ci' "; };
                $query.=" ) ";
            };

            if(isset($this->keywords)){
                $query.=" and ( true ";
                foreach(explode(' ',$this->keywords) as $str){ $query.=" and t.keywords like '%".safeString(addslashes($str))."%' "; };
                $query.=" ) ";
            };

            if(isset($this->fulltext)){
                $query.=" and ( true ";
                foreach(explode(' ',$this->fulltext) as $str){ $query.=" and t.combined like '%".safeString(addslashes($str))."%' COLLATE 'utf8_general_ci' "; };
                $query.=" ) ";
            };

            if(isset($this->kod_systemu)){
                //$query.=" and kod_systemu='".addslashes($this->kod_systemu)."' ";
                $query.=" and ( kod_systemu='".safeString(addslashes($this->kod_systemu))."' or t.tovar_id in (select tovar as tovar_id from ".Config::tableTovarVariantyData." where kod_systemu='".safeString(addslashes($this->kod_systemu))."' ) ) ";
            };
	        if(isset($this->kod_systemu_like)){
		        //$query.=" and kod_systemu='".addslashes($this->kod_systemu)."' ";
		        $query.=" and ( kod_systemu LIKE '%".safeString(addslashes($this->kod_systemu_like))."%' or t.tovar_id in (select tovar as tovar_id from ".Config::tableTovarVariantyData." where kod_systemu LIKE '%".safeString(addslashes($this->kod_systemu_like))."%' ) ) ";
	        };

            if(isset($this->kod_vyrobcu)){ $query.=" and t.kod_vyrobcu='".safeString(addslashes($this->kod_vyrobcu))."' "; };
            if(isset($this->id)){
                if(is_array($this->id)){
                    $search_id=array();
                    foreach($this->id as $id){
                        $search_id[] = safeString($id);
                    }
                    $query.=" and t.tovar_id IN(".implode(",",$search_id).")";
                } else {
                    $query.=" and t.tovar_id=".(int)safeString($this->id)." ";
                }
             };
            if(isset($this->noid)){ $query.=" and t.tovar_id!=".(int)($this->noid)." "; };
            if(isset($this->ean)){ $query.=" and t.ean='".safeString(addslashes($this->ean))."' "; };

            if(isset($this->vyrobca)){
                if(is_array($this->vyrobca)){
                    //$vyr = join(',',($this->vyrobca));
                    $vyr="'";
                    //dump($this->vyrobca);
                    foreach($this->vyrobca as $v){
                        //dump($v);
                        if($v != "" and is_numeric($v)){
                            $vyr.=safeString($v)."','";
                        }
                    }
                    $vyr=substr($vyr,0,-2);
                    $query.=" and t.vyrobca_id in (".($vyr).") ";
                } else { $query.=" and t.vyrobca_id='".(int)safeString($this->vyrobca)."' "; };

                };
	        if(isset($this->novyrobca)){
		        if(is_array($this->novyrobca)){
			        $vyr="'";
			        foreach($this->novyrobca as $v){
				        if($v != "" and is_numeric($v)){
					        $vyr.=safeString($v)."','";
				        }
			        }
			        $vyr=substr($vyr,0,-2);
			        $query.=" and t.vyrobca_id NOT IN (".($vyr).") ";
		        } else {
			        $query.=" and t.vyrobca_id != '".(int)safeString($this->vyrobca)."' ";
		        }
	        };

            if(isset($this->cenovaskupina)){ $query.=" and t.cenovaskupina_id='".(int)$this->cenovaskupina."' "; };
            if(isset($this->sablona)){ $query.=" and t.sablona_id='".(int)$this->sablona."' "; };

            if(Config::dodavatelVyrobca && isset($this
			            ->dodavatel)){
                $vyrobcovia=array();
                foreach(TovarVyrobca::fetchDodavatel($this->dodavatel) as $TV){
                    $vyrobcovia[]=$TV->id;
                };
                if(sizeof($vyrobcovia)){
                    $query.=" and t.vyrobca_id in (".join(',',($vyrobcovia)).") ";
                } else {
                    $query.=" and false ";
                };
            };

            if($this->cena_od>0){ $query.=" and t.cena>=".(float)safeString($this->cena_od)." "; };
            if($this->cena_do>0){ $query.=" and t.cena<=".(float)safeString($this->cena_do)." "; };
            if($this->sklad){ $query.=" and t.sklad>0 "; };



            if(isset($this->order_by)){
                if(trim($this->order_by)==''){ $this->order_by=' t.priorita desc'; };
                $query.=" order by  t.".safeString($this->order_by)." ";
            };

            if(isset($this->limit) && $this->limit>0){ $query.=" limit ".$this->limit." ";  };
        }
        $query = str_replace("[JOIN]",$JOIN,$query);
        return $query;
    }

    public function kategoria_only(){
        $ret=0;
        if(isset($this->kategoria)){ $ret=1; };
        //if(isset($this->tagy) && is_array($this->tagy)){  if(in_array("D",$this->tagy)){ $ret=0; }  };
        //if(isset($this->tagy)){ foreach(explode(',',$this->tagy) as $tag){ if(trim($tag)!='D'){ $ret=0; } }; };
        if(isset($this->nazov) || isset($this->keywords) || isset($this->fulltext) || isset($this->kod_systemu) || isset($this->kod_vyrobcu) || isset($this->ean) || isset($this->cenovaskupina) || isset($this->sablona) || isset($this->dodavatel)){ $ret=0; };
        return $ret;
    }

    public function vyrobca_only(){
      $ret=0;
      if(isset($this->vyrobca) && !is_array($this->vyrobca)){ $ret=1; };
      if(isset($this->kategoria) and $ret == 0){ $ret=0; };
      if(/*isset($this->tagy) || */isset($this->nazov) || isset($this->keywords) || isset($this->fulltext) || isset($this->kod_systemu) || isset($this->kod_vyrobcu) || isset($this->ean) || isset($this->cenovaskupina) || isset($this->sablona) || isset($this->dodavatel)){ $ret=0; };
      return $ret;
    }


    public function hash(bool $idsOnly = false) : string
    {
	    return md5(serialize($this) .serialize(self::$defaultWhere) ).(($idsOnly) ? "_ids" : "");
    }



    public function isProtein(){
		return $this->configurator->isProtein();
		//return trim($this->host) != "" && ( Strings::contains($this->host,"protein") || Strings::contains($this->host,"localhost"));
    }

	/**
	 * @deprecated
	 * @return bool
	 */
    public function isGiftCategory(){
		return false;
		/** @deprecated  */
		return $this->kategoria == 274;
    }

    public function getGifts($ids_only=false){
		$ret = [];
	    $db = Registry::get("database");
	    $d = Darcek::find(["aktivny"=>1,$db::literal("sklad > ?",0)]);
	    foreach($d as $da){
		    $T = new Tovar($da->tovar,$ids_only);
		    if($T->visible_show()){
			    if($ids_only){
				    $ret[] = $T->id;
			    } else {
				    $ret[] = $T;
			    }
		    }
	    }
	    return $ret;
    }


	/**
	 * @param bool $idsOnly
	 *
	 * @return Tovar[]
	 * @throws Exception
	 */
	public function result(bool $idsOnly = false) : array {

		$callback = function (&$dependencies) use ($idsOnly) {
			if($this->noSkladToEnd){
				$clone = clone $this;
				$this->sklad = 1;
				$onStockResult = (array) $this->netteQuery()->fetchPairs( "tovar_id", "tovar_id" );
				$clone->nosklad = 1;

				if(!isset($_GET["skladom"])){
					$clone->sklad = null;
				}
				$noStockResult = (array) $clone->netteQuery()->fetchPairs("tovar_id","tovar_id");
				$result = $onStockResult + $noStockResult;
			} else {
				$result = (array) $this->netteQuery()->fetchPairs( "tovar_id", "tovar_id" );
			}

			if($this->isProtein()){
				if($this->isGiftCategory()){
					$result += $this->getGifts($idsOnly);
					$this->filter = false;
				}
			}

			if(!empty($result)) {
				return ($idsOnly) ? $result : Tovar::getByIds($result);
			}
			return [];
		};
		//return $callback($idsOnly);
		return ($this->order_by != 'rand()' ) ? Cache::load('tovar_search', $this->hash($idsOnly), $callback) : $callback($idsOnly);
	}

	/**
	 * @return array
	 * @throws Exception
	 */
	public function result_ids() : array
	{
		return $this->result(true);
	}

	/**
	 * @param $start
	 * @param $end
	 *
	 * @return Tovar[]
	 * @throws Exception
	 */
	public function result_part($start,$end) : array{

		$hash=$this->hash().'_part_'.$start.'_'.$end;
		return Cache::load("tovar_search", $hash, function (&$dependencies) use($start, $end) {
			$ret = [];
			$i=0;
			foreach($this->result_ids() as $res_id){
				if($i>=$start && $i<$end){
					$ret[] = new Tovar($res_id);
				}
				$i++;
			}
			return $ret;
		});
	}

	/**
	 * @return int
	 * @throws Exception
	 */
    public function count_fast() : int
    {
    	return Cache::load("tovar_search", $this->hash()."_count_fast", function (&$dependencies){
			$count = $this->netteQuery()->getRowCount();
		    if($this->isProtein() && $this->isGiftCategory()){
			    $count += count($this->getGifts(true));
		    }
		    return $count;
	    });
    }

	/**
	 * @return int
	 * @throws Exception
	 */
    public function count() : int {
	    return Cache::load("tovar_search", $this->hash()."_count", function (&$dependencies){
		    $count = count($this->result_ids());
		    if($this->isProtein() && $this->isGiftCategory()){
			    $count += count($this->getGifts(true));
		    }
		    return $count;
	    });
    }

	public static function setDefaultWhere(array $defaultWhere = []) : void {
		self::$defaultWhere = $defaultWhere;
	}

	public static function getDefaultWhere() : array {
		return self::$defaultWhere;
	}

	public static function setTovarCountWhere(string $query) : void {
		self::$tovarCountWhere = $query;
	}

	public static function getTovarCountWhere() : string {
		return self::$tovarCountWhere;
	}
}
