<?php

class PartnerSearchResult {
    public $nazov;
    public $email;
    public $telefon;
    public $skupina;
    public $tagy;
    public $order_by;
    public $vip=0;
    public $off=0;
    public $allow=0;
    public $created;

	private $counting = false;

    /*
    private function verify(Partner $P){
        
        if(!$this->off && $P->off){ return false; };  // SQL: pd.off
        
        if($this->allow==1 && !$P->allow_login){ return false; };  // SQL: pd.allow_login
        if($this->allow==-1 && $P->allow_login){ return false; };

        if(isset($this->email)){  // SQL: pd._cache_email , pd.username
            $rx=false;
            if(strstr($P->username, $this->email)){ $rx=true; };
            if(strstr($P->kontakt['email'], $this->email)){ $rx=true; };
            if(!$rx){ return false; };
        };

        if(isset($this->telefon)){ // SQL: pd._cache_telefon
            $rx=false;
            if(strstr($P->kontakt['telefon'], $this->telefon)){ $rx=true; };
            if(!$rx){ return false; };
        };


        if(isset($this->skupina) && $P->skupina->id!=$this->skupina){ return false; }; // SQL: pd.skupina_id

        if(isset($this->tagy)){
            foreach(explode(',',$this->tagy) as $tag){ if(!$P->tag_has($tag)){ return false; }; };  //SQL: pd.vlastnosti_list like %[x]%
        };
        
        if($this->vip && !$P->is_vip()){ return false; };   // SQL: pd.vip_termin > time()

        return true;
    }
    */
	public function netteQuery($limit=null,$offset=null){
		$db = Registry::get("database");
		$join = $where = [];

		$join[] = "left join ec_partneri_data pd on pd.partner_id=o.id";

		$config = Env::shop();
		if(isset($config["orderWithoutRegistration"])) {
			$where["temporary"] = 0;
		}
		if(isset($this->nazov)){
			//$where["o.nazov LIKE"] = "%$this->nazov%";
			$e = explode(" ",$this->nazov);
			foreach($e as $word) {
				$where[] = $db::literal("o.nazov LIKE ?","%" . $word . "%");
			}
		};

		if(!$this->off){
			//$query .= " and pd.off<>1 ";
			$where["pd.off <>"] = 1;
		};

		if($this->allow==1){
			//$query .= " and pd.allow_login=1 ";
			$where["pd.allow_login"] = 1;
		};
		if($this->allow==-1){
			//$query .= " and pd.allow_login<>1 ";
			$where["pd.allow_login <>"] = 1;
			$where["o.temporary"] = 0;
		};

		if(isset($this->email)){
			//$query .= " and ( pd._cache_email like '%".addslashes(trim($this->email))."%' or pd.username like '%".addslashes(trim($this->email))."%' )";
			$where[] = $db::literal("( pd._cache_email LIKE ? or pd.username LIKE ? )","%".trim($this->email)."%","%".trim($this->email)."%");
		};

		if(isset($this->telefon)){
			//$query .= " and pd._cache_telefon like '%".addslashes(trim($this->telefon))."%' ";
			$where["pd._cache_telefon LIKE"] = "%".trim($this->telefon)."%";
		};


		if(isset($this->skupina) && $this->skupina>0){
			//$query .= " and pd.skupina_id=".(int)$this->skupina." ";
			$where["pd.skupina_id"] = $this->skupina;
		};

		if(isset($this->tagy)){
			//$query .= ' and (';
			//$tagy = "";
			$tagy =[];
			foreach(explode(',',$this->tagy) as $tag){
				//$tagy .= "  pd.vlastnosti_list like '%[".addslashes($tag)."]%' or ";
				$tagy[] = $db::literal("pd.vlastnosti_list like ?","%[".$tag."]%");
			};
			$where[] = $db::literal('?or', $tagy);
			//$query .= substr($tagy,0,-3);
			//$query .= ") ";

		};

		if($this->vip){
			$where["pd.vip_termin >="] = time();
		};
		if($this->created > 0){
			$where["o.created >"] = $this->created;
		}

		if(isset($this->order_by)){
			$e = explode(" ",$this->order_by);
			$d=["DESC"=>false,"ASC"=>true];
			$order = ["o.".$e[0]=>$d[strtoupper($e["1"])]];
		} else {
			$order = ["o.nazov"=>true];
		}

		$select = "o.*";
		if($this->counting){
			$select = " count(o.id) as pocet";
		}

		if(isset($this->limit) && $this->limit >0){
			$q = $db->query("SELECT $select FROM ec_partneri as o ".implode(" ",$join)." WHERE",$where,"ORDER BY",$order,"LIMIT",[$this->limit]); // ,"ORDER BY",$order
		} else {
			if($limit !== null && $offset !== null){
				$q = $db->query( "SELECT $select FROM ec_partneri as o " . implode( " ", $join ) . " WHERE", $where,
					"ORDER BY", $order,"LIMIT",[$limit],"OFFSET",[$offset] );
			} else {
				$q = $db->query( "SELECT $select FROM ec_partneri as o " . implode( " ", $join ) . " WHERE", $where,
					"ORDER BY", $order );
			}
		}


		return $q;//->getQueryString();
	}


    public function hash(){
        return hash('sha256',gzcompress(serialize($this),1));
    }

    public function result(){
        if(Cache::check('partneri_search',$this->hash())){ $ret=Cache::get('partneri_search',$this->hash()); } else {
            $ret=array();
	        foreach($this->netteQuery() as $R){
	              $ret[] = new Partner($R);
              };
            Cache::put('partneri_search',$this->hash(),$ret);
        };
        return $ret;
    }

    public function result_part($start,$end){
        $i=0; $ret=array();
        $hash=$this->hash().'_part_'.$start.'_'.$end;
        if(Cache::check('partneri_search',$hash)){ $ret=Cache::get('partneri_search',$hash); } else {
          $q_offset = (int)$start;
          $q_limit = (int)($end - $start);
	        foreach($this->netteQuery($q_limit,$q_offset) as $R){
		        $ret[] = new Partner($R);
	        }
          Cache::put('partneri_search',$hash,$ret);
        };
        return $ret;
    }

    public function count(){
        $ret=0;
        if(Cache::check('partneri_search',$this->hash().'_count')){ $ret=Cache::get('partneri_search',$this->hash().'_count'); } else {
            //$ret = mysqli_num_rows(mysqli_query(CommerceDB::$DB,$this->query()  ));
	        $this->counting = true;
	        $R = $this->netteQuery()->fetch();
	        $ret = $R["pocet"];
	        $this->counting = false;
            Cache::put('partneri_search',$this->hash().'_count',$ret);
        };
        return $ret;
    }

}


?>
