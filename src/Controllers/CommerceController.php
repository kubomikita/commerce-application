<?php

use \Nette\Utils\Strings,
	\Nette\Reflection\ClassType,
	\Nette\Utils\Callback;
/**
 * @property stdClass $template
 */
abstract class CommerceController implements ArrayAccess {
	const ALERT_UNAUTHORIZED = "Nieste prihláseny. Prosím prihláste sa.";

	public $defaultView = "default";
	public $objIcon = 'icon_eshop.gif';
	public $objImage = 'img_eshop.gif';
	protected $path;

	public $commerce = true;
	/** @var \Latte\Engine */
	protected $latte;
	protected $latteTemplate;
	protected $lattePath = APP_DIR."view/";

	protected $OBJECTS = [];

	/** @var \Kubomikita\Commerce\Microdata */
	protected $microdata;
	/** @var \Kubomikita\Commerce\ConfiguratorInterface */
	public $context;
	/** @var \Kubomikita\Commerce\ConfiguratorInterface */
	public $container;
	/** @var \Nette\Http\Request */
	protected $httpRequest;
	/** @var \Nette\Http\Response */
	protected $httpResponse;
	/** @var \Nette\Security\User */
	//protected $user;
	/** @var \Nette\Database\Connection */
	protected $db;
	/** @var stdClass */
	protected $template;

	protected $components = [];
	private  $component_init = false;
	/**
	 * @deprecated
	 * @var array
	 */
	protected $FORM = [];

	public function setObjects(array $obj){
		$this->OBJECTS = $obj;
	}

	/**
	 * @deprecated
	 * @param $name
	 *
	 * @throws \Nette\Application\BadRequestException
	 */
	public function setLatteTemplate($name){
		$this->setView($name);
	}

	public function setView(string $view) : void
	{
		$names = [$view, Strings::firstLower($view), Strings::firstUpper($view)];

		foreach ($names as $name){
			$file = $this->getLattePath($name);
			if(file_exists($file)){
				$this->latteTemplate = $name;
				break;
			}
		}
		if($this->latteTemplate === null){
			throw new \Nette\Application\BadRequestException("Template not found. Missing template '".str_replace($this->lattePath,"",$file)."'");
		}
	}
	protected function getLattePath($view = null) : string
	{
		if($view !== null) {
			if(!Strings::endsWith($view,".latte")){
				$view .= ".latte";
			}
			return $this->lattePath.static::getId()."/".$view;
		} elseif($this->latteTemplate !== null){
			return $this->lattePath.static::getId()."/".$this->latteTemplate;
		}
		return $this->lattePath.static::getId()."/".static::getId();
	}
	public function isLatteSupported($params = null) : bool{
		if($this->latte === null){
			$this->checkLatte($params);
		}
		return ($this->latte instanceof Latte\Engine);
	}
	protected function checkLatte( $params = null){
		$view = isset($params["view"]) ? $params["view"] : $this->latteTemplate;
		$e =explode(".",$view);
		if(count($e) == 1 || end($e) == "latte" || !isset($params["view"])) {
			if (end($e) == "latte") {
				$this->setView( $e[0] );
			}
			$latteTemplate = $this->getLattePath() . ".latte";
			if ( file_exists( $latteTemplate ) ) {
				return $latteTemplate;
			}
		}

		return false;
	}

	public function __construct() {
		$this->container = $this->context = \Kubomikita\Service::get("container");
		$this->db = $this->context->getByType(\Nette\Database\Connection::class);
		$this->microdata = $this->context->getService("microdata");
		$this->httpRequest = $this->context->getService("request");
		$this->httpResponse = $this->context->getService("response");
		$this->latte = $this->context->getService( "latte" );

		$this->template = new stdClass();
		//$this->user = $this->getUser();

		$this->defaultView = static::getId();
		$this->path = APP_DIR."view/".static::getId()."/*";

		$dv = $this->lattePath.static::getId()."/".$this->defaultView.".phtml";
		$dir = $this->lattePath.static::getId()."/";
		if(!is_dir($dir)){
			mkdir($dir,0777);
		}

	}
	
	private function loadComponents() : void{
		//$c = ClassType::from($this);
		$c = new ReflectionClass($this);
		foreach($c->getMethods() as $method){
			if(Strings::contains($method->name,"createComponent")) {
				$key = Strings::firstLower(Strings::replace($method->name,"/createComponent/",""));
				$callback = [$this,$method->name];
				$this->components[$key] = $callback($key);
			}
		}
		$this->component_init = true;
	}

	public function getComponent(?string $key){
		if(!$this->component_init){
			$this->loadComponents();
			$this->component_init = true;
		}
		if(!isset($this->components[$key])){
			throw new \Nette\InvalidStateException("Component with name '$key' not exists in '".get_class($this)."'");
		}
		return $this->components[$key];
	}

	public function removeComponent(?string $key) : void{
		unset($this->components[$key]);
	}

	public function addComponent($component, string $name) :void {
		$this->components[$name] = $component;
	}

	/**
	 * Default params for every Controller
	 * @return array
	 */
	public function objParams(){
		$parm[0]["id"] = 'view';
		$parm[0]["type"] = 'SEL';
		$parm[0]["title"] = 'View';
		$folder=glob($this->path);
		if(!empty($folder)){
			$multi = [];
			foreach($folder as $f){
				$file = str_replace($this->lattePath.static::getId()."/","",$f);
				$parm[0]["vals"][$file] = $file;
			}

		}

		return $parm;
	}
	public function adminRender($pageData){
		return '<img src="img/rse/'.$this->objIcon.'"> <strong>'.$this->objName.'</strong>';
	}
	public function adminRenderInfo($pageData){
		if(!empty($pageData->params)){
			$out = "";
			foreach($pageData->params as $k=>$v){
				if(strlen(strip_tags($v)) > 0) {
					$out .= '<strong>'.$k . ':</strong> ' . strip_tags($v) . "<br>";
				}
			}
		}
		return $out;
	}

	public function onStartup(){
		$this->loadComponents();
	}

	public function beforeRender(){

	}
	public function getCanonical(){
		/** @var \Nette\Http\UrlScript $URL */
		$URL       = $this->container->getService("request")->getUrl();
		$uri       = $URL->getAbsoluteUrl();
		$euri      = explode( "?", $uri );
		$CANONICAL = preg_replace( "~/page-[0-9]+~", "", $euri[0] );
		return $CANONICAL;
	}

	public function redirect(string $url,int $code = \Nette\Http\Response::S302_FOUND) :void {
		if($url === "this"){

			if($this->httpRequest->getReferer() !== null) {
				$url = ( $this->httpRequest->getReferer()->getPath() );
			} else {
				$url = $this->httpRequest->getUrl()->getScriptPath();
			}
			//exit;
		}
		$this->httpResponse->redirect($url, $code);
		exit;
	}

	protected function parseQueryParameters(string $method, array $query = []) : array {
		$args = [];
		$reflection = new Nette\Reflection\Method($this, $method);
		foreach ($reflection->getParameters() as $param){
			if(!$param->isOptional() && !isset($query[$param->getName()])){
				throw new \Nette\InvalidArgumentException("Missing required parameter '$".$param->getName()."' in '".get_called_class()."::".$method."()'");
			}
			if($param->getType() && isset($query[$param->getName()])){
				settype($query[$param->getName()], (string) $param->getType()->getName());
			}
			$args[] = $query[$param->getName()];
		}

		return $args;
	}

	public function render($page = [], $params = [], $VARS = []){
		$latteTemplate = $this->checkLatte($params);
		if($latteTemplate && $this->latte !== null){
			$this->onStartup();
			$e = explode("/",$latteTemplate);
			$name = str_replace(".latte","",end($e));
			$method = "action".\Nette\Utils\Strings::firstUpper($name);
			$anotherMethod = "render".Strings::firstUpper($name);
			//bdump($this->context->getByType(\Kubomikita\Commerce\ApplicationInterface::class)->getDefaultTemplateParameters());
			$parameters = [
				//"basePath"=> $this->context->getParameter("shop","relurl"),
				//"baseUrl"=> $this->context->getParameter("shop","url"),
				"selfUrl" => $this->getCanonical(),
				"httpRequest" => $this->httpRequest,
				"httpResponse" => $this->httpResponse,
				"HOST" => $this->httpRequest->getUrl()->getHost(),
				"params"=>$params,
				"PAGE" => isset($page["pageobject"]) ? $page["pageobject"] : null,
				"controller" => $this,
				//"BOX" => $this->context->getByType(\Kubomikita\Commerce\ApplicationInterface::class)->getBoxes()
			];
			//bdump($parameters);
			//bdump($this->context->getByType(\Kubomikita\Commerce\ApplicationInterface::class)->getBoxes());
			//bdump($page["pageobject"]->template()->fullBoxes());

			if(method_exists($this,$method) || method_exists($this,$anotherMethod)){
				if($this->httpRequest->isAjax() /*&& $this->httpRequest->getQuery("do")*/){
					$callbackFunction = [ $this, method_exists($this,$method) ? $method : $anotherMethod ];
					$query    = $this->httpRequest->getQuery();
					unset( $query["seolink"], $query["do"], $query["presenter"] );
					$args = $this->parseQueryParameters($callbackFunction[1], $query);
					$callback = $callbackFunction( ... $args );
				} else {
					$callback = $this->{$method}( $page, $params, $VARS );
				}
				if ( is_array( $callback ) ) {
					$VARS += $callback;
				}
			}
			if(!empty($VARS)){
				foreach($VARS as $k=>$v){
					$parameters[$k] = $v;
				}
			}
			$this->beforeRender();
			return $this->latte->renderToString($latteTemplate, $parameters + (array) $this->template + $this->context->getByType(\Kubomikita\Commerce\ApplicationInterface::class)->getDefaultTemplateParameters());
		}
	}

	public function link(string $target, array $args = []) : ?string {
		if($target === 'this'){
			return $this->httpRequest->getUrl()->getScriptPath();
			//exit;
		}
		$reflection = new ReflectionClass($this);
		if(Strings::endsWith($target, "!")){
			$method = "handle".Strings::firstUpper(rtrim($target,"!"));
			$type = "signal";
			$exists = $reflection->hasMethod($method);
		} else {
			$method = "action".Strings::firstUpper(rtrim($target,"!"));
			$anotherMethod = "render".Strings::firstUpper($target);
			$type = "action";
			$exists = $reflection->hasMethod($method) || $reflection->hasMethod($anotherMethod);
		}

		if(!$exists){
			$message = "Unknown $type. '".$method."' in '".get_called_class()."' not exists.";
			trigger_error($message, E_USER_WARNING);
			return "#" . $message;
		}
		$url = $this->httpRequest->getUrl()->getScriptPath();
		$link = new \Nette\Http\UrlScript($url);
		$query = [];
		$query["do"] = rtrim($target,"!");
		$query["presenter"] = $reflection->getName();
		$query += $args;
		return (string)$link->withQuery($query);
	}

	public function run() {
		return $this->objRender(null, []);
	}

	public function objRender($page,$params,$VARS = []){
		if(($do = $this->httpRequest->getQuery("do")) !== null && $this->httpRequest->getQuery("presenter") === get_called_class()){
			$method = "handle" . Strings::firstUpper( $do );
			if ( method_exists( $this, $method ) ) {
				$callback = [ $this, $method ];
				$query    = $this->httpRequest->getQuery();
				unset( $query["seolink"], $query["do"], $query["presenter"] );
				$args = $this->parseQueryParameters($method, $query);
				$callback( ... $args );
			} else {
				trigger_error("Unknown signal. '".$method."' in '".get_called_class()."' not exists.", E_USER_WARNING);
			}

		}
		$latteRender = $this->render( $page, $params, $VARS );
		if ( $latteRender !== null ) {
			return $latteRender;
		}



		$view=(isset($params["view"]) && $params["view"] != null)?str_replace(".phtml","",$params["view"]):$this->defaultView;
		$path=/*dirname(__DIR__).*/APP_DIR."view/".static::getId()."/".$view."";

		/*if(strpos(".phtml",$path) !== false){

		}*/
		$path.=".phtml";



		$ECHELON_CALL = false;
		if(file_exists($path)){
			//$VARS["echelon_db"] = self::$echelon_db;
			ob_start();
			if(!empty($VARS)){
				foreach ($VARS as $key => $value) {
					${$key} = $value;
				}
				$VARS=array();
				unset($VARS);
				unset($key);
				unset($value);
			}
			if(isset($params["box"])) echo static::$prepend;
			$ECHELON_CALL = true;
			include/*_once*/ $path;
			if(isset($params["box"])) echo static::$append;
			$ret=  ob_get_contents();
			ob_end_clean();
			return $ret;
		} else {
			return "<div class=\"alert alert-danger\"><b>Chyba:</b> View <b>".str_replace(dirname(__DIR__),"commerce",$path)."</b> neexistuje.</div>";
		}

	}

	/**
	 * @return string
	 */
	public static function getKey() :string {
		$c = static::getControllerName();
		$keys = splitAtUpperCase($c);
		$key= "eshop_".strtolower(implode("_", $keys));
		return $key;
	}

	/**
	 * @return string
	 */
	public static function getId() :string {
		return static::getControllerName();
	}

	/**
	 * @deprecated Use new ControllerService -> create
	 */
	public static function startup() : array{
		/** @var \Kubomikita\Commerce\ControllerService $controller */
		$controller = Registry::get("container")->getService("controller");
		$objects = $controller->create();

		return $objects;
	}

	/**
	 * @return string
	 */
	protected static function getControllerName(): string {
		return str_replace(["_controller","Controller","Presenter"],["","",""],get_called_class());
	}

	/**
	 * @return \Nette\Security\User
	 */
	public function getUser(){
		/** @var \Nette\Security\User $user */
		$user = $this->context->getService("user");
		return $user;
	}

	public function getSession($section = null){
		/** @var \Nette\Http\Session $session */
		$session = $this->context->getService("session");
		if($section !== null){
			$ses = $session->getSection($section);
			$ses->setExpiration(0);
			return $ses;
		}
		return $session;
	}

	/**
	 * @return Partner
	 */
	public function getPartner(){
		/** @var Partner */
		$partner = Auth::$User;
		if($partner == null){
			$partner = new Partner();
		}
		return $partner;
	}

	/**
	 * @param \Kubomikita\Form $form
	 */
	public static function defaultFormOnError(\Kubomikita\Form $form){
		$errors = [];
		$errors += $form->getErrors();
		$selector = null;
		foreach($errors as $domid => $error){
			$e = explode("|",$domid);
			$selector = $e[0];
			break;
		}
		echo '<script>
			$(document).ready(function(){
				var elem = $("#'.$selector.'");
				var offset = elem.offset().top - 35;
				if(!elem.is(":in-viewport")){
					$("html,body").animate({
						scrollTop: offset
					},500);
				}
			});
		</script>';
	}

	public function createTemplate(string $template, array $args = []) : string {
		$args["controller"] = $this;
		//dumpe($args,$template);
		return $this->latte->renderToString($this->lattePath.static::getId().DIRECTORY_SEPARATOR.$template,$args);
	}

	/**
	 * @param string $message
	 * @param string $file
	 * @param array $params
	 *
	 * @return string
	 */
	public function createMailTemplate(string $message, string $file = "message.latte", array $params = []) : string {
		$params["message"] = $message;
		return $this->latte->renderToString(APP_DIR."view/@mail/" . $file, $params);
	}

	public function offsetExists( $offset ) {
		return isset($this->components[$offset]);
	}
	public function offsetGet( $offset ) {
		return $this->getComponent($offset);
	}
	public function offsetSet( $offset, $value ) {
		$this->addComponent($value,$offset);
	}
	public function offsetUnset( $offset ) {
		$this->removeComponent($offset);
	}
}
