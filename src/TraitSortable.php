<?php

namespace Kubomikita\Core;

use Nette\InvalidStateException;

trait TraitSortable {

	public function saveSortableTrait() {
		if($this->id == null) {
			$this->{static::$sortableColumn} = $this->setOrder();
		}
	}
	public function deleteSortableTrait(){
		$i = 1;
		$where = [];
		if(isset(static::$sortableGroup)){
			$where[static::$sortableGroup] = $this->getGroup();
		}
		foreach (static::fetchAll($where,static::$sortableColumn." ASC") as $item){
			$item->save([static::$sortableColumn => $i]);
			$i++;
		}
	}

	private function init(){
		if(!isset(static::$sortableColumn)){
			throw new InvalidStateException("Static property '\$sortableColumn' must be defined when using Trait 'Sortable'.");
		}
	}
	private function getOrder(){
		return $this->{static::$sortableColumn};
	}
	public function setOrder(){
		$this->init();
		$where = [];
		if(isset(static::$sortableGroup)){
			$where[static::$sortableGroup] = $this->getGroup();
		}
		$r = $this->db->query("SELECT max(".static::$sortableColumn.") as poradie FROM ".static::$table." WHERE",$where)->fetch();
		return $r->poradie + 1;
	}

	private function getGroup(){
		return $this->{static::$sortableGroup};
	}

	public function getPrev(){
		$this->init();
		$where = [static::$sortableColumn." <" => $this->getOrder()];
		if(isset(static::$sortableGroup)){
			$where[static::$sortableGroup] = $this->getGroup();
		}
		$r = $this->db->query("SELECT id FROM ".static::$table." WHERE",$where,"ORDER BY ".static::$sortableColumn." DESC LIMIT 1")->fetch();
		if($r){
			return new static($r->id);
		}
		return false;
	}

	public function getNext(){
		$this->init();
		$where = [static::$sortableColumn." >" => $this->getOrder()];
		if(isset(static::$sortableGroup)){
			$where[static::$sortableGroup] = $this->getGroup();
		}
		$r = $this->db->query("SELECT id FROM ".static::$table." WHERE",$where,"ORDER BY ".static::$sortableColumn." ASC LIMIT 1")->fetch();
		if($r){
			return new static($r->id);
		}
		return false;
	}

	public function swapOrder($pair){
		$tmp= $this->getOrder();
		$this->{static::$sortableColumn}=$pair->{static::$sortableColumn};
		$pair->{static::$sortableColumn}=$tmp;
		$this->save();
		$pair->save();
	}

	public function movableUp(){
		return !!$this->getPrev()->id;
	}
	public function movableDown(){
		return !!$this->getNext()->id;
	}
	public function moveTop(){
		if($this->movableUp()){
			$where = [];
			if(isset(static::$sortableGroup)){
				$where[static::$sortableGroup] = $this->getGroup();
			}
			$r = $this->db->query("SELECT min(".static::$sortableColumn.") as poradie FROM ".static::$table." WHERE",$where)->fetch();
			$this->{static::$sortableColumn}=( $r->poradie - 1 );
			$this->save();
		};
	}
	public function moveBottom(){
		if($this->movableDown()){
			$where = [];
			if(isset(static::$sortableGroup)){
				$where[static::$sortableGroup] = $this->getGroup();
			}
			$r = $this->db->query("SELECT max(".static::$sortableColumn.") as poradie FROM ".static::$table." WHERE",$where)->fetch();
			$this->{static::$sortableColumn}=($r->poradie + 1);
			$this->save();
		};
	}
	public function moveUp(){
		if($this->movableUp()){
			$this->swapOrder($this->getPrev());
		};
	}
	public function moveDown(){
		if($this->movableDown()){
			$this->swapOrder($this->getNext());
		};
	}

}