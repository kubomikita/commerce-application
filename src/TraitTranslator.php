<?php
namespace Kubomikita\Core;

use Kubomikita\Service;
use Kubomikita\Utils\Arrays;
use Nette\InvalidArgumentException;
use Tracy\Debugger;

trait Translator {
	/** @var ITranslator */
	static protected $translator = [];

	protected $translate = true;
	protected $object;

	public function getTranslator(){
		$classname = (__CLASS__)."Translator";
		if(static::$translator[$classname] === null){
			if(!class_exists($classname)){
				throw new \Exception("Translator class '$classname' not found. Skipping translate.");
			}
			$translator = new $classname($this->id);
			//$t = (__CLASS__);
			//bdump(new $t($this->id));
			static::$translator[$classname] = $translator;
			//return $translator;
		}
		return static::$translator[$classname];

	}
	private function sanitizeProperty($prop){
		return explode("|",$prop);
	}
	public function translate($object){
		if($object->id) {
			try {
				$this->object = $object;
				/** @var \Nette\DI\Container $container */
				$container = Service::get( "container" )->getByType( \Nette\DI\Container::class );
				$params    = $container->parameters["translations"];
				if ( ! isset( $params["googleTranslateKey"] ) && $params["googleTranslateKey"] == "" ) {
					throw new InvalidArgumentException( "Parameter 'googleTranslateKey' in section 'translations' not defined." );
				}

				$translator      = $this->getTranslator();
				//bdump($translator);
				//bdump($object);
				$textToTranslate = $propertyToTranslate = [];
				foreach ( $translator->toTranslate() as $p ) {
					$sanit = $this->sanitizeProperty($p);
					if(count($sanit)  ==  2) {
						$textToTranslate[] = (string) $object->{$sanit[0]}[ $sanit[1] ];
					} elseif (count($sanit) == 3){
						$textToTranslate[] = (string) $object->{$sanit[0]}[ $sanit[1] ][$sanit[2]];
					} else {
						$textToTranslate[] = (string) $object->{$p};
					}
					$propertyToTranslate[] = $p;
				}

				$len_old = 0;
				$cycles  = array();
				foreach ( $textToTranslate as $k => $a ) {
					$alen = strlen( (string) $a );
					if ( $alen > 4000 ) {
						$cycles[ $k ] = str_split( preg_replace( '/\s+/u', ' ', ( (string) $a ) ), 2000 );
					} else {
						$cycles[ $k ][0] = "" . ( preg_replace( '/\s+/u', ' ', ( (string) $a ) ) );
					}
					$len_old += strlen( (string) $a );
				}


				$NEWARRAY = array();
				$len      = 0;
				foreach ( $cycles as $ki => $cyc ) {
					foreach ( $cyc as $cycc ) {
						$get   = \HTTP::get( "https://www.googleapis.com/language/translate/v2?key=" . $params["googleTranslateKey"] . "&source=sk&target=cs&q=" . urlencode( $cycc ) );
						$trans = json_decode( $get );
						if ( $trans->data->translations != null and ! empty( $trans->data->translations ) ) {
							foreach ( $trans->data->translations as $k => $t ) {
								$new = str_replace( "[Vlastnost: ", "[Vlastnost:",
									str_replace( "[Vyrobce: ", "[Vyrobca:", (string) $t->translatedText ) );
								if ( \Str::starts_with_upper( $textToTranslate[ $ki ] ) ) {
									$new = ucfirst( $new );
								}
								if ( $new == "" and $textToTranslate[ $ki ] != "" ) {
									$this->translate = false;
									break;
								}
								$len                                                  += strlen( $new );
								$NEWARRAY[ $ki ][ $propertyToTranslate[ $ki ] ]["sk"] = $textToTranslate[ $ki ];
								$NEWARRAY[ $ki ][ $propertyToTranslate[ $ki ] ]["cz"] .= $new;
							}
						} else {
							$this->translate = false;
						}
					}
				}
				if ( ! empty( $NEWARRAY ) ) {
					foreach ( $NEWARRAY as $NEWARRAY1 ) {
						foreach ( $NEWARRAY1 as $kkk => $TTT ) {
							list( $var, $key, $key1 ) = explode( "|", $kkk );
							if ( $key == null ) {
								$this->{$var} = new \LangStr( array( "sk" => $TTT["sk"], "cz" => $TTT["cz"] ) );
							} else {
								if ( $key1 == null ) {
									$this->{$var}[ $key ] = new \LangStr( array(
										"sk" => $TTT["sk"],
										"cz" => $TTT["cz"]
									) );
								} else {
									$this->{$var}[ $key ][ $key1 ] = new \LangStr( array(
										"sk" => $TTT["sk"],
										"cz" => $TTT["cz"]
									) );
								}
							}
						}
					}
				}

				if ( $this->translate ) {
					$this->saveTranslate();
					return true;
				} else {
					trigger_error( "Vyskytol sa problem pri prekladaní " . __CLASS__ . "" );
				}
			} catch ( \Exception $e ) {
				trigger_error( $e->getMessage() );
			}
		}
		return false;
	}

	public function saveTranslate(){
		$translator = $this->getTranslator();
		$save = [];
		foreach($translator->toTranslate() as $key){
			$sanit = $this->sanitizeProperty($key);
			if(count($sanit) == 2) {
				$save[ $sanit[0] ][ $sanit[1] ] = $this->object->{$sanit[0]}[ $sanit[1] ];
			} elseif (count($sanit) == 3) {
				$save[ $sanit[0] ][ $sanit[1] ][$sanit[2]] = $this->object->{$sanit[0]}[ $sanit[1] ][$sanit[2]];
			} else {
				$save[ $key ] = $this->object->{$key};
			}
		}
		if(isset($this->translated)) {
			$this->translated = 1;
			$save["translated"] = 1;
		}
		//dump($save);
		$this->object->save($save);
	}
}

