<?php
namespace Kubomikita\Latte\Macro;

use Kubomikita\Commerce\DataLayer\Container;
use Kubomikita\Commerce\DataLayer\DataLayer;
use Latte\Compiler;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;
use Latte\Tokenizer;

class CommerceMacro extends MacroSet{
	public static function install(Compiler $compiler){
		$me = new static($compiler);
		$me->addMacro("cache", [$me, "macroCacheStart"],[$me, "macroCacheEnd"]);
		$me->addMacro("minifyjs", [$me, "macroMinifyjs"]);
		$me->addMacro("minifycss", [$me, "macroMinifycss"]);
		$me->addMacro("dataLayer", [$me, "macroDataLayer"]);
	}

	public function macroDataLayer(MacroNode $node,PhpWriter $writer){

		$name = Container::CONTAINER_TOP;
		$initDataLayer = true;
		$getname = $node->tokenizer->fetchWords()[0];
		if(isset($getname)){
			$name = $getname;
		}
		$init = '';
		$params = $node->tokenizer->nextAll();
		if(!empty($params)){
			$value = end($params)[0];
			$init = '->setInitDataLayer('.(String) $value .')';
		}
		$ret = $writer->write('echo \\Kubomikita\\Service::get("dataLayerContainer")->setType("'.$name.'")'.$init.'->getContainer();');
		return $ret;
	}

	public function macroCacheStart(MacroNode $node, PhpWriter $writer){
		$ret = $writer->write("if (Cache::check(%node.args)): ");
		$ret .= $writer->write("echo Cache::get(%node.args);");
		$ret .= $writer->write("else:");
		$ret .= $writer->write("ob_start();");
		return $ret;
	}
	public function macroCacheEnd(MacroNode $node, PhpWriter $writer){
		$ret = $writer->write('$output = ob_get_clean();');
		$ret .= $writer->write('echo $output;');
		$ret .= $writer->write('Cache::put(%node.args,$output);');
		$ret .= $writer->write("endif");
		return $ret;
	}
	public function macroMinifyjs(MacroNode $node, PhpWriter $writer){
		$ret = $writer->write('$min = new Kubomikita\Commerce\Minifier(%node.args,new MatthiasMullie\Minify\JS());');
		$ret .= $writer->write('echo "\t".$min->output()."\n";');
		//$ret .= $writter->write('dump(%node.args,$min);');
		return $ret;
	}
	public function macroMinifycss(MacroNode $node, PhpWriter $writer){
		$ret = $writer->write('$min = new Kubomikita\Commerce\Minifier(%node.args,new MatthiasMullie\Minify\CSS());');
		$ret .= $writer->write('echo "\t".$min->output()."\n";');
		//$ret .= $writter->write('dump(%node.args,$min);');
		return $ret;
	}
}