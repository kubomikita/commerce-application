<?php


namespace Kubomikita\Api;


use Kubomikita\Commerce\Routing\Api\BaseApi;
use PHPMailer\PHPMailer\Exception;

class Pohoda extends BaseApi {

	public function beforeStartup() {
		if($this->args["key"] != \BridgeConfig::pohoda_key){
			throw new \Exception("Access denied");
		}
	}

	public function actionSkladPush(){
		echo "hello world";
	}
	public function actionObjednavkaList(){
		header("Content-type: text/plain");
		$Search = new \ObjednavkaSearchResult();
		$Search->pohoda_export=true;
		$i=0;
		foreach($Search->result() as $O){
			if((int)$i<\BridgeConfig::pohoda_objednavky_max_at_once && $O->metadata['ready'] && !$O->metadata['pohoda_exported']){
				echo($O->id."|");
				$i++;
			};
		};
	}
	public function actionObjednavkaCommit(){
		$id = $this->args["id"];
		$O = new \Objednavka($id);
		$O->metadata['pohoda_exported']=1;
		//$O->metadata['pohoda_export_count']++; // asi mu jebe a ide sync navyse
		$O->flush_rezervacia();
		$O->save();
		\Params::set('pohoda_offline',0);
	}
}