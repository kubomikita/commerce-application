<?php


namespace Kubomikita\Commerce\Api;


use Kubomikita\Commerce\ProteinConfigurator;
use Kubomikita\Commerce\Routing\Api\BaseApi;
use Nette\Application\BadRequestException;
use Nette\Utils\Strings;

class Voucher extends BaseApi {
	public $types =array(
		"small"=>array(
			"image"=>WWW_DIR."images/poukazka_small.jpg",
			"fontsizeValue" => 23,
			"fontsizeCode" => 22,
			"valueLeft" => 210,
			"valueTop" => 290,
			"codeLeft" => 270,
			"codeTop" => 390
		),
		"full"=>array(
			"image"=>WWW_DIR."images/poukazka_full.jpg",
			"fontsizeValue" => 51,
			"fontsizeCode" => 49,
			"valueLeft" => 465,
			"valueTop" => 642,
			"codeLeft" => 598,
			"codeTop" => 864
		)
	);

	public function beforeStartup() {
		// TODO: Implement beforeStartup() method.
	}

	public function actionGet(){
		$type = $this->args["type"];
		$types = $this->types;
		if($this->types[$type]["image"] != null){
			$rImg = ImageCreateFromJPEG($types[$type]["image"]);

			$fontsizeValue = $types[$type]["fontsizeValue"];
			$fontsizeCode = $types[$type]["fontsizeCode"];

			$valueLeft = $types[$type]["valueLeft"];
			$valueTop = $types[$type]["valueTop"];
			$codeLeft = $types[$type]["codeLeft"];
			$codeTop = $types[$type]["codeTop"];

			$black = imagecolorallocate($rImg, 0, 0, 0);
			$white = imagecolorallocate($rImg, 255,255, 255);
			$font = WWW_DIR.'assets/fonts/opensans/OpenSans-Regular.ttf';
			$hodnota = "na nákup v hodnote ". urldecode($this->args["value"])." €";
			$kod = urldecode($this->args["code"]);

			imagettftext($rImg, $fontsizeValue, 0, $valueLeft, $valueTop, $black, $font, $hodnota);
			imagettftext($rImg, $fontsizeCode, 0, $codeLeft, $codeTop, $white, $font, $kod);
			header('Content-type: image/jpeg');
			imagejpeg($rImg,NULL,100);
			exit;
		}
	}

}
