<?php


namespace Kubomikita\Commerce\Api;;


use Commerce\Xml\XmlFeed;
use Kubomikita\Commerce\Routing\Api\BaseApi;
use Nette\Application\BadRequestException;
use Nette\Application\Routers\Route;
use Nette\InvalidStateException;
use Nette\Utils\Strings;

class Feed extends BaseApi {

	public function beforeStartup() {	}

	public function actionShow(string $name){

		$filename = $this->args["name"];
		$e = explode(".",$filename);
		$ext = Strings::firstUpper(end($e));

		//dumpe($e,Route::path2presenter($e[0]));

		$class = "\\Commerce\\".$ext."\\".Route::path2presenter($e[0]);

		if(!class_exists($class)) {
			trigger_error("Feed ".$class." not exists");
			throw new BadRequestException("Feed not exists");
		}
		/** @var $feed XmlFeed */
		$feed = new $class();
		//$feed->debug = true;
		$output = null;
		try {
			$output = $feed->render();
		} catch (InvalidStateException $e){
			echo $e->getMessage();
			dump($e);
		}
		if($output !== null){
			$feed->headers();
			echo $output;
		}

	}

}