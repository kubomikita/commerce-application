<?php


namespace Kubomikita\Commerce\Api;


use Commerce\BackendAuthenticator;
use Commerce\FrontendAuthenticator;
use Kubomikita\Commerce\ProteinConfigurator;
use Kubomikita\Commerce\Routing\Api\BaseApi;
use Kubomikita\Commerce\UserService;
use Nette\FileNotFoundException;
use Nette\Http\Request;
use Nette\Security\AuthenticationException;
use Nette\Security\User;

class Auth extends BaseApi {
	/**
	 * @var User
	 */
	private $User;


	/**
	 *  Ziskanie objektu Nette\Security\User
	 *
	 * @return void
	 */
	public function beforeStartup() {
		$this->User = $this->context->getByType(User::class);
	}
	/**
	 * Kontrola ci je admin prihlaseny
	 *
	 * @throws \Exception
	 * @return void
	 */
	public function beforeStartupMaster() {
		$this->User->getStorage()->setNamespace("admin");

		$User = \Registry::get("user");
		/** @var UserService $UserService */
		$UserService = \Registry::get("user.service");

		$backendAuthentificator = new BackendAuthenticator();
		$User->setAuthenticator($backendAuthentificator);
		$User->getStorage()->setNamespace("admin");
		$UserService->checkCookie("admin");

		$backendUser = $User->isLoggedIn();

		if($backendUser === false) {
			//throw new \Exception( "Access denied" );
		}
	}

	/**
	 *
	 * @return void|string
	 */
	public function actionMaster(){
		$this->beforeStartupMaster();
		try {
			$frontendAuthenticator = new FrontendAuthenticator($this->context->getService("database"));
			$this->User->setAuthenticator($frontendAuthenticator);
			$this->User->getStorage()->setNamespace("front");
			$this->User->login($this->args["uid"], $this->args["pwd"],$this->args["partner_id"]);

			header("Location:".$this->context->getParameter("shop","basehref"));
		} catch (AuthenticationException $e) {
			echo('<div class="alert alert-danger text-center"><i class="fa fa-times"></i> '.$e->getMessage().'</div>');
		}
	}
	public function actionLogin(string $uid, string $pwd, string $ref = null){
		try{
			$frontendAuthenticator = new FrontendAuthenticator($this->context->getService("database"));
			$this->User->setAuthenticator($frontendAuthenticator);
			$this->User->getStorage()->setNamespace("front");
			$this->User->login($this->args["uid"], $this->args["pwd"]);
			if(isset($this->args["ref"])) {
				header( "Location:" . $this->context->getParameter( "shop", "basehref" ).$this->args["ref"] );
			} else {
				header( "Location:" . $this->context->getParameter( "shop", "basehref" ) );
			}
		} catch (AuthenticationException $e){
			echo('<div class="alert alert-danger text-center"><i class="fa fa-times"></i> '.$e->getMessage().'</div>');
		}
	}

	public function actionDownload(){
		$this->beforeStartupMaster();
		try {
			\Tracy\Debugger::$showBar = false;
			if(!isset($this->args["file"])){
				throw new FileNotFoundException();
			}
			$filename = $this->args["file"];
			if(!file_exists(WWW_DIR.$filename)){
				throw new FileNotFoundException();
			}
			$e = explode("/",$filename);
			header( 'Content-Disposition: attachment;filename=' . end($e) );
			readfile(WWW_DIR.$filename);
			exit;
		} catch (FileNotFoundException $e){
			echo "file not found";
		}
	}

	public function actionLogout(){
		if($this->context->isHost('1day.sk') && \Auth::$User->isVO()){
			\Kosik::flush();
		}

		$this->User->getStorage()->setNamespace("front");
		$this->context->getByType(UserService::class)->eraseCookie("front");
		$this->User->logout(true);
		unset($_SESSION["view_list"]);

		$this->redirect($this->request->getQuery("ref"));
		exit;
	}
	public function actionNewsletterDisable(){
		header('Content-type: text/html; charset=UTF-8');
		$db = \Registry::get("database");
		$q = $db->query("DELETE FROM ec_blog_newsletter WHERE email=?",$this->args["email"]);
		if($q->getRowCount()){
			echo 'Úspešne ste sa odhlásili z odberu.';
		}
	}
	public function actionSessionSet(){
		$request = \Registry::get("request");
		$response = \Registry::get("response");
		$_SESSION[$this->args['k']]=$this->args['v'];

		if($request->getReferer()!==null){
			$response->redirect($request->getReferer()->getAbsoluteUrl());
			exit;
		}

	}
	public function actionValidate(){
		$response = \Registry::get("response");

		$token=$this->args['token'];
		$U=\Partner::getByToken($token);

		if($U->id){
			$U->allow_login=1;
			$U->resetToken();
			$U->save();
			$_SESSION["NewUserId"] = $U->id;
			$_SESSION["NewUserName"] = $U->username;
		};
		$response->redirect($this->context->getParameter("shop","url")."registracia-uspesna");
	}

	public function actionInvoice(){
		if($this->User->isLoggedIn()){
			$obj = $this->args["obj"];

			$o = new \Objednavka($obj);

			if($o->id !== null){
				if($o->klient->id === \Auth::$User->id){
					$content = file_get_contents(WWW_DIR."fa/".$o->metadata["faktura"]);
					//dump($content,$o);
					header('Content-Type: application/pdf');
					header('Content-Length: ' . strlen($content));
					header('Content-Disposition: inline; filename="'.$o->metadata["faktura"].'"');
					header('Cache-Control: private, max-age=0, must-revalidate');
					header('Pragma: public');
					ini_set('zlib.output_compression','0');

					die($content);

				} else {
					header('Content-Type: application/json');
					echo json_encode(array("error"=>"Not authorized"));
				}
			} else {
				header('Content-Type: application/json');
				echo json_encode(array("error"=>"Not authorized"));
			}
		} else {
			header('Content-Type: application/json');
			echo json_encode(array("error"=>"Not authorized"));
		}

	}

}