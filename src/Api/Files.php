<?php


namespace Kubomikita\Commerce\Api;


use Kubomikita\Commerce\ConfiguratorInterface;
use Kubomikita\Commerce\Routing\Api\BaseApi;
use Nette\Application\BadRequestException;
use Nette\Http\Request;
use Nette\Http\Response;
use Nette\Utils\Strings;

class Files extends BaseApi {

	public function beforeStartup() {}
	public function actionDownload(){
		$a = $this->args;
		//bdump($a);
		if(count($a) > 1){
			throw new BadRequestException();
		}
		$id = (int) key($a);
		$tp = new \TovarPriloha($id);
		if(!($tp->id > 0)){
			throw new BadRequestException();
		}
		/** @var Request $request */
		$request = $this->request;//$this->context->getService("request");
		/** @var Response $response */
		$response = $this->response;//$this->context->getService("response");


		$file_url = $tp->binUrl();
		$pdfname = Strings::webalize($tp->nazov).".pdf";

		$response->setHeader('Content-Type','application/pdf');
		$response->setHeader("Content-Transfer-Encoding", "Binary");
		$response->setHeader("Content-Disposition","attachment; filename=".$pdfname);
		$response->setHeader('Content-Length', filesize($file_url));

		$response->setHeader('Pragma', null);
		$response->setHeader('Cache-Control', null);

		echo readfile($file_url);
	}

	public function actionInline(int $id, string $filename){
		$tp = new \TovarPriloha($id);
		if(!($tp->id > 0)){
			throw new BadRequestException();
		}

		$file_url = \TovarPriloha::storage_file($tp->id);
		$pdfname = Strings::webalize($tp->nazov).".pdf";

		$this->response->setHeader('Content-Type','application/pdf');
		$this->response->setHeader("Content-Disposition","inline; filename=".$pdfname);
		$this->response->setHeader("Content-Transfer-Encoding", "Binary");
		$this->response->setHeader('Content-Length', filesize($file_url));
		echo @readfile($file_url);
		exit;
	}

}