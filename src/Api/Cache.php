<?php


namespace Kubomikita\Commerce\Api;


use Kubomikita\Commerce\Routing\Api\BaseApi;


class Cache extends BaseApi {
	public function beforeStartup() {
		// TODO: Implement beforeStartup() method.
	}

	public function actionFlush(){
		if(!$this->context->isDebugMode()){
			echo "Access rejected!";
			exit;
		}
		$dir = $this->context->getCacheDirectory()."/";
		$file = "*";
		if(isset($this->args["file"])) { $file = $this->args["file"];}

		foreach(glob($dir.$file) as $f){
			if(is_dir($f)){
				foreach(glob($dir.$file."/*") as $ff){
					if(is_dir($ff)){
						foreach(glob($ff."/*") as $fff){
							@unlink($fff);
							echo $fff.'<br>'.PHP_EOL;
						}
						rmdir($ff);
						echo $f.' - <span style="color:red">cely priecinok vymazany.</span><br>'.PHP_EOL;
					} else {
						@unlink($ff);
						echo $ff.'<br>'.PHP_EOL;
					}

				}
				rmdir($f);
				echo $f.' - <span style="color:red">cely priecinok vymazany.</span><br>'.PHP_EOL;
				//unlink($f);
			} else {
				echo $f.'<br>'.PHP_EOL;
				@unlink($f);
			}
		}
	}
}