<?php


namespace Kubomikita\Api;


use Kubomikita\Commerce\Routing\Api\BaseApi;
use Nette\Database\Connection;
use Nette\Http\Request;
use Nette\Utils\DateTime;

class Payment extends BaseApi {

	public function beforeStartup() {
		// TODO: Implement beforeStartup() method.
	}
	public function actionVubecard(){
		/** @var Request $request */
		$request = \Registry::get("request");
		/** @var Connection $db */
		$db = \Registry::get("database");
		$post = $request->getPost();
		if(!empty($post)) {
			ob_start();
			var_dump($post);
			$request1=ob_get_clean();
			$db->query("INSERT INTO ec_payment_reponse_log",[
				"datum" => new DateTime(),
				"ip" => $_SERVER["REMOTE_ADDR"],
				"mod"=>"vubpay",
				"request"=>$request1
			]);
			/** @var \Payment_VUBPay $p */
			$p     = \Payment::load( "vubeplatby" );
			$p->setOrder($p->objednavka_find($post["oid"]));
			$valid = $p->isValidResponse( $request->getPost() );
			if ( $valid ) {
				if ( $p->getOrder() instanceof \Objednavka && $p->getOrder()->id > 0 ) {
					$suma=$p->getOrder()->suma_spolu()-$p->getOrder()->suma_uhradene();
					$p->getOrder()->uhrada_add(time(),$suma,'ONLINE','VUB ePlatby');
					if($p->getOrder()->klient->isVO()){
						$p->getOrder()->stav_call('pay_commit_vubpay_vo');
					} else {
						$p->getOrder()->stav_call('pay_commit_vubpay');
					}

					header("Location:".\Registry::get("container")->getParameter("shop","relurl")."platba-uspesna?id=".$p->getOrder()->id);
					exit;
				}
			}
			if($p->getOrder()->klient->isVO()){
				$p->getOrder()->stav_call('pay_error_vo');
			} else {
				$p->getOrder()->stav_call( "pay_error" );
			}
			header("Location:".\Registry::get("container")->getParameter("shop","relurl")."platba-nepresla?id=".$p->getOrder()->id);
			exit;
		}
	}
	public function actionCardpay(){
		$PAY=new \Payment_TatraCardpay();
		$O = $PAY->objednavka_find($this->args['VS']);
		$PAY->setOrder($O);
		$PAY->setConfig(\Registry::get("container")->getParameter("payment","tatra_cardpay"));
		//bdump($this->args);
		if(!$O->id){
			throw new \Exception("Order not exists");
		}

		if($this->args["CID"]!="" and $this->args["TRES"] != ""){
			if($PAY->verify_response_hmac($this->args)){
				if($O instanceof \Objednavka && $O->id){
					$suma=$O->suma_spolu()-$O->suma_uhradene();
					$O->uhrada_add(time(),$suma,'ONLINE','Tatrabanka CardPay');
					$O->stav_call('pay_commit_cardpay');

					if(!is_array($O->klient->karty)) {
						$O->klient->karty = array();
					}
					if(!in_array($_GET["CC"],$O->klient->karty)){
						$O->klient->karty[$_GET["CID"]] = $_GET["CC"];
						$O->klient->save();
					}

					header("location: ".\Registry::get("container")->getParameter("shop","relurl")."platba-uspesna?id=".$O->id."&pid=".$O->platba->id);
					exit;
				}
			} else {
				$O->stav_call("pay_error");
				//echo "error call";
			}

			header("Location: ".\Registry::get("container")->getParameter("shop","relurl")."platba-nepresla?id=".$O->id."&pid=".$O->platba->id);
			exit;

		} else {
			if($PAY->verify_response_hmac($this->args)){
				if($O instanceof \Objednavka && $O->id){
					$suma = $O->suma_spolu()-$O->suma_uhradene();
					$O->uhrada_add(time(),$suma,'ONLINE','Tatrabanka CardPay');
					$O->stav_call('pay_commit_cardpay');
					$O->klient->karty=null;
					$O->klient->save();
					header("location: ".\Registry::get("container")->getParameter("shop","relurl")."platba-uspesna?id=".$O->id."&pid=".$O->platba->id);
					exit;
				};
			} else {
				$O->stav_call("pay_error");
			}
			header("Location: ".\Registry::get("container")->getParameter("shop","relurl")."platba-nepresla?id=".$O->id."&pid=".$O->platba->id);
			exit;

		}
	}
	public function actionTatrapay(){
		$PAY=new Payment_TatraTatrapay();
		$O=$PAY->objednavka_find($_REQUEST['VS']);
		$PAY->setOrder($O);
		$PAY->setConfig(\Registry::get("container")->getParameter("payment","tatra_tatrapay"));
		if($PAY->verify_response($_REQUEST)){
			if($O instanceof Objednavka && $O->id){
				$suma=$O->suma_spolu()-$O->suma_uhradene();
				$O->uhrada_add(time(),$suma,'ONLINE','Tatrabanka TatraPay');
				$O->stav_call('pay_commit_tatrapay');

				/*$M=new Mail();
				$M->from=Params::val('e_name_from').' <'.Params::val('e_mail_from').'>';
				$M->to=$O->klient->kontakt['email'];
				$M->subject=LangStr('zaplatená objednávka č. ').$O->cislo;
				$M->content_type='text/html';
				ob_start();
				include('commerce/eshop/mail/platba_ok_tatrapay.php');
				$M->text=ob_get_contents();
				ob_end_clean();
				$M->send();

				$M->to=Params::val('e_mail_to');
				//$M->subject=LangStr('objednávka č. ').$O->cislo.' ('.$O->klient->format_nazov().')';
				$M->text='Potvrdená autorizácia platby cez systém Tatrapay pre objednávku č. '.$O->cislo.' ('.$O->klient->format_nazov().')';
				$M->send();
				*/
				$ok=1;
				header("location: ".\Registry::get("container")->getParameter("shop","relurl")."platba-uspesna?id=".$O->id."&pid=".$O->platba->id);
				exit;
			};
		} else {
			$O->stav_call("pay_error");
		}

		header("Location: ".\Registry::get("container")->getParameter("shop","relurl")."platba-nepresla?id=".$O->id."&pid=".$O->platba->id);
		exit;
	}

	public function actionHomecredit(){
		$PAY=new Payment_Homecredit();
		$O=$PAY->objednavka_find($_REQUEST['hc_o_code']);
		$PAY->setOrder($O);
		$PAY->setConfig(\Registry::get("container")->getParameter("payment","homecredit"));
		if($PAY->verify_response($_REQUEST)){
			if($O instanceof Objednavka && $O->id){
				$suma=$O->suma_spolu()-$O->suma_uhradene();
				$O->uhrada_add(time(),$suma,'ONLINE','HomeCredit');
				$O->stav_call('pay_commit_homecredit');

				/*$M=new Mail();
				$M->from=Params::val('e_name_from').' <'.Params::val('e_mail_from').'>';
				$M->to=$O->klient->kontakt['email'];
				$M->subject=LangStr('zaplatená objednávka č. ').$O->cislo;
				$M->content_type='text/html';
				ob_start();
				include('commerce/eshop/mail/platba_ok_tatrapay.php');
				$M->text=ob_get_contents();
				ob_end_clean();
				$M->send();

				$M->to=Params::val('e_mail_to');
				//$M->subject=LangStr('objednávka č. ').$O->cislo.' ('.$O->klient->format_nazov().')';
				$M->text='Potvrdená autorizácia platby cez systém Tatrapay pre objednávku č. '.$O->cislo.' ('.$O->klient->format_nazov().')';
				$M->send();
				*/
				$ok=1;
				header("location: ".\Registry::get("container")->getParameter("shop","relurl")."platba-uspesna?id=".$O->id."&pid=".$O->platba->id);
				exit;
			};
		} else {
			$O->stav_call("pay_error_homecredit");
		}

		header("Location: ".\Registry::get("container")->getParameter("shop","relurl")."platba-nepresla?id=".$O->id."&pid=".$O->platba->id);
		exit;
	}
}