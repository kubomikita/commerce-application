<?php


namespace Kubomikita\Api;




use Kubomikita\Commerce\Routing\Api\BaseApi;
use Kubomikita\Format;
use Kubomikita\Helpers\SocketException;
use Nette\Application\AbortException;
use Nette\Application\BadRequestException;
use Nette\Application\ForbiddenRequestException;
use Nette\InvalidArgumentException;
use Nette\Utils\DateTime;
use Nette\Utils\Json;
use Tracy\Debugger;

class Part extends BaseApi {

	protected $logRequests = true;
	protected $throwException = true;

	public function beforeStartup() {
		if(!$this->config->api_enabled){
			throw new BadRequestException("API is disabled.");
		}
		Debugger::$showBar = false;
	}

	public function authenticate(): void {
		parent::authenticate();
		if(!((isset($this->args["token"]) && $this->args["token"] == $this->config->api_key) || ($this->bearerToken !== null && $this->bearerToken == $this->config->api_key))){
			throw new ForbiddenRequestException('API key is invalid.');
		}
		//dumpe($this);
	}

	/**
	 * @method GET
	 * @method POST
	 * @param int $orderNumber
	 */
	public function actionOrderDelivered(int $orderNumber) {
		$o = \Objednavka::getByCislo($orderNumber);
		if($o instanceof \Objednavka && $o->id > 0){
			$o->stav_call("cc-delivered");
			$this->sendJsonResponse([]);
		}
		$this->sendJsonResponse(["response"=> ["code" => 500,"message" => "Order not exists."]], 500);
	}

	/**
	 *
	 * @method POST
	 * @param int $orderNumber
	 */
	public function actionOrderPrepared(int $orderNumber) {
		$o = \Objednavka::getByCislo($orderNumber);
		if($o instanceof \Objednavka && $o->id > 0){
			try {
				if ( strlen( trim( ( $rawData = $this->request->getRawBody() ) ) ) > 0 ) {
					$data = Json::decode( $rawData, Json::FORCE_ARRAY );
					$result = [];
					foreach($data["items"] as $d){
						$result[$d["Code"]] = ["nedodane" => $d["nedodane"], "quantity" => $d["Quantity"], "priceUnit" => $d["priceUnit"], "priceTotal" => $d["price"]];
					}

					$products = \Tovar::getByEan(array_keys($result));
					$productsIds = [];
					$productsResult = $productsResultNedodane =  [];
					$price = [0];
					/** @var \Tovar $p */
					foreach($products as $p) {
						if($p->id > 0){
							$productsIds[] = $p->id;
							$row = $result[$p->ean];
							if($row["quantity"] > 0) {
								$price[] = $row["priceTotal"];
								$productsResult[] = '<tr><td align="center"><img src="' . $this->context->getParameter( "shop",
										"url" ) . $p->image()->resizedCachedUrl( "fit", 30,
										30 ) . "\"></td><td>$p->nazov</td><td align=\"center\">" . $row["quantity"] . " ks</td><td align=\"right\"> " . \Format::money( $row["priceTotal"] ) . "</td></tr>";
							}
							if($row["nedodane"] > 0) {
								$productsResultNedodane[] = '<tr><td align="center"><img src="' . $this->context->getParameter( "shop",
										"url" ) . $p->image()->resizedCachedUrl( "fit", 30,
										30 ) . "\"></td><td>$p->nazov</td><td align=\"center\">" . $row["nedodane"] . " ks</td><td align=\"right\"> " . \Format::money( $row["priceUnit"] ) . "</td></tr>";
							}
						}
					}
					ob_start();
					echo '<table width="100%" border="0" cellspacing="0" cellpadding="1">';
					echo '<tr><th></th><th>názov</th><th align="center">množst.</th><th align="right">cena</th></tr>';
					echo implode("\n", $productsResult);
					echo '<tr><td colspan="3" align="right"><strong>Spolu:</strong></td><td align="right">'.\Format::money(array_sum($price)).'</td></tr>';
					echo '</table>';

					if(count($productsResultNedodane) > 0) {
						$unprepared = [];
						//echo "\n".'<strong> Ospravedlňujeme sa,</strong> ale tieto produkty sme Vám nepripravili z dôvodu nedostatočnej skladovej zásoby a výpadku na strane dodávateľa:'."\n\n";
						echo "\n".'<strong> Ospravedlňujeme sa,</strong> ale produkty vypísané nižšie sme Vám nemohli pripraviť z dôvodu nedostatočnej zásoby vo Vami zvolenej predajní, alebo sme zistili, že vybraný tovar je poškodený, alebo mu skončila záručná doba a je nepredajný.';
						echo '<table width="100%" border="0" cellspacing="0" cellpadding="1">';
						echo '<tr><th></th><th>názov</th><th align="center">množst.</th><th align="right">cena</th></tr>';
						echo implode("\n", $productsResultNedodane);
						//echo '<tr><td colspan="3" align="right"><strong>Spolu:</strong></td><td align="right">'.\Format::money(array_sum($price)).'</td></tr>';
						echo '</table>';
						//dump($o->items(), $productsIds);
					}

					$productsResult = ob_get_clean();

					$store = str_replace('Na adrese: ',"",$o->metadata["predajna_adresa"]);
					if(isset($o->metadata["predajna_id"]) && $o->metadata["predajna_id"] > 0){
						$predajna = new \PredajnaModel($o->metadata["predajna_id"]);
						$predajna_link = $this->context->getParameter("shop","url").$predajna->link();
						$store .= "<br><strong>Otváracie hodiny predajne nájdete na: <br> <a href='".$predajna_link."'>".$predajna_link."</a></strong>";
					}

					//dumpe($productsResult);
					//implode("\n", $productsResult);
					//exit;
					//dumpe($result,$products,$productsResult);
					$o->stav_call("pickup", ["{order}" => $o->cislo,"{store}" => $store/*$o->metadata["predajna_adresa"]*/, "{products}" =>  $productsResult]);
					$this->sendJsonResponse([]);
				}
			} catch (\Throwable $e){
				$response["code"] = 500;
				$response["message"] = 'Posted bad data.';
				$this->sendJsonResponse(["response" => $response]);
			}

		}
		$this->sendJsonResponse(["response"=> ["code" => 500,"message" => "Order not exists."]], 500);
	}

	/**
	 * @method GET
	 * @method POST
	 * @param int $orderNumber
	 */
	public function actionOrderCancelled(int $orderNumber) {
		$o = \Objednavka::getByCislo($orderNumber);
		if($o instanceof \Objednavka && $o->id > 0){
			$o->stav_call("cc-cancelled");
			$this->sendJsonResponse([]);
		}
		$this->sendJsonResponse(["response"=> ["code" => 500,"message" => "Order not exists."]], 500);
	}

	/**
	 * @method POST
	 * @method GET
	 * @param int $storeId
	 */
	public function actionSell(int $storeId) {
		$data = [];
		try {
			if ( strlen( trim( ( $rawData = $this->request->getRawBody() ) ) ) > 0 ) {
				$data = Json::decode( $rawData, Json::FORCE_ARRAY );
				$result = [];
				foreach($data["Zasoba"] as $d){
					$result[$d["Code"]] = $d["Quantity"];
				}

				//\Tovar::sklad_predajna_update($storeId, $result);

			}
		} catch (\Throwable $e){
			$response["code"] = 500;
			$response["message"] = 'Posted bad data.';
			$this->sendJsonResponse(["response" => $response]);
		}

		$this->sendJsonResponse(["storeId" => $storeId, "recievedData" => $result/*, "bearer" => $this->bearerToken*/]);
	}

	/**
	 * @method POST
	 * @method GET
	 * @param int $storeId
	 */
	public function actionStock(int $storeId) {
		$data = [];
		try {
			if ( strlen( trim( ( $rawData = $this->request->getRawBody() ) ) ) > 0 ) {
				$data = Json::decode( $rawData, Json::FORCE_ARRAY );
				$result = [];
				foreach($data["Zasoba"] as $d){
					$result[$d["Code"]] = $d["Quantity"];
				}

				\Tovar::sklad_predajna_update($storeId, $result);

			}
		} catch (\Throwable $e){
			$response["code"] = 500;
			$response["message"] = 'Posted bad data.';
			$this->sendJsonResponse(["response" => $response]);
		}

		$this->sendJsonResponse(["storeId" => $storeId, "recievedData" => $result/*, "bearer" => $this->bearerToken*/]);
	}

	public function actionOrder() {

		$order = [
			"date" => (new DateTime())->format("c"),
			"orderName"=> "objednavka 202102001",
			"orderNumber" => 202102001,
			"storeId" => 22,
			"pickupDate" => (new DateTime())->modifyClone("+2 hour"),
			"clientName" => "Jakub Mikita",
			"clientEmail" => "kubomikita@gmail.com",
			"clientPhone" => '+421911531872',
			'items' => [
				[
					'code' => 1111,
					'name' => 'Goral master',
					'quantity' => 1,
					'priceUnit' => 10.99,
					'price' => 10.99
				],[
					'code' => 1112,
					'name' => 'Nestville Whiskey',
					'quantity' => 2,
					'priceUnit' => 7.50,
					'price' => 15.00
				]

			]

		];

		try {
			$socket = new \Kubomikita\Helpers\SocketClient( '87.197.109.40', 2005 );
			//$socket->setResponseType(\Kubomikita\Helpers\SocketClient::RESPONSE_JSON);
			$socket->setTimeout( 10 );
			$response = $socket->send( \Nette\Utils\Json::encode( $order ) );

			//dump( $response );
			$code = $order["response"]["code"] = 200;
			$order["response"]["message"] = $response;
			
		} catch (SocketException $e){
			$code = $order["response"]["code"] = 500;
			$order["response"] = array_merge($order["response"],$e->getSocketClient()->report()[0]);
		}

		$this->sendJsonResponse($order, $code);
		
	}
	/**
	 * @method POST
	 * @method GET
	 * @param int $storeId
	 */
	public function actionCuponDeactivate(string $cupon) {

		try {

			$k = \Kupon::findAll()->where(["kod" => $cupon, "aktivny" => 1])->fetch();
			if($k->id === null){
				throw new InvalidArgumentException("Cupon code '$cupon' not exists!");
			}
			$kupon = new \Kupon($k);
			$kupon->aktivny = 0;
			$kupon->save();
			//dumpe($kupon->id);

		} catch (\Throwable $e){
			$response["code"] = 500;
			$response["message"] = $e->getMessage();
			$this->sendJsonResponse(["response" => $response]);
		}

		$this->sendJsonResponse(["recievedData" => $cupon/*, "bearer" => $this->bearerToken*/]);
	}
}