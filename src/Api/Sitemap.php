<?php


namespace Kubomikita\Commerce\Api;


use Kubomikita\Commerce\ProteinConfigurator;
use Kubomikita\Commerce\Routing\Api\BaseApi;
use Kubomikita\Service;
use Nette\Database\Context;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;

class Sitemap extends BaseApi {
	/** @var Context */
	public $db;
	public $baseUrl;
	public $stylesheet = "main-sitemap.xsl";

	private $sections = ["Product","Category","Manufacturer","Page"]; //Article, ArticleCategory
	public $render = true;
	public $cache = false;

	public function beforeStartup() {
		if(!defined("WWW_DIR")){
			throw new \Exception("Please define constant WWW_DIR.");
		}
		$this->db = $this->context->getService("database");
		//dumpe($this->context->getByType(\Nette\Caching\Cache::class));
		$this->baseUrl = $this->context->getParameter("shop","url");
		if(!file_exists(WWW_DIR.$this->stylesheet)){
			$this->createStylesheet();
		}
	}
	private function renderXml($dom) {
		//dumpe($dom);
		if($dom instanceof \DOMDocument) {
			if ( $this->render ) {
				header( "Content-type: text/xml" );
				echo $dom->saveXML();
				exit;
			} else {
				return $dom->saveXML();
			}
		} elseif(is_string($dom)){
			if ( $this->render ) {
				header( "Content-type: text/xml" );
				echo $dom;
				exit;
			} else {
				return $dom;
			}
		}
	}
	private function checkTemp($section){
		if($this->render && $this->cache) {

			$sectionsmall = Strings::lower( $section );
			$filename     = TEMP_DIR . "export/sitemap_" . $sectionsmall . ".xml";
			if ( file_exists( $filename ) ) {
				$this->renderXml( file_get_contents( $filename ) );
				exit;
			}
		}
	}
	public function actionIndex(){
		$this->checkTemp("Index");
		$baseUrl = $this->baseUrl;
		$dom = new \DOMDocument("1.0","UTF-8");
		$dom->formatOutput = true;

		$stylesheet = new \DOMProcessingInstruction("xml-stylesheet",'type="text/xsl" href="'.$baseUrl.$this->stylesheet.'"');
		$dom->appendChild($stylesheet);

		$sitemapindex = $dom->createElement("sitemapindex");
		$sitemapindex->setAttribute("xmlns","http://www.sitemaps.org/schemas/sitemap/0.9");

		foreach($this->sections as $section){
			$fname = "is".$section;
			$exists = $this->{$fname}();
			if($exists) {
				$sitemap = $dom->createElement("sitemap");
				$sitemap->appendChild($dom->createElement("loc",$baseUrl."sitemap-".Strings::lower($section).".xml"));
				/** @var DateTime $date */
				$date = DateTime::from($exists->date);
				$sitemap->appendChild($dom->createElement("lastmod",$date->format("c")));
				$sitemapindex->appendChild($sitemap);
			}
		}



		$dom->appendChild($sitemapindex);
		return $this->renderXml($dom);
	}
	public function actionProducts(){
		return $this->actionProduct();
	}
	public function actionProduct(){
		$this->checkTemp("Product");
		$dom = new \DOMDocument("1.0","UTF-8");
		$dom->formatOutput = true;

		$stylesheet = new \DOMProcessingInstruction("xml-stylesheet",'type="text/xsl" href="'.$this->baseUrl.$this->stylesheet.'"');
		$dom->appendChild($stylesheet);
		$urlset = $dom->createElementNs("http://www.sitemaps.org/schemas/sitemap/0.9","urlset");
		$urlset->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
		$urlset->setAttributeNS('http://www.w3.org/2000/xmlns/',"xmlns:image","http://www.google.com/schemas/sitemap-image/1.1");
		$urlset->setAttributeNS("http://www.w3.org/2001/XMLSchema-instance","schemaLocation","http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");		                //->setAttributeNS("http://www.w3.org/1999/xhtml","xmlns:xhtml","http://www.w3.org/1999/xhtml");//->setAttribute("xmlns:xhtml","http://www.w3.org/1999/xhtml");
		foreach($this->getProduct() as $row) {
			$T = new \Tovar($row->tovar_id);
			if ( $T->id > 0 ) {

				$url = $dom->createElement("url");
				$url->appendChild($dom->createElement("loc",$this->baseUrl.$T->link().""));
				$date = DateTime::from($T->date_updated->val());
				$url->appendChild($dom->createElement("lastmod",$date->format("c")));
				$url->appendChild($dom->createElement("changefreq","monthly"));
				$url->appendChild($dom->createElement("priority","0.9"));
				$images = $T->images();
				if(!empty($images)) {
					foreach($images as $img) {
						$image = $dom->createElement( "image:image" );
						$image->appendChild( $dom->createElement( "image:loc", $this->baseUrl.$img->cachedUrlName() ) );
						$image->appendChild($dom->createElement("image:title", htmlspecialchars($T->nazov)));
						$image->appendChild($dom->createElement("image:caption", htmlspecialchars(strip_tags($T->intro))));
						$url->appendChild( $image );
					}
				}
				$urlset->appendChild($url);
			}
		}

		$dom->appendChild($urlset);
		return $this->renderXml($dom);
	}
	public function actionCategory(){
		$this->checkTemp("Category");
		$dom = new \DOMDocument("1.0","UTF-8");
		$dom->formatOutput = true;

		$stylesheet = new \DOMProcessingInstruction("xml-stylesheet",'type="text/xsl" href="'.$this->baseUrl.$this->stylesheet.'"');
		$dom->appendChild($stylesheet);
		$urlset = $dom->createElementNs("http://www.sitemaps.org/schemas/sitemap/0.9","urlset");
		$urlset->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
		$urlset->setAttributeNS('http://www.w3.org/2000/xmlns/',"xmlns:image","http://www.google.com/schemas/sitemap-image/1.1");
		$urlset->setAttributeNS("http://www.w3.org/2001/XMLSchema-instance","schemaLocation","http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");		                //->setAttributeNS("http://www.w3.org/1999/xhtml","xmlns:xhtml","http://www.w3.org/1999/xhtml");//->setAttribute("xmlns:xhtml","http://www.w3.org/1999/xhtml");
		$Root=new \Kategoria(1);
		foreach ($Root->fetchList() as $T) {
			if ( $T->id > 1 && $T->visible) {
				$url = $dom->createElement("url");
				$url->appendChild($dom->createElement("loc",$this->baseUrl.$T->link()));
				$date = DateTime::from($T->date_updated->val());
				$url->appendChild($dom->createElement("lastmod",$date->format("c")));
				$url->appendChild($dom->createElement("changefreq","weekly"));
				$url->appendChild($dom->createElement("priority","1"));
				$images = $T->images();
				if(!empty($images)) {
					foreach($images as $img) {
						$image = $dom->createElement( "image:image" );
						$image->appendChild( $dom->createElement( "image:loc", $this->baseUrl.$img->cachedUrl() ) );
						$image->appendChild($dom->createElement("image:title", htmlspecialchars($T->nazov)));
						//$image->appendChild($dom->createElement("image:caption", htmlspecialchars(strip_tags($T->popis))));
						$url->appendChild( $image );
					}
				}
				$urlset->appendChild($url);
			}
		}

		$dom->appendChild($urlset);
		return $this->renderXml($dom);
	}
	public function actionCategories(){
		return $this->actionCategory();
	}

	public function actionManufacturers(){
		return $this->actionManufacturer();
	}

	public function actionManufacturer(){
		$this->checkTemp("Manufacturer");
		$dom = new \DOMDocument("1.0","UTF-8");
		$dom->formatOutput = true;

		$stylesheet = new \DOMProcessingInstruction("xml-stylesheet",'type="text/xsl" href="'.$this->baseUrl.$this->stylesheet.'"');
		$dom->appendChild($stylesheet);
		$urlset = $dom->createElementNs("http://www.sitemaps.org/schemas/sitemap/0.9","urlset");
		$urlset->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
		$urlset->setAttributeNS('http://www.w3.org/2000/xmlns/',"xmlns:image","http://www.google.com/schemas/sitemap-image/1.1");
		$urlset->setAttributeNS("http://www.w3.org/2001/XMLSchema-instance","schemaLocation","http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");		                //->setAttributeNS("http://www.w3.org/1999/xhtml","xmlns:xhtml","http://www.w3.org/1999/xhtml");//->setAttribute("xmlns:xhtml","http://www.w3.org/1999/xhtml");
		$vyrobca = \TovarVyrobca::fetch();
		foreach($vyrobca as $T){
			if ( $T->id > 0 && $T->aktivny) {
				$url = $dom->createElement("url");
				$url->appendChild($dom->createElement("loc",$this->baseUrl.$T->link()));
				$date = DateTime::from($T->date_updated->val());
				$url->appendChild($dom->createElement("lastmod",$date->format("c")));
				$url->appendChild($dom->createElement("changefreq","weekly"));
				$url->appendChild($dom->createElement("priority","1"));
				$images = $T->images();
				if(!empty($images)) {
					foreach($images as $img) {
						$image = $dom->createElement( "image:image" );
						$image->appendChild( $dom->createElement( "image:loc", $this->baseUrl.$img->cachedUrl() ) );
						$image->appendChild($dom->createElement("image:title", htmlspecialchars($T->nazov)));
						//$image->appendChild($dom->createElement("image:caption", htmlspecialchars(strip_tags($T->popis))));
						$url->appendChild( $image );
					}
				}
				$urlset->appendChild($url);
			}
		}

		$dom->appendChild($urlset);
		return $this->renderXml($dom);


	}
	public function actionPage(){
		$this->checkTemp("Page");
		$dom = new \DOMDocument("1.0","UTF-8");
		$dom->formatOutput = true;

		$stylesheet = new \DOMProcessingInstruction("xml-stylesheet",'type="text/xsl" href="'.$this->baseUrl.$this->stylesheet.'"');
		$dom->appendChild($stylesheet);
		$urlset = $dom->createElementNs("http://www.sitemaps.org/schemas/sitemap/0.9","urlset");
		$urlset->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
		//$urlset->setAttributeNS('http://www.w3.org/2000/xmlns/',"xmlns:image","http://www.google.com/schemas/sitemap-image/1.1");
		$urlset->setAttributeNS("http://www.w3.org/2001/XMLSchema-instance","schemaLocation","http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd");		                //->setAttributeNS("http://www.w3.org/1999/xhtml","xmlns:xhtml","http://www.w3.org/1999/xhtml");//->setAttribute("xmlns:xhtml","http://www.w3.org/1999/xhtml");

		foreach ($this->getPage() as $T) {
			if ( $T->id > 0) {
				$url = $dom->createElement("url");
				$url->appendChild($dom->createElement("loc",$this->baseUrl.$T->url));
				//$date = DateTime::from($T->date_updated->val());
				//$url->appendChild($dom->createElement("lastmod",$date->format("c")));
				$url->appendChild($dom->createElement("changefreq","monthly"));
				$url->appendChild($dom->createElement("priority",$T->main?"1":"0.9"));
				$urlset->appendChild($url);
			}
		}

		$dom->appendChild($urlset);
		return $this->renderXml($dom);
	}
	private function isCategory(){
		return $this->db->query("SELECT date_updated as `date` FROM ec_kategorie ORDER BY date_updated DESC LIMIT 1")->fetch();
	}

	private function isManufacturer(){
		return $this->db->query("SELECT date_updated as `date` FROM ec_tovar_vyrobcovia ORDER BY date_updated DESC LIMIT 1")->fetch();
	}
	private function isPage(){
		$p = new \stdClass();
		$p->date = time();
		return $p;
	}
	private function getPage(){
		$qry = $this->db->query("SELECT *
FROM `cms_pages`
WHERE ((`url` != '' and (robots not like '%noindex%' or robots is null)) and system!=1)  or main=1 ORDER BY url ");
		return $qry;
	}
	private function getProduct(){
		//dumpe(\TovarSearchResult::getDefaultWhere());
		$Search = new \TovarSearchResult();
		$Search->indexable=0;
		$Search->aktivny=1;
		//$Search->limit = 10;

		return $Search->netteQuery();
	}
	private function isProduct(){
		return $this->db->query("SELECT datum_updated as `date` FROM ec_tovar ORDER BY datum_updated DESC LIMIT 1")->fetch();
	}
	private function createStylesheet(){
		$stylesheet = file_get_contents(__DIR__."/../assets/sitemap.xsl");
		file_put_contents(WWW_DIR.$this->stylesheet,$stylesheet);
	}
}