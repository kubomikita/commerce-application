<?php


namespace Kubomikita\Api;


use Kubomikita\Commerce\Routing\Api\BaseApi;

class Gls extends BaseApi {

	public function beforeStartup() {
		if($this->args["key"] != \BridgeConfig::gls_key){
			throw new \Exception("Access denied");
		}
	}
}