<?php
namespace Kubomikita\Commerce;


use Kubomikita\Image\ImgCacheHandler;
use Nette\Database\Connection;
use Nette\Http\IRequest;
use Nette\Http\Request;

class ImgCacheFactory {
	/** @var Connection */
	protected $db;
	/** @var Request */
	protected $request;
	/** @var array  */
	protected $options;
	public function __construct(Connection $db, IRequest $request, array $options = []) {
		$this->db = $db;
		$this->request = $request;
		$this->options = $options;
	}

	public function initialize() {
		if(isset($this->options["cache"]) && !$this->options["cache"]){
			$handler = new ImgCacheHandler(['id' => 0], $this->db);
			$handler->flushCache();
		}
	}

	public function create() : ImgCacheHandler {
		$handler = new ImgCacheHandler($this->request->getQuery(), $this->db);
		if(isset($this->options["cache"])){
			$handler->setSaveImage((bool) $this->options["cache"]);
		}
		if(isset($this->options["watermark"]["image"])){
			//dumpe($this);
			$watermark = $this->options["watermark"];
			$handler->addWatermark(
				$watermark["image"],
				isset($watermark["position"]) ? $watermark["position"] : ImgCacheHandler::WATERMARK_RIGHT_BOTTOM,
				isset($watermark["alpha"]) ? $watermark["alpha"] : 100,
				isset($watermark["margin"]) ? $watermark["margin"] : 90,
				isset($watermark["disableType"]) ? $watermark["disableType"] : []
			);
		}
		return $handler;
	}
}