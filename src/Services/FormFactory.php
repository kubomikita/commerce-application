<?php


namespace Kubomikita\Commerce;

use Kubomikita\DefaultFormTranslator;
use Kubomikita\FormAlertHandler;
use Nette,
	Kubomikita\FormAjaxHandler;

class FormFactory {
	public static function create(ConfiguratorInterface $container) : \Kubomikita\FormFactory{
		if(!$container->isCli()) {
			$formfactory = new \Kubomikita\FormFactory();
			$formfactory->setAjaxHandler( new FormAjaxHandler( $_GET, $_POST, $_FILES ) );
			$formfactory->setAlertHandler(new FormAlertHandler());
			$formfactory->setJavascriptFilename("assets/js/formfactory.min.js");
			$formfactory->setWithJquery(false);
			$formfactory->setTranslator(new DefaultFormTranslator());
			//$formfactory->register();

			return $formfactory;
		}
		return new \Kubomikita\FormFactory();
	}
}