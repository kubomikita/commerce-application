<?php


namespace Kubomikita\Commerce;

use Kubomikita\Commerce\Api\Auth;
use Nette;
use Nette\Security\User;

class UserService implements IService{
	/**
	 * @var User
	 */
	private $user;
	/**
	 * @var Nette\DI\Container
	 */
	private $context;

	const COOKIE_NAME = "kubomikita-autologin-[namespace]";
	const COOKIE_LIFETIME = "365 days";

	const NAMESPACE_SHOP = "front";
	const NAMESPACE_ADMIN = "admin";

	/**
	 * @deprecated
	 * 
	 * @return Nette\Security\User
	 */
	public function startup($configurator){
		$Authenticator = new \Commerce\FrontendAuthenticator();
		$Storage = new Nette\Http\UserStorage($configurator->container["session"]);
		$User = new Nette\Security\User($Storage,$Authenticator);
		$User->getStorage()->setNamespace(static::NAMESPACE_SHOP);

		if($User->isLoggedIn()){
			$Partner = new \Partner($User->getId());
			\Auth::$User = $Partner;
			\Auth::$auth=1;
			\Auth::$userId=\Auth::$User->id;
			\Auth::$userName=\Auth::$User->username;
		}
		$userService = new self($User);
		\Registry::set("user.service",$userService);

		return $User;
	}
	public function __construct( User $user = null, Nette\DI\Container $container= null) {
		$this->context = $container;
		if($user!==null){
			$this->user = $user;
		}
	}

	/**
	 * @param Nette\DI\Container $container
	 *
	 * @return Nette\Security\User
	 */
	public static function diUserStartup(Nette\DI\Container $container){
		$Authenticator = new \Commerce\FrontendAuthenticator($container->getByType(Nette\Database\Connection::class));
		$Storage = new Nette\Http\UserStorage($container->getByType(Nette\Http\Session::class));
		$User = new Nette\Security\User($Storage,$Authenticator);
		$User->getStorage()->setNamespace(static::NAMESPACE_SHOP);

		$service = new self($User,$container);
		$service->checkCookie();
		$service->initAuth();

		if(\Auth::$User->isVO()){
			$_SESSION["view_list"] = 1;
		} else {
			unset($_SESSION["view_list"]);
		}

		return $User;
	}
	public function initAuth($namespace = self::NAMESPACE_SHOP){
		if($namespace == static::NAMESPACE_SHOP){
			$this->user->getStorage()->setNamespace($namespace);
			if($this->user->isLoggedIn()){
				$Partner = new \Partner($this->user->getId());
				\Auth::$User = $Partner;
				\Auth::$auth=1;
				\Auth::$userId=\Auth::$User->id;
				\Auth::$userName=\Auth::$User->username;
				return true;
			} else {
				\Auth::$User = new \Partner();
				\Auth::$auth = 0;
			}
		}

		return false;
	}

	public function createCookie($namespace = self::NAMESPACE_SHOP, $partner_id = null){
		/** @var Nette\Http\Response $response */
		$response = $this->context->getByType(Nette\Http\Response::class);
		if($namespace==static::NAMESPACE_SHOP){
			$hash = Nette\Utils\Random::generate(60);
		} else {
			$hash = password_hash( Nette\Utils\Random::generate( 128 ), PASSWORD_DEFAULT );
		}
		$response->setCookie($this->getCookieName($namespace),$hash,static::COOKIE_LIFETIME);
		if((int) $partner_id > 0 && $namespace == static::NAMESPACE_SHOP) {
			\PartnerLoginModel::create($partner_id,$hash);
		}
		if((int) $partner_id > 0 && $namespace == static::NAMESPACE_ADMIN) {
			\UserLoginModel::create($partner_id,$hash);
		}
		return $hash;
	}

	public function checkCookie($namespace = self::NAMESPACE_SHOP){
		$this->user->getStorage()->setNamespace($namespace);
		/** @var Nette\Http\Response $request */
		$request = $this->context->getByType(Nette\Http\Request::class);
		/** @var Nette\Http\Response $response */
		$response = $this->context->getByType(Nette\Http\Response::class);
		$autologinhash = $request->getCookie($this->getCookieName($namespace));
		if(isset($autologinhash) && $autologinhash !== null && !$this->user->isLoggedIn()){
			$identity = $this->checkHash($autologinhash,$namespace);
			if($identity instanceof Nette\Security\Identity){
				$this->user->getStorage()->setIdentity($identity);
				$this->user->getStorage()->setAuthenticated(true);
				$this->notifyUser($namespace);
				//echo "authenticate me";
			}
		} else {
			/*if($namespace == self::NAMESPACE_ADMIN) {
				echo $this->createCookie( $namespace, 7 );
			}*/
		}
		//echo $this->createCookie($namespace,56289);
		//dump($autologinhash,$this->getCookieName($namespace),$this->user->isLoggedIn());
	}
	private function getCookieName($namespace){
		return Nette\Utils\Strings::replace(static::COOKIE_NAME,"*\[namespace\]*",$namespace);
	}
	public function eraseCookie($namespace = self::NAMESPACE_SHOP){
		/** @var Nette\Http\Response $response */
		$response = $this->context->getByType(Nette\Http\Response::class);
		/** @var Nette\Http\Request $request */
		$request = $this->context->getByType(Nette\Http\Request::class);
		$cookie = $request->getCookie($this->getCookieName($namespace));
		if($namespace == self::NAMESPACE_SHOP) {
			$p = \PartnerLoginModel::unactive( $cookie );
		} else {
			$p = \UserLoginModel::unactive($cookie);
		}
		$response->deleteCookie($this->getCookieName($namespace));
	}

	/**
	 * @param $hash
	 *
	 * @return null|Nette\Security\Identity
	 */
	private function checkHash($hash, $namespace = self::NAMESPACE_SHOP) {
		if($namespace == static::NAMESPACE_SHOP) {
			$l = \PartnerLoginModel::checkHash( $hash );
			if ((int) $l->id > 0 ) {
				$p = $l->partner;
				if ( $p->isLoginAllowed() ) {
					return new Nette\Security\Identity( $l->partner->id, null, (array) $l->partner );
				}
			}
		} elseif($namespace == static::NAMESPACE_ADMIN) {
			$l = \UserLoginModel::checkHash($hash);
			if((int) $l->id > 0 ){
				$p = $l->users;
				if($p->allow_login){
					return new Nette\Security\Identity($l->users->id,null, (array) $l->users);
				}
			}
		}
		return null;
	}

	private function notifyUser($namespace = self::NAMESPACE_SHOP){
		/*echo '<div class="alert alert-danger">Prebehlo automatické prihlásenie.</div>';
		$l = new \stdClass();
		$l->device = \Detect::deviceType();
		$l->device_os = \Detect::os();
		$l->device_brand = \Detect::brand();
		$l->browser = \Detect::browser();
		$l->ip = \Detect::ip();
		$l->host = \Detect::ipHostname();
		$l->country = \Detect::ipCountry();

		dump($l);*/
	}
}