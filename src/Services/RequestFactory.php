<?php

namespace Kubomikita\Commerce;


use Nette;
use Nette\Http\Request;
use Nette\DI\Container;
class RequestFactory {

	public static function create(Nette\Http\UrlScript $urlScript) : Request {
				//if(!(PHP_SAPI === 'cli')) {
			//$ip = ( isset( $_SERVER["HTTP_X_REAL_IP"] ) ) ? $_SERVER["HTTP_X_REAL_IP"] : $_SERVER["REMOTE_ADDR"];
			$requestFactory = new Nette\Http\RequestFactory();
			return $requestFactory->fromGlobals()->withUrl($urlScript);
		//}
		//return new Request(new Nette\Http\UrlScript());
	}
}