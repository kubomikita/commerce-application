<?php
namespace Kubomikita\Commerce;

use Kubomikita\Service;

final class RegistryProvider {
	private $registry;
	public function __construct() {
		$this->registry = Service::debug();
	}

	public static function getRegistry() : RegistryProvider {
		return new RegistryProvider();
	}
}