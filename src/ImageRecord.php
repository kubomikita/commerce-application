<?php

/*interface ImageRecord {
	CONST IMG_SCALE = 1;
	CONST IMG_FIT = 3;
	CONST IMG_CROP = 5;
	CONST IMG_MAX = 4;
}*/

class ImageRecord extends ActiveRecord{
	CONST IMG_SCALE = 1;
	CONST IMG_FIT = 3;
	CONST IMG_CROP = 5;
	CONST IMG_MAX = 4;

	CONST TYPE_JPEG = "jpg";
	CONST TYPE_WEBP = 'webp';

	protected static $table_parent;
	
	
	public function save($data=[]){
		$this->saveSortableTrait();
		parent::save($data);
		$this->saveImageTrait();
	}
	public function delete() {
		parent::delete();
		$this->deleteSortableTrait();
		$this->deleteImageTrait();
	}

	public function __toString() {
		$id = $this->id;
		$id .= isset($this->tovar) ? $this->tovar : '';
		$id .= isset($this->darcek) ? $this->darcek : '';
		$id .= isset($this->variant) ? serialize($this->variant) : '';
		return $id;
	}
}