<?php

namespace Kubomikita\Commerce;



use Nette\DI\Container;
use Tracy\ILogger;
use Tracy\Logger;
use Nette\Utils\Strings;
use Sentry\Integration\RequestIntegration;
use Sentry\Severity;
use Sentry\State\Scope;
use Throwable;
use Tracy\Debugger;
use Tracy\Dumper;

use function Sentry\captureException;
use function Sentry\captureMessage;
use function Sentry\configureScope;
use function Sentry\init;

class SentryLogger extends Logger {

	public function __construct(Container $container) {
		init( [
			'dsn'                  => $container->parameters["sentry"]["dsn"],
			'environment'          => $container->parameters["sentry"]["environment"],
			'attach_stacktrace'    => true,
			'default_integrations' => false,
			'integrations'         => [
				new RequestIntegration(),
			],
		] );

		$this->email     = &Debugger::$email;
		$this->directory = Debugger::$logDirectory;
	}


	public function log( $value, $level = self::INFO ) {
		$response = parent::log( $value, $level );
		$severity = $this->tracyPriorityToSentrySeverity( $level );
		bdump($value, $level);
		// if we still don't have severity, don't log anything
		if ( ! $severity ) {
			return $response;
		}
		bdump( $response, "Response" );
		bdump( $severity, "severity" );

		$message = ( $value instanceof \Throwable ) ? $value->getMessage() : $value;
		//bdump($message);
		//bdump($value);
		if ( Strings::contains( $message, "Notice" ) || Strings::contains($message, 'Warning: fopen') ) {
			return $response;
		}

		//dumpe($severity);

		configureScope( function ( Scope $scope ) use ( $severity, $response ) {
			if ( ! $severity ) {
				return;
			}
			$scope->setLevel( $severity );
			$scope->setExtra( "blueScreen", $response );
			//$scope->addEventProcessor( $this->userContextProcessor );
			//$scope->addEventProcessor( $this->sessionContextProcessor );


		} );


		if ( $value instanceof Throwable ) {
			captureException( $value );
		} else {
			captureMessage( is_string( $value ) ? $value : Dumper::toText( $value ) );
		}

		return $response;
	}

	private function tracyPriorityToSentrySeverity( string $priority ): ?Severity {
		switch ( $priority ) {
			case ILogger::DEBUG:
				return Severity::debug();
			case ILogger::INFO:
				return Severity::info();
			case ILogger::WARNING:
				return Severity::warning();
			case ILogger::ERROR:
			case ILogger::EXCEPTION:
				return Severity::error();
			case ILogger::CRITICAL:
				return Severity::fatal();
			default:
				return null;
		}
	}
}
