<?php


namespace Kubomikita\Commerce;

use Nette;

class SessionService implements IService{

	public function startup($configurator){
		$session = new Nette\Http\Session($configurator->container["request"],$configurator->container["response"]);
		$session->setExpiration(608400);
		$session->setOptions(array("auto_start"=>true));
		return $session;
	}

	/**
	 * @param Nette\DI\Container $container
	 *
	 * @return Nette\Http\Session
	 */
	public static function diStartup(Nette\DI\Container $container){
		$session = new Nette\Http\Session($container->getByType(Nette\Http\Request::class),$container->getByType(Nette\Http\Response::class));
		$session->setExpiration(608400);
		//$session->setName("proteinsk");
		//bdump($session,"SESSION START");
		$session->start();
		//$session->setOptions(array("auto_start"=>true));


		return $session;
	}
}