<?php
namespace Kubomikita\Commerce;

use Nette\Application\AbortException;
use Nette\DI\Container;
use Nette\Http\Request;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Mail\IMailer;
use Nette\Mail\Mailer;

class MailService implements IService{

	const SESSION_SECTION = "MailMessages";

	/** @var IMailer */
	public $mailer;
	/** @var ProteinConfigurator */
	public $context;
	/** @var SessionSection */
	public $session;
	public $options = [];
	/** @var Nette\Mail\Message[]  */
	//public $messages = [];

	/**
	 * @param Configurator $configurator
	 */
	public function startup( $configurator ) {
		$this->context = $configurator;
		/** @var Session $session */
		$session =$this->context->getService("session");
		$this->session = $session->getSection("MailMessages");
		$this->session->setExpiration(0);
		$config = $configurator->getParameter("mail");

		if(!is_array($this->session->messages)){
			$this->session->messages = [];
		}

		if(isset($config["mailer"]) and $config["mailer"] instanceof Mailer){
			$class = $config["mailer"];
			unset($config["mailer"]);
			$this->options = $options =  $config;
			$this->mailer = $config["mailer"];
		} else {
			echo "Mailer is not defined.";
			throw new AbortException("mailer is not defined");
		}

		return $this;
	}
	public function __construct(Session $session, Mailer $mailer) {
		//if($container!==null) {
			//$session       = $container->getService( "session" );
			//$configurator  = $container->getService( "configurator" );
			$this->mailer = $mailer;
			$this->session = $session->getSection( self::SESSION_SECTION );
			$this->session->setExpiration( 0 );
			//$config = $configurator->getParameter( "mail" );
			if ( ! is_array( $this->session->messages ) ) {
				$this->session->messages = [];
			}

		//}
	}

	public function setMail(array $mail){
		$this->session->messages[] = $mail;
	}
	public function getMails(){
		return $this->session->messages;
	}
	public function flushMails(){
		$this->session->messages = [];
	}
}