<?php


namespace Kubomikita\Commerce;

use \Gettext\Translator;
use \Gettext\Translations;
use \Gettext\GettextTranslator;
use Nette\Utils\Strings;
use PHPMailer\PHPMailer\Exception;

class GettextService implements IService{
	private static $langName = ["sk"=>"sk_SK","cz"=>"cs_CZ"];
	/** @var Translations */
	private static $translations;
	private function initLanguage($configurator){
		if($configurator->isHost("protein.cz")){
			$_SESSION["lang"]="cz";
			$_SESSION["currency"] = "CZK";
			\LangStr::$document_locale = "cs";
		} else {
			$_SESSION["lang"]="sk";
			unset($_SESSION["currency"]);
			\LangStr::$document_locale = "sk";
			//$_SESSION["currency"] = "CZK";
		}

		//$_SESSION["lang"] = "cz";
		\LangStr::$locale=$_SESSION["lang"];
	}

	public function startup($configurator){

		$this->initLanguage($configurator);

		$locale = self::$langName[\LangStr::$locale];
		putenv("LC_ALL=".$locale);
		setlocale(LC_ALL, $locale);

		$domain = "protein";

		$t = new Translator();
		$t->defaultDomain($domain);

		$pofile = __DIR__.'/../../languages/'.$locale.'/LC_MESSAGES/'.$domain.'.po';
		$translations = new Translations();
		if(file_exists($pofile)) {
			try {
				$translations->addFromPoFile( $pofile );
				$t->loadTranslations( $translations );
			} catch ( \Exception $e ) {
				trigger_error( $e->getMessage(), E_USER_NOTICE );
			}
		}
		\Registry::set("translator",$t);
		\Registry::set("translations",$translations);
	}

	/**
	 * @param ConfiguratorInterface $configurator
	 *
	 * @return Translator
	 */
	public static function diStartup(ConfiguratorInterface $configurator){
		if($configurator->isProtein()) {
			if ( $configurator->isHost( "protein.cz" ) ) {
				$_SESSION["lang"]     = "cz";
				$_SESSION["currency"] = "CZK";
				\LangStr::$document_locale = "cs";
			} else {
				$_SESSION["lang"] = "sk";
				unset( $_SESSION["currency"] );
				\LangStr::$document_locale = "sk";
			}
		} else {
			$_SESSION["lang"] = "sk";
			unset( $_SESSION["currency"] );
		}

		\LangStr::$locale = $_SESSION["lang"];

		$locale = self::$langName[\LangStr::$locale];
		putenv("LC_ALL=".$locale);
		setlocale(LC_ALL, $locale);

		$domain = Strings::webalize($configurator->getParameter("shop","name"));

		$t = new Translator();
		$t->defaultDomain($domain);

		$pofile = APP_DIR.'languages/'.$locale.'/LC_MESSAGES/'.$domain.'.po';
		$translations = new Translations();
		if(file_exists($pofile)) {
			try {
				$translations->addFromPoFile( $pofile );
				$t->loadTranslations( $translations );
			} catch ( \Exception $e ) {
				trigger_error( $e->getMessage(), E_USER_NOTICE );
			}
		}
		self::$translations = $translations;
		return $t;
	}

	/**
	 * @return Translations
	 */
	public static function diStartupTranslations(){
		return self::$translations;
	}
}

