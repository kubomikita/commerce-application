<?php


namespace Kubomikita\Commerce;

use Nette;
use Nette\Http\UrlScript;

class UrlFactory {
	public static function create(/*string $shopUrl*/) : UrlScript {
		if(PHP_SAPI === 'cli') {
			return new UrlScript();
		}
		$url = new UrlScript(self::getRequestScheme()."://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]);
		return $url->withQuery($_REQUEST);
	}

	private static function getRequestScheme(){
		if(isset($_SERVER['HTTP_X_FORWARDED_PROTO'])){
			return $_SERVER['HTTP_X_FORWARDED_PROTO'];
		} elseif(
			(isset($_SERVER['REDIRECT_HTTPS']) && $_SERVER["REDIRECT_HTTPS"] == "on") ||
			(isset($_SERVER['REQUEST_SCHEME']) && $_SERVER["REQUEST_SCHEME"] == "https") ||
			(isset($_SERVER['HTTP_HTTPS']) && $_SERVER["HTTP_HTTPS"] == "on") ||
			(isset($_SERVER['HTTPS']) && $_SERVER["HTTPS"] == "on")
		) {
			return "https";
		}
		return "http";
	}
}