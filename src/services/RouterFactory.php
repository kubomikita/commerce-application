<?php

namespace Kubomikita\Commerce;

use Kubomikita\Form;
use Kubomikita\FormFactory;
use Kubomikita\IFromAjaxHandler;
use Nette\DI\Container;
use Nette\Http\Request;
use Nette\Utils\Callback;

class RouterFactory {
	/**
	 * @var Container
	 */
	private $container;
	/**
	 * @var ConfiguratorInterface
	 */
	private $conf;
	/**
	 * @var array
	 */
	private $initialized = [];
	private $routers = [];

	/**
	 * RouterFactory constructor.
	 *
	 * @param ConfiguratorInterface $container
	 */
	public function __construct(\Nette\DI\Container $container) {
		//$this->conf = $container;
		//dumpe($container,$container->getByType(ConfiguratorInterface::class));
		$this->container = $container;
		$this->conf = $container->getByType(ConfiguratorInterface::class);
		$this->routers = $this->conf->getRouters();

	}
	public function getByType($class){
		$callback = [$this,array_search($class,$this->routers)];
		return $callback($class);
	}
	public function getRouter($class){
		$this->initialized[] = $class;
		$callback = [$class, "create"];
		return $callback($this->container->getByType(Container::class));
	}
	public function api($class){
		$conf = $this->conf;
		$request = $this->container->getByType(Request::class);
		if(!$conf->isCli()) {
			if ( $request->getQuery( 'api' ) ) {
				return $this->getRouter($class);
			}
		}
	}
	public function ajax($class){
		$request = $this->container->getByType(Request::class);
		if(!$this->conf->isCli()) {
			if ( $request->isAjax() ) {
				/*$formFactory = $this->container->getByType(FormFactory::class);
				if($formFactory->getAjaxHandler() instanceof IFromAjaxHandler){
					dump($this,Form::$instances);
					exit;
				}*/
				$this->getRouter($class);
			}
		}
	}
	public function cli($class){
		$conf = $this->conf;
		if ( $conf->isCli() ) {
			return $this->getRouter($class);
		}
	}
	public function redirects($class){
		if(!$this->conf->isCli()){
			return $this->getRouter($class);
		}
	}

}