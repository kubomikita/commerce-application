<?php


namespace Kubomikita\Commerce;

use Kubomikita\Latte\Macro\CommerceMacro;
use Kubomikita\Latte\Macro\FormFactoryMacro;
use Latte\Engine,
	Latte\Macros\MacroSet;

class LatteService implements IService{

	/**
	 * @param $configurator ProteinConfigurator
	 *
	 * @return Engine
	 */
	public function startup($configurator){
		$cacheDir = $configurator->getCacheDirectory();

		$latte = new Engine;

		FormFactoryMacro::install($latte->getCompiler());
		CommerceMacro::install($latte->getCompiler());

		if($configurator->isProduction()) {
			$latte->setTempDirectory($cacheDir);
		}
		$latte->addFilter("translate",function($string){
			return __($string);
		});
		return $latte;
	}
	/**
	 * @param $configurator ProteinConfigurator
	 *
	 * @return Engine
	 */
	public static function diStartup(ConfiguratorInterface $configurator){
		$cacheDir = $configurator->getCacheDirectory();
		$latte = new Engine;

		FormFactoryMacro::install($latte->getCompiler());
		CommerceMacro::install($latte->getCompiler());

		if($configurator->isProduction()) {
			$latte->setTempDirectory($cacheDir);
		//	$latte->setAutoRefresh();
		}
		$latte->addFilter("translate",function($string){
			return __($string);
		});
		$latte->addFilter("currency",function($string){
			return \Format::money_user($string);
		});
		/**
		 * BC break - latte v2.5.1
		 */
		$latte->addFilter("replace",function ($subject,$search,$replacement = ''){
			return str_replace($search,$replacement,(string) $subject);
		});

		return $latte;
	}

}