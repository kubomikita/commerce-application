<?php


namespace Kubomikita\Commerce;


class MicrodataService implements IService{

	public function startup($configurator){
		$microdata = new Microdata($configurator);
		return $microdata;
	}

	/**
	 * @param ConfiguratorInterface $configurator
	 *
	 * @return Microdata
	 */
	public static function diStartup(ConfiguratorInterface $configurator){
		$microdata = new Microdata($configurator);
		return $microdata;
	}
}