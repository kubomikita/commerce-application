<?php


namespace Kubomikita\Commerce;

use Nette\Database;
use Nette\Caching;
use Nette\DI\Container;

class DatabaseFactory  {
	
	public static function createConnection(array $options) : Database\Connection
	{
		if($options["socket"] != ""){
			$dsn = "mysql:unix_socket=".$options["socket"].";dbname=".$options["name"];
		} else {
			$dsn = "mysql:host=".$options["host"].";dbname=".$options["name"];
		}
		$database = new Database\Connection($dsn,$options["user"],$options["password"], ["charset" => "utf8"]);

		\CommerceDB::connect($options);

		return $database;
	}

	public static function createContext(Database\Connection $connection, Caching\IStorage $storage) : Database\Context
	{
		$db_structure = new Database\Structure($connection,$storage);
		$db_conventions = new Database\Conventions\DiscoveredConventions($db_structure);
		return new Database\Context($connection,$db_structure,$db_conventions,$storage);
	}

	public static function disconnect(Database\Connection $connection){
		$connection->disconnect();
		mysqli_close(\CommerceDB::$DB);
		//bdump($connection,"DATABASE disconnect");
	}
}