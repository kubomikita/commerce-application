<?php


namespace Kubomikita\Commerce;

use Nette;
class ResponseService implements IService{
	/**
	 * @param $configurator
	 *
	 * @return Nette\Http\Response
	 */
	public function startup($configurator){
		return new Nette\Http\Response();
	}
	/**
	 *
	 * @return Nette\Http\Response
	 */
	public static function diStartup(){
		return new Nette\Http\Response();
	}
}