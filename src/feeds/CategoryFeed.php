<?php


namespace Commerce\Xml;


use Kubomikita\Commerce\ProteinConfigurator;
use Kubomikita\Service;

class Category extends XmlFeed {
	/** @var ProteinConfigurator */
	private $context;

	public function getXml(array $query = []) {
		$this->context = Service::get("container");

		$domtree = new \DOMDocument("1.0","UTF-8");
		$domtree->preserveWhiteSpace = false;
		$domtree->formatOutput = true;

		$xmlRoot = $domtree->createElement("categories");
		$xmlRoot = $domtree->appendChild($xmlRoot);



		$kategorie = \Kategoria::fetchList();
		$i = 0;
		/** @var \Kategoria $k */
		foreach($kategorie as $k) {
			if ( $k->visible && $k->level() > 0 && $k->countActiveItems() > 0) {
				//dump( $k );

				$y = 1;
				$name = [];
				foreach ( $k->fetchPath() as $p ){
					if( $p->level() > 0) {
						$name[] = htmlspecialchars((string) $p->nazov);
						$y ++;
					}
				}
				$Search = new \TovarSearchResult();$i=0;

				$Search->aktivny = 1;
				$Search->indexable = 1;
				$Search->visible = 1;
				$Search->kategoria = $k->id;
				$Search->order_by = "vyrobca_id";

				$q = $Search->netteQuery("MIN(t.cena) as mincena, MAX(t.cena) as maxcena, COUNT(t.tovar_id) as pocet");
				$r = $q->fetch();
				//dump($name,$r);
				$shopitem = $domtree->createElement( "category" );
				$shopitem = $xmlRoot->appendChild( $shopitem );
				$shopitem->appendChild( $domtree->createElement( 'id', htmlspecialchars( $k->id ) ) );
				$shopitem->appendChild( $domtree->createElement( 'category_url',
					$this->context->getParameter( "shop", "url" ) . $k->link() ) );
				$shopitem->appendChild($domtree->createElement("category_name", implode(" / ",$name)));

				$item = $domtree->createElement( "brands" );
				foreach($Search->netteQuery("DISTINCT(t.vyrobca_id)") as $vid){
					$V = new \TovarVyrobca($vid["vyrobca_id"]);
					if((int) $V->id > 0){
						$item->appendChild($domtree->createElement("brand",htmlspecialchars((string) $V->nazov)));
					}
				}
				$shopitem->appendChild($item);
				$shopitem->appendChild($domtree->createElement("min_price",strtr( sprintf( "%.2F", $r->mincena ), array( '.' => ',' ) )));
				$shopitem->appendChild($domtree->createElement("max_price",strtr( sprintf( "%.2F", $r->maxcena ), array( '.' => ',' ) )));
				$shopitem->appendChild($domtree->createElement("products_count",(int)$r->pocet));
				$shopitem->appendChild($domtree->createElement("min_sale",(int)0));
				$shopitem->appendChild($domtree->createElement("max_sale",(int)0));

				$i++;
				if($i == 4) {
					//break;
				}
			}
		}
		$xml = $domtree->saveXML();

		return $xml;
	}
}