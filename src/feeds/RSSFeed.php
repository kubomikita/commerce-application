<?php
namespace Commerce\Xml;


use Kubomikita\Commerce\ConfiguratorInterface;
use Kubomikita\Service;

class Rss extends XmlFeed {
	/** @var ConfiguratorInterface */
	private $context;

	protected $debug = true;

	public function getXml(array $query = []){
		$this->context = Service::get("container");
		$lang = $lang_code = "sk";
		if($this->context->isProtein()) {
			if($this->context->isHost("protein.cz")) {
				$lang = "cz";
				$lang_code = "cs";
			}
		}

		$xml = new \DOMDocument("1.0", "UTF-8"); // Create new DOM document.
		$xml->formatOutput = true;
//create "RSS" element
		$rss = $xml->createElement("rss");
		$rss_node = $xml->appendChild($rss); //add RSS element to XML node
		$rss_node->setAttribute("version","2.0"); //set RSS version

//set attributes
		$rss_node->setAttribute("xmlns:dc","http://purl.org/dc/elements/1.1/"); //xmlns:dc (info http://j.mp/1mHIl8e )
		$rss_node->setAttribute("xmlns:content","http://purl.org/rss/1.0/modules/content/"); //xmlns:content (info http://j.mp/1og3n2W)
		$rss_node->setAttribute("xmlns:atom","http://www.w3.org/2005/Atom");//xmlns:atom (http://j.mp/1tErCYX )

//Create RFC822 Date format to comply with RFC822
		$date_f = date("D, d M Y H:i:s T", time());
		$build_date = gmdate(DATE_RFC2822, strtotime($date_f));

//create "channel" element under "RSS" element
		$channel = $xml->createElement("channel");
		$channel_node = $rss_node->appendChild($channel);

//a feed should contain an atom:link element (info http://j.mp/1nuzqeC)
		$channel_atom_link = $xml->createElement("atom:link");
		$channel_atom_link->setAttribute("href",$this->context->getParameter("shop","url")."feed/rss.xml"); //url of the feed
		$channel_atom_link->setAttribute("rel","self");
		$channel_atom_link->setAttribute("type","application/rss+xml");
		$channel_node->appendChild($channel_atom_link);

//add general elements under "channel" node
		$channel_node->appendChild($xml->createElement("title", $this->context->getParameter("rss","title"))); //title
		$channel_node->appendChild($xml->createElement("description", $this->context->getParameter("rss","desc")));  //description
		$channel_node->appendChild($xml->createElement("link", $this->context->getParameter("shop","url"))); //website link
		$channel_node->appendChild($xml->createElement("language", $lang_code));  //language
		$channel_node->appendChild($xml->createElement("lastBuildDate", $build_date));  //last build date
		$channel_node->appendChild($xml->createElement("generator", $this->context->getParameter("rss","generator"))); //generator

		$db = $this->context->getService("database");
		$ids = [];
		$langqry = $db->query( "SELECT params FROM `cms_pages_data` WHERE `object` = 'eshop_news_list' and box = 'variable-main' and lang = ? ORDER BY poradie", $lang );
		foreach($langqry as $r){
			$params = unserialize($r->params);
			$ids[]=(int)$params["id"];
		}
		if(!empty($ids)) {
			//$qry = mysqli_query(echelon_db_connect(),"select nl.nazov as kategoria, nl.id as kategoria_id ,nd.*,nl.popis,nl.extra_title from protein_news_list as nl LEFT JOIN protein_news_data as nd ON nd.container = nl.id WHERE nd.visible=1  ORDER BY nd.date_created DESC");
			$q = $db->query( "select 
						nd.id,nd.nazov,nd.seo_name,nd.content,nd.intro,nd.date_created 
						from cms_news_data as nd WHERE nd.visible=1 and nd.container IN (?) ORDER BY nd.date_created DESC",
				$ids );
			foreach ( $q as $r ) {
				$seolink = \Format::seoName( $r['nazov'] ) . '-' . $r['id'] . '-clanok';
				//$imglink='img.php?class=news&id='.$Ri['id'].'';
				$imglink = 'n-' . $r['id'] . '.jpg';
				$imglink = image_url( "news", $r->id );

				$item_node = $channel_node->appendChild( $xml->createElement( "item" ) ); //create a new node called "item"
				//$item_node->appendChild($xml->createElement("guid",$r->id));
				$title_node = $item_node->appendChild( $xml->createElement( "title",
					$r->nazov ) ); //Add Title under "item"
				$link_node  = $item_node->appendChild( $xml->createElement( "link",
					$this->context->getParameter( "shop", "url" ) . $seolink ) ); //add link node under "item"

				//Unique identifier for the item (GUID)
				$guid_link = $xml->createElement( "guid", $this->context->getParameter( "shop", "url" ) . $seolink );
				$guid_link->setAttribute( "isPermaLink", "false" );
				$guid_node = $item_node->appendChild( $guid_link );

				//create "description" node under "item"
				$description_node = $item_node->appendChild( $xml->createElement( "description" ) );

				//fill description node with CDATA content
				$description_contents = $xml->createCDATASection( str_replace("&nbsp;"," ", \Format::shortText( $r['content'],
					300 ) ) );
				$description_node->appendChild( $description_contents );

				//Published date
				$date_rfc      = gmdate( DATE_RFC2822, ( $r->date_created ) );
				$pub_date      = $xml->createElement( "pubDate", $date_rfc );
				$pub_date_node = $item_node->appendChild( $pub_date );

				$item_node->appendChild( $xml->createElement( "image",
					$this->context->getParameter( "shop", "url" ) . $imglink ) );
				/*echo '';
				echo('<item>');
				echo('<title><![CDATA['.$r['nazov'].']]></title>');
				echo('<link>https://www.protein.sk/'.$seolink.'</link>');
				echo('<description><![CDATA['.Format::shortText($r['content'],300).']]></description>');
				echo('<pubDate>'.date(DATE_RSS,$r['date_created']).'</pubDate>');
				//echo('<guid>https://www.protein.sk/'.$seolink.'</guid>');
				//echo('<enclosure url="'.strtr($SYSTEM['web_url'].'/'.$imglink,array('&'=>'&amp;')).'" length="'.$Ri['imglen'].'" type="image/jpeg" />');
				echo('</item>');*/
			}
		}

		//$xmlRoot = $domtree->createElementNS("http://www.w3.org/2005/Atom","feed");
		//$xmlRoot->setAttributeNS('http://www.w3.org/2000/xmlns/' ,'xmlns:g', 'http://base.google.com/ns/1.0');
		//$xmlItem = $domtree->appendChild($xmlRoot);

		//$item = $domtree->createElement("title", $this->context->getParameter("shop","name"));
		//$xmlItem->appendChild($item);
		return $xml->saveXML();


		//$xml = $domtree->saveXML();
		//return $xml;
	}
}

/*


echo('<'.'?xml version="1.0" encoding="utf-8"?'.'>');
echo('<rss version="2.0" >');
echo('<channel>');
echo('<title>Protein.sk - blog</title>');
echo('<description>Prinášame Vám blog o najnovších trendoch z oblasti tréningu, stravovania, doplnkoch výživy, zdraví a kráse. Poradíme Vám ako účinne schudnúť, ako sa zdravo a rozumne stravovať, nájdete tu rôzne tréningové plány a vhodné doplnky výživy pri cvičení..</description>');
echo('<link>https://www.protein.sk</link>');
echo('<lastBuildDate>'.date(DATE_RSS).'</lastBuildDate>');
echo('<generator>STEGA TRADE s.r.o.</generator>');
$qry = mysqli_query(echelon_db_connect(),"select nl.nazov as kategoria, nl.id as kategoria_id ,nd.*,nl.popis,nl.extra_title from protein_news_list as nl LEFT JOIN protein_news_data as nd ON nd.container = nl.id WHERE nd.visible=1  ORDER BY nd.date_created DESC");
while($r=mysqli_fetch_assoc($qry)){
	$seolink='clanok/'.$r['id'].'-'.Format::seoName($r['nazov']);
	//$imglink='img.php?class=news&id='.$Ri['id'].'';
	$imglink='n-'.$r['id'].'.jpg';
	echo '';
	echo('<item>');
	echo('<title><![CDATA['.$r['nazov'].']]></title>');
	echo('<link>https://www.protein.sk/'.$seolink.'</link>');
	echo('<description><![CDATA['.Format::shortText($r['content'],300).']]></description>');
	echo('<pubDate>'.date(DATE_RSS,$r['date_created']).'</pubDate>');
	//echo('<guid>https://www.protein.sk/'.$seolink.'</guid>');
	//echo('<enclosure url="'.strtr($SYSTEM['web_url'].'/'.$imglink,array('&'=>'&amp;')).'" length="'.$Ri['imglen'].'" type="image/jpeg" />');
	echo('</item>');
}
echo('</channel>');
echo('</rss>');
/*$feedId=esc($_REQUEST['feed']);
$Q=mysqli_query(CommerceDB::$DB,"select * from ${SYSTEM['db_prefix']}_objects where typ='rssx' and ident='${feedId}'");
if($Q && mysqli_num_rows($Q)){
  $R=mysqli_fetch_assoc($Q);

  $params=unserialize(stripslashes($R['xdata']));
  $query="select ".$MOD['news']->dataColumns.",length(img) as imglen from ".$SYSTEM['db_prefix']."_news_data where visible=1";
  if($params['id']!=''){ $query.=" and container=".$params['id']." "; };
  if($params['is_top']!=''){ $query.=" and is_top=1 "; };
  if($params['tag']!=''){ $query.=" and tags like '%[".$params['tag']."]%' "; };
  if($params['max_days_past']!=''){ $query.=" and date_event>=".(time()-($params['max_days_past']*86400))." "; };
  if($params['max_days_future']!=''){ $query.=" and date_event<=".(time()+($params['max_days_future']*86400))." "; };

  if($params['sort']!=''){ $query.=" order by ".$params['sort'].""; } else { $query.=" order by poradie asc"; };

  if($params['pagesize']!=''){ $query.=" limit ".$params['pagesize']." "; };

  //var_dump($query);

  echo('<'.'?xml version="1.0" encoding="utf-8"?'.'>');
  echo('<rss version="2.0" >');
  echo('<channel>');
  echo('<title>'.$R['nazov'].'</title>');
  echo('<description>'.$R['popis'].'</description>');
  echo('<link>'.$SYSTEM['web_url'].'</link>');
  echo('<lastBuildDate>'.date(DATE_RSS).'</lastBuildDate>');
  echo('<generator>ECHELON CMS 2.1</generator>');

  $Qi=mysqli_query(CommerceDB::$DB,$query);
  while($Ri=mysqli_fetch_assoc($Qi)){

    $seolink='--'.$params['render'].'-'.$Ri['id'].'-'.seo_transcode($Ri['nazov']);
    //$imglink='img.php?class=news&id='.$Ri['id'].'';
    $imglink='n-'.$Ri['id'].'.jpg';
    echo('<item>');
    echo('<title><![CDATA['.$Ri['nazov'].']]></title>');
    echo('<link>'.$SYSTEM['web_url'].'/'.$seolink.'</link>');
    echo('<description><![CDATA['.$Ri['intro'].']]></description>');
    echo('<pubDate>'.date(DATE_RSS,$Ri['date_created']).'</pubDate>');
    echo('<guid>'.$SYSTEM['web_url'].'/'.$seolink.'</guid>');
    if($params['include_images']){ echo('<enclosure url="'.strtr($SYSTEM['web_url'].'/'.$imglink,array('&'=>'&amp;')).'" length="'.$Ri['imglen'].'" type="image/jpeg" />'); };
    echo('</item>');

  };

  echo('</channel>');
  echo('</rss>');


};
*/