<?php


namespace Commerce\Xml;


use Kubomikita\Commerce\ProteinConfigurator;
use Kubomikita\Service;

class Products extends XmlFeed {
	/** @var ProteinConfigurator */
	private $context;

	public function getXml(array $query = []) {
		$this->context = Service::get("container");

		$domtree = new \DOMDocument("1.0","UTF-8");
		$domtree->preserveWhiteSpace = false;
		$domtree->formatOutput = true;

		$xmlRoot = $domtree->createElement("products");
		$xmlRoot = $domtree->appendChild($xmlRoot);

		$Search = new \TovarSearchResult();$i=0;

		$Search->aktivny = 1;
		$Search->indexable = 1;
		$Search->visible = 1;

		foreach($Search->netteQuery() as $row){
			$T = new \Tovar($row->tovar_id);
			$kcena = (float) $T->konkurencna_cena();
			$cena = $T->predajna_cena_user()->suma();
			$shopitem = $domtree->createElement( "product" );
			$shopitem = $xmlRoot->appendChild( $shopitem );
			$shopitem->appendChild( $domtree->createElement( 'id', htmlspecialchars( $T->id ) ) );
			$shopitem->appendChild( $domtree->createElement( 'name', htmlspecialchars( $T->nazov ) ) );
			$shopitem->appendChild( $domtree->createElement( 'description', htmlspecialchars( strip_tags($T->intro) ) ) );
			$shopitem->appendChild( $domtree->createElement( 'url',
				$this->context->getParameter( "shop", "url" ) . $T->link() ) );
			$shopitem->appendChild( $domtree->createElement( 'price',
				strtr( sprintf( "%.2F", $cena ), array( '.' => ',' ) ) ) );
			$shopitem->appendChild( $domtree->createElement( 'brand',
				htmlspecialchars( $T->vyrobca->nazov ) ) );
			$kategorie = [];
			foreach ( $T->primarna_kategoria->fetchPath() as $Kp ) {
				if ( $Kp->id != 1 ) {
					$kategorie[] = (string) $Kp->nazov;
				};
			};
			$category_text = htmlspecialchars( join( ' | ', $kategorie ) );
			$shopitem->appendChild( $domtree->createElement( 'category', $category_text ) );

			$sale = 0;
			if($kcena > $cena){
				$sale = round((1 - ($cena / $kcena)) * 100,0);
			}
			$shopitem->appendChild($domtree->createElement("sale", $sale));
			$shopitem->appendChild($domtree->createElement("stock",$T->sklad_available()));

		}
		$xml = $domtree->saveXML();

		return $xml;
	}
}