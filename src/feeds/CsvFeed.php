<?php

namespace Commerce\Csv;
use Nette\Utils\Strings;

abstract class CsvFeed {

	public $feedDir, $feedName, $feedFile;


	public function __construct()
	{
		//$this->xmlDocument = new \DOMDocument('1.0', 'utf-8');
		//$this->xmlDocument->formatOutput = true;
		$this->feedDir = __DIR__."/../../temp/export/";
		$e = explode("\\",get_class($this));
		$this->feedName = Strings::lower(end($e));
		$this->feedFile = $this->feedDir.$this->feedName.".csv";
	}


	public function render(){
		if(file_exists($this->feedFile)){
			bdump("jedeme z cache");
			return file_get_contents($this->feedFile);
		} else {
			bdump("renderujeme on demand");
			$csv = $this->saveCsv($this->feedFile);
			return $csv;
		}
	}

	/**
	 * @return string
	 */
	abstract public function getCsv();



	/**
	 * @param string
	 * @return void
	 * @throws \InvalidStateException
	 */
	public function saveCsv($file)
	{
		$p = pathinfo($file);
		if(!file_exists($p["dirname"])){
			mkdir($p["dirname"]);
		}
		$csv = $this->getCsv();
		file_put_contents($file, $csv);
		return $csv;
	}



	/**
	 * @param string
	 * @param string
	 * @return \DOMElement
	 */
	protected function createElement($name, $cdata)
	{
		$el = $this->xmlDocument->createElement($name);
		if ($cdata !== NULL) {
			$el->appendChild($el->ownerDocument->createCDATASection($cdata));
		}
		return $el;
	}
}