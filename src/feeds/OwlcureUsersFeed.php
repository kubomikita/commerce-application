<?php


namespace Commerce\Xml;


use Kubomikita\Commerce\ConfiguratorInterface;
use Kubomikita\Service;
use Nette\InvalidStateException;

class OwlcureUsers extends XmlFeed {
	/** @var ConfiguratorInterface */
	private $context;
	/** @var array */
	private $config;

	protected function isAllowedQuery(): array {
		return ["days","limit","offset"];
	}

	public function getXml($query = []) {

		$this->context = Service::get("container");
		$this->config = $this->context->getParameter("heureka");

		$domtree = new \DOMDocument("1.0","UTF-8");
		$domtree->preserveWhiteSpace = false;
		$domtree->formatOutput = true;

		$xmlRoot = $domtree->createElement("USERS");
		$xmlRoot = $domtree->appendChild($xmlRoot);

		$Search = new \PartnerSearchResult();$i=0;
		//$Search->limit = 10;

		if(isset($query["days"]) && $query["days"] > 0){
			$Search->created = strtotime("-{$query['days']} day");
			$Result = $Search->netteQuery();
		} else {
			if ( ! isset( $query["limit"] ) ) {
				throw new InvalidStateException( "Limit is required parameter" );
			}
			if ( ! isset( $query["offset"] ) ) {
				throw new InvalidStateException( "Offset is required parameter" );
			}
			if( $query["limit"] > 500) {
				throw new InvalidStateException("Max limit is 500 rows");
			}
			$Result = $Search->netteQuery((int) $query["limit"], (int) $query["offset"]);
		}

		//$Search->id = [253,23410,1113];
		//file_put_contents(TEMP_DIR."export/owlcure_users_count",$Search->count());

		foreach($Result as $row){

			$P = new \Partner($row);

			$shopitem = $domtree->createElement( "USER" );
			$shopitem = $xmlRoot->appendChild( $shopitem );
			$shopitem->appendChild($domtree->createElement("USER_ID", htmlspecialchars($P->id)));
			$shopitem->appendChild($domtree->createElement("NAME", trim(htmlspecialchars($P->format_nazov()))));
			$shopitem->appendChild($domtree->createElement("BIRTH_DATE",null));
			$shopitem->appendChild($domtree->createElement("SEX",null));
			$shopitem->appendChild($domtree->createElement("EMAIL",htmlspecialchars($P->username)));
			$shopitem->appendChild($domtree->createElement("CREATED_AT",$P->created->show("Y-m-d H:i:s")));
			$shopitem->appendChild($domtree->createElement("GROUP",$P->mailing ? "Newsletter" : "Others"));

		}
		$xml = $domtree->saveXML();

		return $xml;
	}

}