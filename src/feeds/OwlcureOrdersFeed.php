<?php


namespace Commerce\Xml;

use Nette\InvalidStateException;
use Kubomikita\Commerce\ConfiguratorInterface;
use Kubomikita\Service;

class OwlcureOrders extends XmlFeed {
	/** @var ConfiguratorInterface */
	private $context;
	/** @var array */
	private $config;

	protected function isAllowedQuery(): array {
		return ["days","offset","limit"];
	}

	public function getXml($query = []) {

		$this->context = Service::get("container");
		$this->config = $this->context->getParameter("heureka");

		$domtree = new \DOMDocument("1.0","UTF-8");
		$domtree->preserveWhiteSpace = false;
		$domtree->formatOutput = true;

		$xmlRoot = $domtree->createElement("ORDERS");
		$xmlRoot = $domtree->appendChild($xmlRoot);

		$Search = new \ObjednavkaSearchResult();$i=0;

		if(isset($query["days"]) && $query["days"] > 0){
			$Search->datum_od = new \Timestamp(strtotime("-{$query['days']} day"));
			$Result = $Search->netteQuery();
		} else {
			if ( ! isset( $query["limit"] ) ) {
				throw new InvalidStateException( "Limit is required parameter" );
			}
			if ( ! isset( $query["offset"] ) ) {
				throw new InvalidStateException( "Offset is required parameter" );
			}
			if( $query["limit"] > 500) {
				throw new InvalidStateException("Max limit is 500 rows");
			}
			$Result = $Search->netteQuery((int) $query["limit"], (int) $query["offset"]);
		}
		//file_put_contents(TEMP_DIR."export/owlcure_orders_count",$Search->count());

		foreach($Result as $row){

			$P = new \Objednavka($row);

			$shopitem = $domtree->createElement( "ORDER" );
			$shopitem = $xmlRoot->appendChild( $shopitem );
			$shopitem->appendChild($domtree->createElement("ORDER_ID", htmlspecialchars($P->id)));
			$shopitem->appendChild($domtree->createElement("CREATED_AT",$P->datum->show("Y-m-d H:i:s")));
			$shopitem->appendChild($domtree->createElement("USER_ID", htmlspecialchars($P->klient->id)));

			$items = $domtree->createElement("ITEMS");
			$items = $shopitem->appendChild($items);
			/** @var \ObjednavkaItem $Oi */
			foreach($P->items() as $Oi){
				$variant_id = implode("",$Oi->tovar->Variant);
				$item = $domtree->createElement("ITEM");
				$item = $items->appendChild($item);

				$item->appendChild($domtree->createElement("MAIN_ID", $Oi->tovar->id));
				$item->appendChild($domtree->createElement("ITEM_ID", $Oi->tovar->id.$variant_id));
				$item->appendChild($domtree->createElement("PRICE",$Oi->cena->suma()));
				$item->appendChild($domtree->createElement("QUANTITY",$Oi->mnozstvo_objednane));
				$item->appendChild($domtree->createElement("SALE", 0));
			}

		}

		$xml = $domtree->saveXML();
		return $xml;
	}

}