<?php


namespace Commerce\Xml;


use Kubomikita\Commerce\ProteinConfigurator;
use Kubomikita\Service;

class Heureka extends XmlFeed {
	/** @var ProteinConfigurator */
	private $context;
	/** @var array */
	private $config;

	//protected $debug = TRUE;

	public function getXml(array $query = []) {
		$this->context = Service::get("container");
		$this->config = $this->context->getParameter("heureka");

		$domtree = new \DOMDocument("1.0","UTF-8");
		$domtree->preserveWhiteSpace = false;
		$domtree->formatOutput = true;

		$xmlRoot = $domtree->createElement("SHOP");
		$xmlRoot = $domtree->appendChild($xmlRoot);

		$Search = new \TovarSearchResult();$i=0;

		if($this->config["vyrobcaSearch"]) {
			$vyrobcovia = array();
			foreach ( \TovarVyrobca::fetch() as $TV ) {
				if ( $TV->heureka_xml ) {
					$vyrobcovia[] = $TV->id;
				}
			}
			$Search->vyrobca = $vyrobcovia;
		}
		$Search->aktivny = 1;
		$Search->indexable = 1;
		$Search->visible = 1;
		//$Search->id = 2789;
		//$Search->limit = 10;
		file_put_contents(TEMP_DIR."export/heureka_xml_count",$Search->count());

		foreach($Search->netteQuery() as $row){

			$T = new \Tovar($row->tovar_id);
			//dumpe(method_exists($T, "alt_nazov"), $T->nazov, $T->alt_nazov("heureka"));
			$productName = method_exists($T, "alt_nazov") ? $T->alt_nazov("heureka") : (string) $T->nazov;
			//dumpe($productName,method_exists($T, "alt_nazov"));
			if(!$this->config["xmlAllVariants"]) {
				$shopitem = $domtree->createElement( "SHOPITEM" );
				$shopitem = $xmlRoot->appendChild( $shopitem );
				$shopitem->appendChild( $domtree->createElement( 'ITEM_ID', htmlspecialchars( $T->id ) ) );
				$shopitem->appendChild( $domtree->createElement( 'PRODUCT', htmlspecialchars( $productName ) ) );
				$shopitem->appendChild( $domtree->createElement( 'PRODUCTNAME', htmlspecialchars( $productName ) ) );
				$shopitem->appendChild( $domtree->createElement( 'DESCRIPTION', htmlspecialchars( strip_tags($T->intro) ) ) );
				$shopitem->appendChild( $domtree->createElement( 'URL',
					$this->context->getParameter( "shop", "url" ) . $T->link() ) );
				$shopitem->appendChild( $domtree->createElement( 'IMGURL',
					$this->context->getParameter( "shop", "url" ) . $T->image()->cachedUrl() ) );
				$shopitem->appendChild( $domtree->createElement( 'PRICE',
					strtr( sprintf( "%.2F", $T->predajna_cena_user()->zaklad() ), array( '.' => ',' ) ) ) );
				$shopitem->appendChild( $domtree->createElement( 'PRICE_VAT',
					strtr( sprintf( "%.2F", $T->predajna_cena_user()->suma() ), array( '.' => ',' ) ) ) );
				$shopitem->appendChild( $domtree->createElement( 'VAT',
					strtr( sprintf( "%.2F", $T->predajna_cena_user()->dph() ), array( '.' => ',' ) ) ) );
				$shopitem->appendChild( $domtree->createElement( 'MANUFACTURER',
					htmlspecialchars( $T->vyrobca->nazov ) ) );
				$shopitem->appendChild( $domtree->createElement( 'ITEM_TYPE', 'new' ) );


					$kategorie = [];
				/*	foreach ( $T->primarna_kategoria->fetchPath() as $Kp ) {
						if ( $Kp->id != 1 ) {
							$kategorie[] = $Kp->alt_nazov( 'heureka_sk' );
						};
					};
					$category_text = htmlspecialchars( join( ' | ', $kategorie ) );
				*/
				$path = $T->primarna_kategoria->fetchPath() ;
				rsort($path);
				foreach ($path as $Kp ) {
					if ( $Kp->id != 1 ) {
						$alt = $Kp->alt_nazov( 'heureka_category' );
						if(is_array($alt)) {
							if ( $alt["stop"] == false ) {
								$kategorie[] = $alt["category"];
							} else {
								$kategorie   = [];
								$kategorie[] = $alt["category"];
								break;
							}
						} else {
							$kategorie[] = $alt;
						}
					}
				}
				rsort($kategorie);
				$category_text = htmlspecialchars( join( ' | ', $kategorie ) );

				$shopitem->appendChild( $domtree->createElement( 'CATEGORYTEXT', $category_text ) );
				$shopitem->appendChild( $domtree->createElement( 'EAN', htmlspecialchars( strip_tags( $T->ean ) ) ) );
				$shopitem->appendChild( $domtree->createElement( 'DELIVERY_DATE', $this->getDeliveryDate($T)  ) );
			} else {

				$Var_list=$T->sablona->varianty_list();
				foreach($T->varianty_fetch_obj('all') as $V){
					$visible=$V->visible_show();
					if(\Config::variantHideInactivePublic){ $visible=$visible && $V->V_aktivny; };
					if($V->predajna_cena_user()==0){ $visible=0; };
					if($visible){
						$shopitem = $domtree->createElement( "SHOPITEM" );
						$shopitem = $xmlRoot->appendChild( $shopitem );
						$var_nazov = $var_link = array(); $var_ids = array();
						$params = [];
						foreach($Var_list as $TTV){ if($T->varianty_used($TTV->ident)){
							$var_nazov[]=$V->Variant($TTV->ident)->nazov;
							$var_ids[]=$V->Variant($TTV->ident)->id;
							$var_link["variant"][$TTV->ident] = $V->Variant($TTV->ident)->id;

							$item = $domtree->createElement( "PARAM" );
							$item->appendChild( $domtree->createElement( "PARAM_NAME", htmlspecialchars($TTV->nazov) ) );
							$item->appendChild( $domtree->createElement( "VAL",
								htmlspecialchars($V->Variant($TTV->ident)->nazov) ) );

							$params[] = $item;

						}; };

						$shopitem->appendChild( $domtree->createElement( 'ITEM_ID', htmlspecialchars( $T->id.(!empty($var_ids)?"-":"").join("-",$var_ids) ) ) );
						$shopitem->appendChild( $domtree->createElement( 'PRODUCT', htmlspecialchars($T->nazov) ) ); //.' '.join(' ',$var_nazov)
						$shopitem->appendChild( $domtree->createElement( 'PRODUCTNAME', htmlspecialchars($T->nazov) ) ); // .' '.join(' ',$var_nazov)
						$shopitem->appendChild( $domtree->createElement( 'DESCRIPTION', htmlspecialchars( strip_tags($T->intro) ) ) );
						$shopitem->appendChild( $domtree->createElement( 'URL',
							htmlspecialchars($this->context->getParameter( "shop", "url" ) . $T->link().(!empty($var_link["variant"]) ? '?'.http_build_query($var_link) : "" ) )));
						$shopitem->appendChild( $domtree->createElement( 'IMGURL',
							$this->context->getParameter( "shop", "url" ) . $T->image()->cachedUrl() ) );
						$shopitem->appendChild( $domtree->createElement( 'PRICE',
							strtr( sprintf( "%.2F", $T->predajna_cena_user()->zaklad() ), array( '.' => ',' ) ) ) );
						$shopitem->appendChild( $domtree->createElement( 'PRICE_VAT',
							strtr( sprintf( "%.2F", $T->predajna_cena_user()->suma() ), array( '.' => ',' ) ) ) );
						$shopitem->appendChild( $domtree->createElement( 'VAT',
							strtr( sprintf( "%.2F", $T->predajna_cena_user()->dph() ), array( '.' => ',' ) ) ) );
						$shopitem->appendChild( $domtree->createElement( 'MANUFACTURER',
							htmlspecialchars( $T->vyrobca->nazov ) ) );
						$shopitem->appendChild( $domtree->createElement( 'ITEM_TYPE', 'new' ) );

						$kategorie = [];
						$path = $T->primarna_kategoria->fetchPath() ;
						rsort($path);
						foreach ($path as $Kp ) {
							if ( $Kp->id != 1 ) {
								$alt = $Kp->alt_nazov( 'heureka_category' );
								if(is_array($alt)) {
									if ( $alt["stop"] == false ) {
										$kategorie[] = $alt["category"];
									} else {
										$kategorie   = [];
										$kategorie[] = $alt["category"];
										break;
									}
								} else {
									$kategorie[] = $alt;
								}
							}
						}
						rsort($kategorie);
						$category_text = htmlspecialchars( join( ' | ', $kategorie ) );

						$shopitem->appendChild( $domtree->createElement( 'CATEGORYTEXT', $category_text ) );
						$shopitem->appendChild( $domtree->createElement( 'EAN', htmlspecialchars( strip_tags( $T->ean ) ) ) );
						$shopitem->appendChild( $domtree->createElement( 'DELIVERY_DATE', $this->getDeliveryDate($V) ) );
						if($V->variant_gift()->id>0){
							$shopitem->appendChild( $domtree->createElement( 'GIFT', $V->variant_gift()->nazov ) );
						}
						foreach($params as $p){
							$shopitem->appendChild($p);
						}
					}
				}
			}

		}
		$xml = $domtree->saveXML();

		return $xml;
	}
	public function getDeliveryDate(\Tovar $T){
		$config = $this->config;
		if(isset($config["delivery_date"]) && $config["delivery_date"] !== null){
			return $config["delivery_date"];
		}
		if($T->sklad_stav() > 0){
			return 0;
		}
		return "";
	}

}