<?php


namespace Commerce\Xml;


use Kubomikita\Commerce\ProteinConfigurator;
use Kubomikita\Service;

class Brands extends XmlFeed {
	/** @var ProteinConfigurator */
	private $context;

	public function getXml(array $query = []) {
		$this->context = Service::get("container");

		$domtree = new \DOMDocument("1.0","UTF-8");
		$domtree->preserveWhiteSpace = false;
		$domtree->formatOutput = true;

		$xmlRoot = $domtree->createElement("brands");
		$xmlRoot = $domtree->appendChild($xmlRoot);



		$kategorie = \TovarVyrobca::fetch();
		$i = 0;
		/** @var \TovarVyrobca $k */
		foreach($kategorie as $k) {
			if ( $k->aktivny && $k->count_items() > 0) {
				//dump( $k );

				$y = 1;

				$Search = new \TovarSearchResult();$i=0;

				$Search->aktivny = 1;
				$Search->indexable = 1;
				$Search->visible = 1;
				$Search->vyrobca = $k->id;
				$Search->order_by = "vyrobca_id";

				$q = $Search->netteQuery("MIN(t.cena) as mincena, MAX(t.cena) as maxcena, COUNT(t.tovar_id) as pocet");
				$r = $q->fetch();
				bdump($r);
				//dump($name,$r);
				$shopitem = $domtree->createElement( "brand" );
				$shopitem = $xmlRoot->appendChild( $shopitem );
				$shopitem->appendChild( $domtree->createElement( 'id', htmlspecialchars( $k->id ) ) );
				$shopitem->appendChild( $domtree->createElement( 'brand_url',
					$this->context->getParameter( "shop", "url" ) . $k->link() ) );
				$shopitem->appendChild($domtree->createElement("brand_name", htmlspecialchars($k->nazov)));

				/*$item = $domtree->createElement( "brands" );
				foreach($Search->netteQuery("DISTINCT(t.vyrobca_id)") as $vid){
					$V = new \TovarVyrobca($vid["vyrobca_id"]);
					if((int) $V->id > 0){
						$item->appendChild($domtree->createElement("brand",htmlspecialchars((string) $V->nazov)));
					}
				}
				$shopitem->appendChild($item);*/
				$shopitem->appendChild($domtree->createElement("min_price",strtr( sprintf( "%.2F", $r->mincena ), array( '.' => ',' ) )));
				$shopitem->appendChild($domtree->createElement("max_price",strtr( sprintf( "%.2F", $r->maxcena ), array( '.' => ',' ) )));
				$shopitem->appendChild($domtree->createElement("products_count",(int)$r->pocet));
				$shopitem->appendChild($domtree->createElement("min_sale",(int)0));
				$shopitem->appendChild($domtree->createElement("max_sale",(int)0));

				$i++;
				if($i == 4) {
					//break;
				}
			}
		}
		$xml = $domtree->saveXML();

		return $xml;
	}
}