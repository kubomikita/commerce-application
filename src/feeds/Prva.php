<?php


namespace Commerce\Xml;


class Prva extends XmlFeed{
	public function getXml(array $query = []) {
		ob_start();

		echo('<'.'?xml version="1.0" encoding="utf-8"?'.'>'."\n".'<DATA>');
		echo('<TIMESTAMP>'.Timestamp()->show('YmdHis').'00</TIMESTAMP> ');

		$Search = new \TovarSearchResult();
		$q = mysqli_query(\CommerceDB::$DB, $Search->query());
		$i = 0;
		while($r = mysqli_fetch_assoc($q)){
			//dump($r);
			$T = new \Tovar($r["tovar_id"]);
			if($T->visible_show()){


				$Var_list=$T->sablona->varianty_list();
				foreach($T->varianty_fetch_obj('all') as $V){
					$visible=$V->visible_show();
					if(\Config::variantHideInactivePublic){ $visible=$visible && $V->V_aktivny; };
					if($visible){
						$var_nazov = array(); $var_ids = array();
						foreach($Var_list as $TTV){ if($T->varianty_used($TTV->ident)){
							$var_nazov[]=$TTV->nazov.': '.$V->Variant($TTV->ident)->nazov;
							$var_ids[]=$V->Variant($TTV->ident)->id;
						}; };
						$hmotnost = (int)$V->V_hmotnost;
						echo('<PRODUCT>');
						echo('<KATALOG>'.htmlspecialchars($V->V_kod_systemu).'</KATALOG>');
						echo('<NAME>'.htmlspecialchars($T->nazov.' - '.join(', ',$var_nazov)).'</NAME>');
						echo('<ZASOBA>'.htmlspecialchars($V->sklad_stav()).'</ZASOBA>');
						echo('<PRICE>'.strtr(sprintf("%.2F",$V->predajna_cena_user()->multiply(0.9)->zaklad()),array('.'=>',')).'</PRICE>');
						echo('<END_PRICE>'.strtr(sprintf("%.2F",$V->predajna_cena_user()->suma()),array('.'=>',')).'</END_PRICE>');
						echo('<ID>'.htmlspecialchars($T->id).'</ID>');
						echo('<VYROBCA>'.htmlspecialchars($T->vyrobca->nazov).'</VYROBCA>');
						echo('<EAN>'.htmlspecialchars(strip_tags($V->ean)).'</EAN>');
						$kategorie=array();
						foreach($T->primarna_kategoria->fetchPath() as $Kp){ if($Kp->id!=1){ $kategorie[]=$Kp->alt_nazov('prva_sk'); }; };
						echo('<CATEGORY>'.htmlspecialchars($kategorie[1]).'</CATEGORY>');
						echo('<MODEL>'.htmlspecialchars($kategorie[sizeof($kategorie)-1]).'</MODEL>');
						echo('<POPIS>'.htmlspecialchars(strip_tags($T->intro)).'</POPIS>');
						echo('<STATUS>'.($V->sklad_stav()?'1':'0').'</STATUS>');
						echo('<IMG>'.htmlspecialchars($T->image()->cachedNoWatermarkUrl()).'</IMG>');
						echo('<WEIGHT>'.htmlspecialchars(strtr(sprintf("%.3f",($hmotnost/1000)),array('.'=>','))).'</WEIGHT>'); //

						echo('</PRODUCT>');
					}
				};
			};



		};

		echo('</DATA>');

		$xml=ob_get_contents();
		ob_end_clean();
		//Cache::put('tovar','xml_prva',$xml);
		return $xml;
	}
}