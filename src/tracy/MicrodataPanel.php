<?php

namespace Kubomikita\Commerce;

use Kubomikita\Service;
use Tracy\IBarPanel;

class MicrodataPanel implements IBarPanel{
	private $microdata = null;
	public function __construct(Microdata $microdata) {
		$this->microdata = $microdata;
		//dump($this->microdata,Service::get("microdata"));
	}

	public function getPanel() {
		$sections = $this->microdata->getSection();
		ob_start(function () {});
		require __DIR__ . '/templates/MicrodataPanel.panel.phtml';
		return ob_get_clean();
	}
	public function getTab() {
		//$name = $this->name;
		$count = $this->microdata->count();
		//$totalTime = $this->totalTime;
		ob_start(function () {});
		require __DIR__ . '/templates/MicrodataPanel.tab.phtml';
		return ob_get_clean();
	}
}