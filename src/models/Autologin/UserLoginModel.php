<?php

/**
 * @property User $users
 */
class UserLoginModel extends Autologin implements IAutologin  {
	public static $table = "ec_users_logins";
	/**
	 * @inject User
	 */
	public $users_id;

	public function getUsername(){
		return $this->users->username;
	}
	public function getFullname(){
		return (strlen(trim($this->users->fullname)) > 0 ? trim($this->users->fullname) : null);
	}
	public function getEmail(){
		return (strlen(trim($this->users->email)) > 0 ? trim($this->users->email) : null);
	}

	public static function create($partner_id,$hash){
		$l = static::detect(true);
		$l->users_id = $partner_id;
		$l->hash  = $hash;
		$l->valid = 1;
		$l->date  = new Nette\Utils\DateTime();
		$l->save();
	}
}