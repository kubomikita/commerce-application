<?php
abstract class Autologin extends ActiveRecord {
	public $id;
	public $hash;
	public $valid;
	public $date;
	public $device;
	public $device_os;
	public $device_brand;
	public $browser;
	public $ip;
	public $host;
	public $country;

	private $row;

	/*public function __construct($data = null) {

		parent::__construct($data);
		dump($R,$this);
		exit;

	}*/

	/**
	 * @param $hash
	 *
	 * @return UserLoginModel|PartnerLoginModel
	 * @throws Exception
	 */
	public static function checkHash($hash) {
		$db = Registry::get( "database.context" );
		$ret = new static();
		$s = $db->table( static::$table )->where( [ "hash" => $hash, "valid" => 1 ] );
		$q = $s->fetch();
		if($q){
			$ret = new static($q->id);
			$ret->checkDevice();
		}
		return $ret;
	}

	public static function detect($new = false){
		if($new) {
			$l = new static();
		} else {
			$l = new stdClass();
		}
		$l->device = Detect::deviceType();
		$l->device_os = Detect::os();
		$l->device_brand = Detect::brand();
		$l->browser = Detect::browser();
		$l->ip = Detect::ip();
		$l->host = Detect::ipHostname();
		$l->country = Detect::ipCountry();
		return $l;
	}

	public function checkDevice(){
		$compare = ["device","device_os","device_brand","browser","country"];
		$l = self::detect();
		foreach($compare as $key){
			if($this->{$key} != $l->{$key}){
				//$this->notifyLogin();
				break;
			}
		}
	}
	public function notifyLogin(){
		$m = new Mail();
		$m->setSubject("Podozrivé prihlásenie na ".Registry::get("configurator")->getParameter("shop","name"));
		$l = self::detect();
		$text = "<p>Zaznamenali sme podozrivé prihlásenie (účet <strong>".$this->getUsername()." [".$this->getFullname()."]</strong>) z tohto zariadenia: <strong>";
		$text .= implode(", ",(array) $l);
		$text .= "</strong></p><p>Ak ste to boli vy, nevenujte tomuto mailu pozornosť.</p>";

		if($this->getEmail() !== null) {
			$m->addTo($this->getEmail());
		} else {
			$text .= '<p style="color:red;"><strong>Upozornenie:</strong> Užívateľ nemá vyplnený notifikačný mail. Preto tento mail prišiel Vám, ako administrátorovi.</p>';
			$m->addTo(Registry::get("configurator")->getParameter("shop","email"));
		}

		$template = Registry::get("latte")->renderToString(MAIL_DIR."message.latte",["message" => $text]);
		$m->setBody($template);

		$m->send();
	}

	public static  function unactive($hash){
		/** @var \Nette\Database\Context $db */
		$db = Registry::get("database.context");
		$db->table(static::$table)->where(["hash" => $hash,"valid" => 1])->update(["valid" => 0]);
	}
}