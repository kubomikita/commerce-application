<?php
/**
 * @property Partner $partner
 */
class PartnerLoginModel extends Autologin implements IAutologin {
	public static $table = "ec_partneri_logins";
	/** @inject Partner */
	public $partner_id;

	public function getUsername(){
		return $this->partner->username;
	}
	public function getFullname(){
		return (strlen(trim($this->partner->format_nazov())) > 0 ? trim($this->partner->format_nazov()) : null);
	}
	public function getEmail(){
		return (strlen(trim($this->partner->username)) > 0 ? trim($this->partner->username) : null);
	}

	public static function create($partner_id,$hash){
		$l = static::detect(true);
		$l->partner_id = $partner_id;
		$l->hash  = $hash;
		$l->valid = 1;
		$l->date  = new Nette\Utils\DateTime();
		$l->save();
	}
}