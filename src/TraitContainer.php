<?php

namespace Kubomikita\Core;

use Kubomikita\Commerce\Configurator;
use Kubomikita\Commerce\ConfiguratorInterface;
use Kubomikita\Service;
use Nette\Database\Connection;
use Nette\Database\Row;
use Nette\DI\Container;
use Nette\Http\Request;
use Nette\Http\Response;
use Nette\InvalidArgumentException;
use Nette\MemberAccessException;
use Nette\SmartObject;

/**
 * Trait TraitContainer
 * @package Kubomikita\Core
 * @property-read Connection $db
 * @property-read Container $context
 * @property-read Container $container
 * @property-read Request $request
 * @property-read Request $httpRequest
 * @property-read Response $response
 * @property-read Response $httpResponse
 * @property-read Configurator $configurator
 */
trait TraitContainer {
	//use SmartObject {
	//	SmartObject::__get as smart_object__get;
	//}

	public static $instances = [];

	public static function select() {
		if(isset(static::$select)) {
			return implode(", ", static::$select);
		}
		return 'v.*';
	}

	public static function getCacheGroup(){
		if(!isset(static::$table)){
			throw new MemberAccessException('Model static::\$table is undefined.');
		}
		return static::$table;
	}

	public function &__get( $name ) {
		if ( $name == "db" ) {
			return Service::getByType(Connection::class);
		} elseif ($name == "container" || $name == "context") {
			return Service::getByType(Container::class);
		} elseif($name == "request" || $name == "httpRequest"){
			return Service::getByType(Request::class);
		} elseif($name == "response" || $name == "httpResponse"){
			return Service::getByType(Response::class);
		} elseif($name == "configurator") {
			return Service::getByType(ConfiguratorInterface::class);
		}
		//return $this->smart_object__get($name);
	}

	/**
	 * @param int|Row $id
	 *
	 * @return Row|null
	 */
	public function row($id, string $alias = 'v') : ? Row {
		if(!isset(static::$table)){
			throw new MemberAccessException('Model static::\$table is undefined.');
		}
		if($id instanceof Row){
			return $id;
		} elseif (is_numeric($id) && (int) $id > 0) {
			if(!isset(static::$instances[$id])) {
				$R = static::$instances[$id] = \Cache::load(self::getCacheGroup(), (int) $id, function (&$dependencies) use($id, $alias) {
					return $this->db->query("SELECT " . static::select()  . " FROM ".static::$table." AS ".$alias." WHERE ".$alias.".id = ?", (int) $id)->fetch();
				});
			} else {
				$R = static::$instances[$id];
			}
			return $R;
		}

		return null;
	}
}