if(window.jQuery) {
	var CookieBar = {
		el                : ".cookieBar",
		options           : {},
		defaultOptions    : function () {
			return {
				infotext    : "Tento web používa na poskytovanie služieb, personalizáciu reklám a analýzu návštevnosti súbory cookie. <br>" +
					"Ak budete pokračovať bez vykonania zmien nastavení, predpokladáme, že chcete z našej webovej stránky dostávať všetky súbory cookie.",
				infobutton  : "Viac informácií",
				infolink    : "cookies",
				heading     : "Súbory cookie",
				acceptbutton: "Rozumiem",
				changebutton: "Zmeniť nastavenia pre súbory cookie",
				settings    : {
					heading  : "Nastavenie súborov cookie",
					button   : "Uložiť zmeny",
					technical: {
						label   : "Nutné súbory cookies",
						infotext: "Nutné súbory cookie sú potrebné na poskytovanie základných funkcií na našej webovej stránke. Nutné súbory cookie sa nevyužívajú na ukladanie osobných údajov. Webová stránka nemôže správne fungovať bez týchto typov cookie."
					},
					analytics: {
						label   : "Analytické súbory cookie",
						infotext: "Analytické súbory cookie získavajú informácie o vašom používaní webovej stránky. Ako nástroj na analýzu webu používame službu Google Analytics. Tento nástroj nám pomáha optimalizovať naše stránky a tým vám poskytnúť lepší užívateľský komfort.",
					},
					marketing: {
						label   : "Marketingové súbory cookie",
						infotext: "Marketingové súbory cookie používame k prispôsobeniu informácií o našich produktoch a službách podľa vašich záujmov. Vďaka nim vám dokážeme zobrazovať relevantnú a zaujímavú reklamu."
					}
				}
			}
		},
		showSettingsCookie: function () {
			var morebar = $(CookieBar.el + " .barMore");
			if (!morebar.is(":visible")) {
				morebar.slideDown(500);
			} else {
				morebar.slideUp(500);
			}
		},
		saveCookies       : function (allow_analytics_cookies, allow_marketing_cookies) {
			document.cookie = "cookies_setted=1; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
			document.cookie = "allow_technical_cookies=" + 1 + "; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
			document.cookie = "allow_analytics_cookies=" + allow_analytics_cookies + "; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
			document.cookie = "allow_marketing_cookies=" + allow_marketing_cookies + "; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
			CookieBar.close();
			/*location.reload();*/
		},
		saveSettings      : function () {
			var allow_analytics_cookies = $("input[name='allow_analytics_cookies']:checked").length;
			var allow_marketing_cookies = $("input[name='allow_marketing_cookies']:checked").length;
			CookieBar.saveCookies(allow_analytics_cookies, allow_marketing_cookies);
		},
		mergeOptions      : function (obj1, obj2) {
			var obj3 = {};
			for (var attrname in obj1) {
				obj3[attrname] = obj1[attrname];
			}
			for (var attrname in obj2) {
				obj3[attrname] = obj2[attrname];
			}
			return obj3;
		},
		isEmpty           : function (obj) {
			for (var key in obj) {
				if (obj.hasOwnProperty(key))
					return false;
			}
			return true;
		},
		setup             : function (options) {
			CookieBar.options = CookieBar.mergeOptions(CookieBar.defaultOptions(), options);
		},
		close : function(){
			$(CookieBar.el).slideUp(300);
		},
		render            : function () {
			if (CookieBar.isEmpty(CookieBar.options)) {
				CookieBar.setup({});
			}
			$(CookieBar.el).click(function (event) {
				event.stopPropagation();
			});
			var html = '<div class="cookieBar">' +
				'<div class="container py-3">' +
				'<div class="bar">' +
				'<p class="title">' + CookieBar.options.heading + '</p>' +
				'<a href="javascript:;" onclick="javascript:CookieBar.saveCookies(1, 1);" class="button-close"><span class="fa fa-times"></span></a>' +
				'<div class="infotext">' + CookieBar.options.infotext + ' <a class="button-more" href="' + CookieBar.options.infolink + '"><span class="fa fa-link"></span> ' + CookieBar.options.infobutton + '</a>' +
				'</div>' +
				'<div class="d-md-flex d-block justify-content-center align-items-center mt-md-4">' +
				'<div class="mx-2 my-2 my-md-0">' +
				'<a allow="" class="btn btn-primary" href="javascript:;" onclick="javascript:CookieBar.saveCookies(1, 1);">' + CookieBar.options.acceptbutton + '</a></div>' +
				'<div class="mx-2">' +
				'<a change="" href="javascript:;" onclick="javascript:CookieBar.showSettingsCookie();">' + CookieBar.options.changebutton + '</a></div>' +
				'</div>' +
				'</div>' +
				'<div class="barMore">' +
				'<div class="infotext">' +
				'<p class="title">' + CookieBar.options.settings.heading + '</p>' +
				'<div class="settings row mx-auto my-2">' +
				'<div class="col-md-4">' +
				'<div class="cookie-info">' +
				'<div class="custom-control custom-checkbox mb-3">' +
				'<input type="checkbox" id="allow_technical_cookies" name="allow_technical_cookies" checked="" disabled="" value="1" class="custom-control-input">' +
				'<label class="custom-control-label" for="allow_technical_cookies">' + CookieBar.options.settings.technical.label + '</label>' +
				'</div>' +
				'<p>' + CookieBar.options.settings.technical.infotext + '</p>' +
				'</div>' +
				'</div>' +
				'<div class="col-md-4">' +
				'<div class="cookie-info">' +
				'<div class="custom-control custom-checkbox mb-3">' +
				'<input type="checkbox" id="allow_analytics_cookies" name="allow_analytics_cookies" value="1" class="custom-control-input">' +
				'<label class="custom-control-label" for="allow_analytics_cookies">' + CookieBar.options.settings.analytics.label + '</label>' +
				'</div>' +
				'<p>' + CookieBar.options.settings.analytics.infotext + '</p>' +
				'</div>' +
				'</div>' +
				'<div class="col-md-4">' +
				'<div class="cookie-info">' +
				'<div class="custom-control custom-checkbox mb-3">' +
				'<input type="checkbox" id="allow_marketing_cookies" name="allow_marketing_cookies" value="1" class="custom-control-input">' +
				'<label class="custom-control-label" for="allow_marketing_cookies">' + CookieBar.options.settings.marketing.label + '</label>' +
				'</div>' +
				'<p>' + CookieBar.options.settings.marketing.infotext + '</p>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'</div>' +
				'<a href="javascript:;" onclick="javascript:CookieBar.saveSettings();" class="btn btn-primary">' + CookieBar.options.settings.button + '</a>' +
				'</div>' +
				'</div>' +
				'</div>';
			$("body").append(html);
		}
	}
} else {
	console.log("jQuery not loaded");
}